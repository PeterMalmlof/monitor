object ProcForm: TProcForm
  Left = 711
  Top = 610
  AutoScroll = False
  Caption = 'Processes From Boot'
  ClientHeight = 367
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object BottomPanel: TPanel
    Left = 8
    Top = 8
    Width = 417
    Height = 321
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 177
      Top = 0
      Height = 321
      OnCanResize = Splitter1CanResize
    end
    object Pages: TPageControl
      Left = 0
      Top = 0
      Width = 177
      Height = 321
      ActivePage = TabHier
      Align = alLeft
      TabOrder = 0
      OnChange = PagesChange
      object TabLog: TTabSheet
        Caption = 'Log'
        OnResize = TabLogResize
        object ProcList: TListView
          Left = 8
          Top = 4
          Width = 153
          Height = 149
          Columns = <
            item
              Caption = 'PID'
            end
            item
              Caption = 'Process'
            end
            item
              Caption = 'Created'
            end
            item
              Caption = 'Event'
            end
            item
              Caption = 'Note'
            end>
          Ctl3D = True
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnChange = ProcListChange
          OnCompare = ProcListCompare
          OnCustomDrawItem = ProcListCustomDrawItem
          OnDblClick = ProcDoubleClick
        end
      end
      object TabAlive: TTabSheet
        Caption = 'Current'
        ImageIndex = 1
        OnResize = TabAliveResize
        object ProcAlive: TListView
          Left = 8
          Top = 8
          Width = 153
          Height = 145
          Checkboxes = True
          Columns = <
            item
              Caption = 'M/PID'
            end
            item
              Caption = 'Process'
            end
            item
              Caption = 'Prio'
            end
            item
              Caption = 'Long %'
            end
            item
              Caption = 'Short %'
            end
            item
              Caption = 'Memory'
            end>
          Ctl3D = True
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          ViewStyle = vsReport
          OnChange = ProcAliveChange
          OnCustomDrawItem = ProcAliveCustomDrawItem
          OnDblClick = ProcDoubleClick
          OnSelectItem = ProcAliveSelectItem
        end
      end
      object TabHier: TTabSheet
        Caption = 'Hierarchy'
        ImageIndex = 2
        OnResize = TabHierResize
        object ProcHier: TTreeView
          Left = 8
          Top = 8
          Width = 153
          Height = 153
          Ctl3D = True
          Indent = 19
          ParentCtl3D = False
          ReadOnly = True
          RowSelect = True
          TabOrder = 0
          OnChange = ProcHierChange
          OnCustomDrawItem = ProcHierCustomDrawItem
          OnDblClick = ProcDoubleClick
        end
      end
    end
    object ProcPanel: TPanel
      Left = 180
      Top = 0
      Width = 237
      Height = 321
      Align = alClient
      TabOrder = 1
      OnResize = ProcPanelResize
      object ProcName: TLabel
        Left = 8
        Top = 8
        Width = 74
        Height = 16
        Caption = 'ProcName'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ProcDesc: TMemo
        Left = 8
        Top = 32
        Width = 201
        Height = 25
        Ctl3D = True
        Lines.Strings = (
          'ProcDesc')
        ParentCtl3D = False
        TabOrder = 0
        WantReturns = False
        OnChange = ProcDescChange
      end
      object LiveProc: TGroupBox
        Left = 8
        Top = 120
        Width = 201
        Height = 193
        Caption = 'Process Information  '
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 1
        object LabelPrio: TLabel
          Left = 16
          Top = 16
          Width = 31
          Height = 13
          Caption = 'Priority'
        end
        object PropPrio: TLabel
          Left = 64
          Top = 16
          Width = 40
          Height = 13
          Caption = 'PropPrio'
        end
        object LabelParent: TLabel
          Left = 16
          Top = 36
          Width = 31
          Height = 13
          Caption = 'Parent'
        end
        object PropParent: TLabel
          Left = 64
          Top = 36
          Width = 53
          Height = 13
          Caption = 'PropParent'
        end
        object LabelCreated: TLabel
          Left = 16
          Top = 52
          Width = 37
          Height = 13
          Caption = 'Created'
        end
        object PropCreated: TLabel
          Left = 64
          Top = 52
          Width = 59
          Height = 13
          Caption = 'PropCreated'
        end
        object LabelPath: TLabel
          Left = 16
          Top = 132
          Width = 22
          Height = 13
          Caption = 'Path'
        end
        object PropPath: TLabel
          Left = 64
          Top = 132
          Width = 44
          Height = 13
          Caption = 'PropPath'
        end
        object LabelThreads: TLabel
          Left = 16
          Top = 68
          Width = 39
          Height = 13
          Caption = 'Threads'
        end
        object PropThreads: TLabel
          Left = 64
          Top = 68
          Width = 61
          Height = 13
          Caption = 'PropThreads'
        end
        object LabelMem: TLabel
          Left = 16
          Top = 84
          Width = 37
          Height = 13
          Caption = 'Memory'
        end
        object PropMem: TLabel
          Left = 64
          Top = 84
          Width = 45
          Height = 13
          Caption = 'PropMem'
        end
        object LabelUsage: TLabel
          Left = 16
          Top = 100
          Width = 31
          Height = 13
          Caption = 'Usage'
        end
        object PropUsage: TLabel
          Left = 64
          Top = 100
          Width = 53
          Height = 13
          Caption = 'PropUsage'
        end
        object LabelUsageAvg: TLabel
          Left = 16
          Top = 116
          Width = 31
          Height = 13
          Caption = 'Usage'
        end
        object PropUsageAvg: TLabel
          Left = 64
          Top = 116
          Width = 72
          Height = 13
          Caption = 'PropUsageAvg'
        end
        object LabelCmdLine: TLabel
          Left = 16
          Top = 148
          Width = 41
          Height = 13
          Caption = 'CmdLine'
        end
        object PropCmdLine: TLabel
          Left = 64
          Top = 148
          Width = 63
          Height = 13
          Caption = 'PropCmdLine'
        end
      end
      object NotLevel: TGroupBox
        Left = 8
        Top = 64
        Width = 201
        Height = 49
        Caption = 'Notification Level '
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 2
        object BtnNormal: TRadioButton
          Left = 8
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Normal'
          TabOrder = 0
          OnClick = BtnNormalClick
        end
        object BtnWarn: TRadioButton
          Left = 72
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Warn'
          Color = clYellow
          ParentColor = False
          TabOrder = 1
          OnClick = BtnWarnClick
        end
        object BtnAlarm: TRadioButton
          Left = 128
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Alarm'
          Color = 5351679
          ParentColor = False
          TabOrder = 2
          OnClick = BtnAlarmClick
        end
      end
    end
  end
  object LabelHelp: TMemo
    Left = 8
    Top = 336
    Width = 409
    Height = 25
    BorderStyle = bsNone
    Ctl3D = False
    Lines.Strings = (
      'LabelHelp')
    ParentCtl3D = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    WantReturns = False
  end
end
