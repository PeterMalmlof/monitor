unit TInfoFormUnit;

interface

uses
  Windows, Messages,  Variants, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Classes, ImgList, Math,

  TGenAppPropUnit;     // Application Properties
type
  TInfoForm = class(TForm)
    InfoView: TTreeView;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure InfoViewCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure InfoViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure InfoViewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure InfoViewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FFormRect  : TGenAppPropRect;   // Form Position and Size
    FSplitter  : TGenAppPropInt;    // Splitter X Position
    FMouseDown : boolean;           // Splitter is Moving

  protected

    procedure DrawDotLine(const p1,p2 : TPoint);
  
    procedure DrawPlus(
              const Pos : TPoint;
              const Hgt : integer;
              const Typ : integer);

    procedure Log(const Line : string);
  public

    // Add one Value to a TreeNode

    function AddInfo(
        const nHead  : TTreeNode;   // Header Node is it exist
        const sHead  : string;      // Header Text if Node dont exist
        const sTitle : string = ''; // Value Title
        const sValue : string = ''; // Value
        const sDesc  : string = '') // Value Description
                     : TTreeNode;   // Used Header Node

    // Add a WMI class

    procedure AddWmiClass(
              const WmiClass : string;
              const sWhere   : string;
              const sTitle   : string;
              const sId      : string);

    procedure Clear;
end;

var
  InfoForm: TInfoForm;

implementation

{$R *.dfm}

uses
  SysUtils,
  StrUtils,

  TGenStrUnit,
  TPmaWmiUnit,
  TPmaLogUnit,
  TPmaClassesUnit;

const
  prfPref     = 'InformationForm';
  prfRect     = 'Rect';
  prfSplitter = 'Slitter';

  SPLITTER_MIN  = 120;
  SPLITTER_TRAP = 4;

//------------------------------------------------------------------------------
//  Form is Created
//------------------------------------------------------------------------------
procedure TInfoForm.FormCreate(Sender: TObject);
begin
  self.DoubleBuffered := true;
  InfoView.DoubleBuffered := true;

  FFormRect := App.CreateProp(
                TGenAppPropRect, prfPref, prfRect) as TGenAppPropRect;
  FFormRect.SetBounds(self);

  FSplitter := App.CreateProp(
                TgenAppPropInt, prfPref, prfSplitter) as TgenAppPropInt;
end;
//------------------------------------------------------------------------------
//  Form is Destroyed
//------------------------------------------------------------------------------
procedure TInfoForm.FormDestroy(Sender: TObject);
begin

  FFormRect.GetBounds(self);

  // Release it from Parent Window

  if Assigned(self.Owner) then
    begin
      Owner.RemoveComponent(self);
    end;
end;
//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------
procedure TInfoForm.Clear;
begin
  InfoView.Items.Clear;
end;
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TInfoForm.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Form is Resized
//------------------------------------------------------------------------------
procedure TInfoForm.FormResize(Sender: TObject);
begin
  InfoView.Left   := 0;
  InfoView.Width  := self.ClientWidth;
  InfoView.Top    := 0;
  InfoView.Height := self.ClientHeight;

  if Assigned(FSplitter) and
    (FSplitter.pInt > (self.ClientWidth - SPLITTER_MIN)) then
    begin
      FSplitter.pInt := (self.ClientWidth - SPLITTER_MIN);
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//  Draw Item
//------------------------------------------------------------------------------
procedure TInfoForm.DrawDotLine(const p1,p2 : TPoint);
var
  Ind : integer;
begin
  if (p1.X = p2.X) then
    begin
      for Ind := p1.Y to p2.Y do
        if Odd(Ind) then
          InfoView.Canvas.Pixels[p1.X, Ind] := RGB(64,64,64);
    end
  else
    begin
      for Ind := p1.X to p2.X do
        if Odd(Ind) then
          InfoView.Canvas.Pixels[Ind, p1.Y] := RGB(64,64,64);
    end
end;
//------------------------------------------------------------------------------
//  Draw Item
//------------------------------------------------------------------------------
procedure TInfoForm.DrawPlus(
              const Pos : TPoint;
              const Hgt : integer;
              const Typ : integer);
begin
  case Typ of
    0 : begin
          // Draw a Plus Sign

          InfoView.Canvas.Pen.Color := RGB(64,64,64);
          InfoView.Canvas.Pen.Width := 1;
          InfoView.Canvas.Pen.Style := psSolid;

          InfoView.Canvas.MoveTo(Pos.X +  4, Pos.Y +  4);
          InfoView.Canvas.LineTo(Pos.X + 12, Pos.Y +  4);
          InfoView.Canvas.LineTo(Pos.X + 12, Pos.Y + 12);
          InfoView.Canvas.LineTo(Pos.X +  4, Pos.Y + 12);
          InfoView.Canvas.LineTo(Pos.X +  4, Pos.Y +  4);

          InfoView.Canvas.MoveTo(Pos.X +  6, Pos.Y +  8);
          InfoView.Canvas.LineTo(Pos.X + 11, Pos.Y +  8);

          InfoView.Canvas.MoveTo(Pos.X +  8, Pos.Y +  6);
          InfoView.Canvas.LineTo(Pos.X +  8, Pos.Y + 11);
        end;
    1 : begin
          // Draw a Minus Sign

          InfoView.Canvas.Pen.Color := RGB(64,64,64);
          InfoView.Canvas.Pen.Width := 1;
          InfoView.Canvas.Pen.Style := psSolid;

          InfoView.Canvas.MoveTo(Pos.X +  4, Pos.Y +  4);
          InfoView.Canvas.LineTo(Pos.X + 12, Pos.Y +  4);
          InfoView.Canvas.LineTo(Pos.X + 12, Pos.Y + 12);
          InfoView.Canvas.LineTo(Pos.X +  4, Pos.Y + 12);
          InfoView.Canvas.LineTo(Pos.X +  4, Pos.Y +  4);

          InfoView.Canvas.MoveTo(Pos.X +  6, Pos.Y +  8);
          InfoView.Canvas.LineTo(Pos.X + 11, Pos.Y +  8);
        end;
    2 : begin
          // Draw a Through Line with a T

          DrawDotLine(Point(4 + Pos.X + hgt div 2, Pos.Y),
                      Point(4 + Pos.X + hgt div 2, Pos.Y + hgt));

          DrawDotLine(Point(4 + Pos.X + hgt div 2, Pos.Y + hgt div 2),
                      Point(4 + Pos.X + hgt, Pos.Y + hgt div 2));

        end;
    3 : begin
          // Draw a L Line

          DrawDotLine(Point(4 + Pos.X + hgt div 2, Pos.Y),
                      Point(4 + Pos.X + hgt div 2, Pos.Y + hgt Div 2));

          DrawDotLine(Point(4 + Pos.X + hgt div 2, Pos.Y + hgt div 2),
                      Point(4 + Pos.X + hgt, Pos.Y + hgt div 2));

        end;
  end;
end;
//------------------------------------------------------------------------------
//  Draw Item
//------------------------------------------------------------------------------
procedure TInfoForm.InfoViewCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
var
  NodeRect : TRECT;
  TextRect : TRECT;
  nIndex   : integer;
  Ind      : integer;
  bValue   : boolean;
  sTmp : string;
begin
  // Draw the Text

  DefaultDraw := false;

  // Get total Item Rect

  NodeRect := Node.DisplayRect(false);

  NodeRect.Right := NodeRect.Left + 20;
  
  InfoView.Canvas.Brush.Color := RGB(255,255,255);
  InfoView.Canvas.FillRect(NodeRect);

  // Decide What symbol to draw, then Draw it
  
  nIndex := 2;
  if (Node.Count > 0) then
    begin
      if Node.Expanded then
        nIndex := 1
      else
        nIndex := 0;
    end
  else
    begin
      // Either a T or an L

      if (Node.Parent <> nil) and
         (Node.Parent.Count = (Node.Index+1)) then
        nIndex := 3;
    end;

  DrawPlus(Point(NodeRect.Left + Node.Level * 16, NodeRect.Top),
            (NodeRect.Bottom - NodeRect.Top), nIndex);

  // Get Display Rect of the Text only

  NodeRect := Node.DisplayRect(True);

  // Draw Background

  if Node.Level = 0 then
    begin
      InfoView.Canvas.Brush.Color := RGB(255,255,255);
    end
  else
    begin
      // Draw with Yellow (light, lighter) every odd line

      if Odd(Node.Index) then
        InfoView.Canvas.Brush.Color := RGB(245,245,245)
      else
        InfoView.Canvas.Brush.Color := RGB(255,255,255)
    end;

  InfoView.Canvas.FillRect( Rect(
        NodeRect.Left, NodeRect.Top,
        self.ClientWidth - 2, NodeRect.Bottom));

  //----------------------------------------------------------------------------
  // Draw Text
  //----------------------------------------------------------------------------

  Windows.SetBkMode(InfoView.Canvas.Handle, Windows.TRANSPARENT);
  InfoView.Canvas.Font := self.Font;
  TextRect := NodeRect;

  // Find Name = Value text and Draw both
  
  bValue := false;
  for Ind := 1 to length(Node.Text) do
    if (Node.Text[Ind] = '=') then
      begin
        // Draw Name

        sTmp := trim(AnsiLeftStr(Node.Text, Ind -1));
        TextRect.Right := FSplitter.pInt;

        DrawTextEx(InfoView.Canvas.Handle, PAnsiChar(sTmp), -1, TextRect,
            DT_LEFT or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS, nil);

        // Draw Value

        sTmp := trim(AnsiRightStr(Node.Text, length(Node.Text)-Ind));
        TextRect.Left  := FSplitter.pInt;
        TextRect.Right := InfoView.ClientWidth - 2;

        DrawTextEx(InfoView.Canvas.Handle, PAnsiChar(sTmp), -1, TextRect,
            DT_LEFT or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS, nil);

        bValue := true;
      end;

  // If No Name Value was found draw NodeText as Title

  if (not bValue) then
    begin

      DrawTextEx(InfoView.Canvas.Handle, PAnsiChar(Node.Text), -1, TextRect,
            DT_LEFT or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS, nil);
    end;

end;
//------------------------------------------------------------------------------
//  Add Information on a TreeNode
//------------------------------------------------------------------------------
function TInfoForm.AddInfo(
        const nHead  : TTreeNode;   // Header Node is it exist
        const sHead  : string;      // Header Text if Node dont exist
        const sTitle : string = ''; // Value Title
        const sValue : string = ''; // Value
        const sDesc  : string = '') // Value Description
                     : TTreeNode;   // Used Header Node
var
  pChild : TTreeNode;
begin
  // Set Header or Create it

  if (nHead <> nil) then

    // We use this Node to add Childre

    result := nHead
  else
    // We Create a New Orphan Node

    result := InfoView.Items.Add(nil, sHead);

  // Add Value Node

  if (result <> nil) and (length(sTitle) > 0) then
    begin
      // Add a Child Node and return that

      pChild := InfoView.Items.AddChild(result, sTitle);
      if (length(sValue) > 0) then
        pChild.Text := sTitle + ' = ' + sValue;
    end;
end;
//------------------------------------------------------------------------------
// Add an Wmi Class to Info Dialog
//------------------------------------------------------------------------------
procedure TInfoForm.AddWmiClass(
              const WmiClass : string;
              const sWhere   : string;
              const sTitle   : string;
              const sId      : string);

  function IsWmiClass(const sLine : string): boolean;
  var
    I : integer;
  begin
    result := false;
    for I := 1 to length(sLine) do
      if sLine[I] = '=' then
        if AnsiSameText('WMI Class', trim(AnsiLeftStr(sLIne, I-1))) then
          begin
            result := true;
            BREAK;
          end;
  end;

  function GetName(const sLine : string): string;
  var
    I : integer;
  begin
    result := '';
    for I := 1 to length(sLine) do
      if sLine[I] = '=' then
          begin
            result := trim(AnsiLeftStr(sLine, I - 1));
            //TheLog.Log('GetName ' + result);
            BREAK;
          end;
  end;

  function GetValue(const sLine : string): string;
  var
    I : integer;
  begin
    result := '';
    for I := 1 to length(sLine) do
      if sLine[I] = '=' then
          begin
            result := trim(AnsiRightStr(sLIne, length(sLine) -  I));
            //TheLog.Log('GetVale ' + result);
            BREAK;
          end;
  end;

var
  StrList : TStringList;
  Ind     : integer;
  pBase   : TTreeNode;
  sTmp : string;
  I : integer;
begin
  StrList := TStringList.Create;
  pBase := nil;

  if WalkWmiClass(WmiClass, sTitle, StrList, sWhere) then
    begin
      for Ind := 0 to StrList.Count - 1 do
        begin
          // Is the String WMI Class= then add a Second Level Node

          if IsWmiClass(StrList[Ind]) then
            begin
              //TheLog.Log('New Class ' + StrList[Ind]);

              // If sId then find the Next

              sTmp := '';

              if (length(sId) > 0) then
                begin
                  for I := Ind + 1 to StrList.Count - 1 do
                    if AnsiSameText(sId, GetName(StrList[I])) then
                      begin
                        sTmp := GetValue(StrList[I]);
                        //TheLog.Log('Got Id ' + StrList[I]);
                        BREAK;
                      end;
                end;

              pBase := self.AddInfo(nil, sTitle + ' ' +sTmp, StrList[Ind]);
            end
          else
            begin
              // Use the Node you got already

              if (pBase <> nil) then
                self.AddInfo(pBase, sTitle, StrList[Ind]);

            end
        end;
    end;

  StrList.Free;
end;
//------------------------------------------------------------------------------
//  Form is Closing
//------------------------------------------------------------------------------
procedure TInfoForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//------------------------------------------------------------------------------
//  Mouse Down
//------------------------------------------------------------------------------
procedure TInfoForm.InfoViewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (ssLeft in Shift) and
     (X > (FSplitter.pInt - SPLITTER_TRAP)) and
     (X < (FSplitter.pInt + SPLITTER_TRAP)) then
    begin
      FMouseDown := true;
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Move
//------------------------------------------------------------------------------
procedure TInfoForm.InfoViewMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if FMouseDown then
    begin
      FSplitter.pInt := Max(SPLITTER_MIN,
                        Min(self.ClientWidth - SPLITTER_MIN, X));
      infoView.Invalidate;
    end
  else
    begin
      if (X > (FSplitter.pInt - SPLITTER_TRAP)) and
         (X < (FSplitter.pInt + SPLITTER_TRAP)) then
        InfoView.Cursor := crHSplit
      else
        InfoView.Cursor := crDefault;
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Down
//------------------------------------------------------------------------------
procedure TInfoForm.InfoViewMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FMouseDown := false;
end;
//------------------------------------------------------------------------------
//                                   INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TInfoForm);
end.
