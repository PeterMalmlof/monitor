unit TMainMonitorUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus,    Contnrs,  ImgList,  ExtCtrls,

  TMonBaseWinUnit,            // TMonBaseWinUnit          Base Window

  TMonCpuWinUnit,             // TMonCpuWinUnit             Cpu Window
                              // TPmaCpuUtilsUnit           Cpu Utilities
  TPmaCpuUnit,                // TPmaCpuUnit                Cpu Object
  TPmaCpuTempUnit,            // TPmaCpuTempUnit            Cpu Core Temp Object
                              // TMonCpuThreadUnit          Cpu Thread Unit

  TPmaProcessUnit,            // TPmaProcessUnit            Process Object
  TPmaProcessListUnit,        // TPmaProcessListUnit        Process List object
  TPmaProcessListDescUnit,    // TPmaProcessListDescUnit    Process Descriptions
  TPmaProcessUtils,           // TPmaProcessUtils           Process Utilities
  TMonProcessListThreadUnit,  // TMonProcessListThreadUnit  Process Thread

  TMonMemWinUnit,             // TMonMemWinUnit             Memory Window
                              // TPmaMemUtilsUnit           Memory Utiities
  TPmaMemUnit,                // TPmaMemUnit                Memory Object
                              // TMonMemThreadUnit          Memory

  TMonNetWinUnit,             // TMonNetWinUnit             Network Window
                              // TPmaNetworkUnit            Network Object
                              // TMonNetThreadUnit          Network Thread

  TMonDriveWinUnit,           // TMonDriveWinUnit           Drive Window
                              // TGenFileSystemUnit         Drive Object
                              // TMonDriveThreadUnit        Drive Thread

  TMonFileUnit,   // Monitor Values File Management
  TValueFormUnit, // Value Form
  TProcFormUnit,  // Process Form
  TInfoFormUnit,  // Information Form

  TGenAppPropUnit,      // Application  TGenAppPropUnit
  TPmaLogUnit,          // Log
  TGenPopupMenuUnit,    // Popup Menu
  TWmMsgFactoryUnit,    // Message Factory     TWmMsgFactoryUnit
  TBkgThreadQueueUnit,  // Background Threads
  TGenHintManagerUnit,  // Hint Manager
  TGenFileSystemUnit,   // File Manager

  TPmaTimerUnit, StdCtrls;
  // TGenColorPickUnit
  // TPmaDateTimeUnit

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
type
  TMainMonitor = class(TForm)
    AppProp: TAppProp;
    LogComp: TPmaLog;
    AppMenu: TGenPopupMenu;
    WmMsgFactory1: TWmMsgFactory;
    BkgThreadQueue1: TBkgThreadQueue;
    GenHintManager: TGenHintManager;
    GenFileSystem: TGenFileSystem;
    Images: TImageList;
    procedure Log(Line: String);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AppMenuPopup(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure AppPropFontSet;
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSubscriber : TWmMsgSubscriber;  // Message Subscriber
    FStartTick  : cardinal;          // Tick when Monitor Started
    FStartTime  : TFileTime;         // FileTime when Monitor Started
    FWinList    : TObjectList;       // All Windows
    FCurWin     : TMonBaseWin;       // Current Window
    FCpuWin     : TMonCpuWin;
    FValues     : TMonFile;          // Value Handler

    FMouseDown  : boolean;           // Move has Started
    FMousePos   : TPoint;            // Position When Move Started
    FMouseMoved : boolean;           // It was Moved

    // Application Properties

    FLocked : TGenAppPropBool;  // Locked or Movable
    FZOrder : TGenAppPropInt;   // 0 = Free, 1 = On Top, 2 = On Bottom
    FSize   : TGenAppPropInt;   // Relative Size
    FSpeed  : TGenAppPropInt;   // Refresh Speed

    FStartTimer   : TTimer;
    FUserInit     : boolean;
    FUserInitTick : cardinal;
  protected

    procedure OnStart(Sender : TObject);

    // Start Subscribtions and Such

    procedure StartUp;
    procedure ShutDown;

    procedure MsgProcess(var Msg : TMsg);
    
    // Startup all Default Windows

    procedure StartUpWindows;
    procedure RefreshWindows;

    // Resize and Position Current Windows

    procedure ResizeWindows;

    // Return Window under a Position

    function WinByPos(const Value : TPoint):TMonBaseWin;

    // Set new Hint Position

    procedure SetHintPos(const Pos : TPoint);

    // Device Change Message Handler (Uses SendMessage)

    procedure WMDeviceChange(Var Msg : TMessage); message WM_DEVICECHANGE;

    // Return Value Form if it exists

    function  GetValueForm : TValueForm;

    //--------------------------------------------------------------------------
    // Menu Commands
    //--------------------------------------------------------------------------

    procedure OnCloseMe         (Sender : TObject);
    procedure OnOpenValueForm   (Sender : TObject);
    procedure OnStressCpu       (Sender : TObject);
    procedure OnDefreagBoot     (Sender : TObject);
    procedure OnLocked          (Sender : TObject);
    procedure OnZOrder          (Sender : TObject);
    procedure OnRelSize         (Sender : TObject);
    procedure OnOpenProcessForm (Sender : TObject);
    procedure OnProcessAlarm    (Sender : TObject);
    procedure OnSpeed           (Sender : TObject);
  public
    
  end;

var
  MainMonitor: TMainMonitor;

implementation

{$R *.dfm}

uses
  TGenStrUnit,       // String Functions
  TPmaDateTimeUNit,  // TPmaDateTime
  TPmaNetworkUnit,   // Network Utilities
  TPmaFormUtils,     // Form Utilities
  TGenGraphicsUnit,  // Graphic Functions
  TPmaClassesUnit;   // Classes

const
  BRD      = 5;

  prfPref   = 'Preferences';
  prfLocked = 'Locked';
  prfZOrder = 'ZOrder';
  prfSize   = 'Size';
  prfSpeed  = 'Speed';

//------------------------------------------------------------------------------
//
//                                  STARTUP PROCEDURES
//
//------------------------------------------------------------------------------
//  Create Main Form
//------------------------------------------------------------------------------
procedure TMainMonitor.FormCreate(Sender: TObject);
var
  st : TSystemTime;
begin
  Log('');
  Log('MainForm Opening');
  PmaLog.LogMem('');

  Randomize;

  FStartTick := Windows.GetTickCount;
  GetLocalTime(st);
  SystemTimeToFileTime(st, FStartTime);

  Log('  Start Time ' + TPmaDateTime.FileTimeToFullStr(FStartTime));
  Log('  Boot Tick  ' + IntToStr(FStartTick div 1000));

  FValues := nil;

  //----------------------------------------------------------------------------
  // Startup Application
  //----------------------------------------------------------------------------

  App.StartUp;
  App.pAutoSave := true;
  PmaLog.LogMem(' ');

  FWinList := TObjectList.Create(true);

  //self.Startup;
  GenHintManager.Startup;

  //----------------------------------------------------------------------------
  // Opened
  //----------------------------------------------------------------------------

  Log('');
  Log('MainForm Opened');
  PmaLog.LogMem(' ');

  Log('Start Time ' + IntToStr((Windows.GetTickCount div 1000)));

  if (FStartTick < 100000) then
    begin
      FStartTimer := TTimer.Create(nil);
      FStartTimer.OnTimer  := OnStart;
      FStartTimer.Enabled  := true;

      FStartTimer.Interval := 200;
    end
  else
    begin
      // Start Drive and Cpu Temp

      PostMsg(MSG_FS_DRIVE_REFRESH,0,0);
      PostMsg(MSG_CPU_TEMP_INIT,0,0);
    end;

  FUserInit := false;
  FUserInitTick := 0;

  StartUp;

  Log(StringOfChar('-',80));
  self.Invalidate;
end;
//------------------------------------------------------------------------------
// Startup Subscribtions and Such
//------------------------------------------------------------------------------
Procedure TMainMonitor.OnStart(Sender : TObject);
var
  Ind : integer;
begin

  // We need to wait at least 5 seconds before starting some functions

  if (Windows.GetTickCount > (FStartTick + 5000)) then
    begin
      FStartTimer.Enabled := false;
      
      // Need to let other things work for awhile

      for Ind := 0 to 10 do
        begin
          Application.ProcessMessages;
          sleep(10);
        end;

      PostMsg(MSG_FS_DRIVE_REFRESH,0,0);
      PostMsg(MSG_CPU_TEMP_INIT,0,0);
    end;
end;
//------------------------------------------------------------------------------
// Startup Subscribtions and Such
//------------------------------------------------------------------------------
Procedure TMainMonitor.StartUp;
begin

  FValues := TMonFile.Create;
  FValues.StartUp;

  // Create and Read Application Properties

  FLocked := App.CreatePropBool(prfPref, prfLocked, false);

  FZOrder := App.CreatePropInt(prfPref, prfZOrder, Z_FREE);
  TPmaFormUtils.SetWindowZOrder(self.Handle, FZOrder.pInt);

  FSize := App.CreatePropInt(prfPref, prfSize, SIZE_NORMAL);

  FSpeed := App.CreatePropInt(prfPref, prfSpeed, rsNormal);


  //----------------------------------------------------------------------------
  // Startup Components
  //----------------------------------------------------------------------------

  Log('');
  Log('Component StartUp:');

  if Assigned(MsgFactory) then
    begin
      FSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProcess);

      // Handle when new Drives Arrive or existing beeing Removed

      FSubscriber.AddMessage(MSG_FS_DRIVE_ARRIVED);
      FSubscriber.AddMessage(MSG_FS_DRIVE_REMOVED);

      // Manage a Process Alarm

      FSubscriber.AddMessage(MSG_PROC_ALARM);
    end;

  AppMenu.StartUp;

  FileSystem.StartUp;
  PmaLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // Startup Process Management
  //----------------------------------------------------------------------------

  Log('');
  Log('ProcessList StartUp:');

  TPmaProcessListDesc.Startup;
  PmaLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // Create all Windows
  //----------------------------------------------------------------------------

  Log('');
  Log('Windows Startup:');

  StartUpWindows;
  FCurWin := nil;
  PmaLog.LogMem(' ');

  Log('');
  Log('Process Thread Startup:');

  TMonProcessListThread.Create(22);

  sleep(200);

  if Assigned(CpuFactory) then
    CpuFactory.pRunSpeed := FSpeed.pInt;

  if Assigned(MemoryFactory) then
    MemoryFactory.pRunSpeed := FSpeed.pInt;

  if Assigned(NetworkFactory) then
    NetworkFactory.pRunSpeed := FSpeed.pInt;

  if Assigned(Processes) then
    Processes.pRunSpeed := FSpeed.pInt;

  if Assigned(FileSystem) then
    FileSystem.pRunSpeed := FSpeed.pInt;



  //StartText.Visible := false;

end;
//------------------------------------------------------------------------------
//  Startup All Windows
//------------------------------------------------------------------------------
procedure TMainMonitor.StartUpWindows;
var
  pWin : TMonBaseWin;
begin
  // Create all Fixed Windows

  FCpuWin := TMonCpuWin.Create(self.Canvas, Images, FStartTick, FUserInitTick);
  FWinList.Add(FCpuWin);

  pWin := TMonMemWin.Create(self.Canvas, Images);
  FWinList.Add(pWin);

  pWin := TMonNetWin.Create(self.Canvas, Images);
  FWinList.Add(pWin);

  ResizeWindows;
end;
//------------------------------------------------------------------------------
//  On Show
//------------------------------------------------------------------------------
procedure TMainMonitor.FormShow(Sender: TObject);
begin
  ShowWindow(GetWindow(Handle, GW_OWNER), SW_HIDE);
end;
//------------------------------------------------------------------------------
//
//                                  SHUTDOWN PROCEDURES
//
//------------------------------------------------------------------------------
// Close Query
//------------------------------------------------------------------------------
procedure TMainMonitor.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := true;
end;
//------------------------------------------------------------------------------
// Form Is Closing Down
//------------------------------------------------------------------------------
procedure TMainMonitor.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  pForm : TForm;
begin
  Action := caFree;

  if Assigned(FStartTimer) then
    FStartTimer.Free;

  //----------------------------------------------------------------------------
  // Close Opened Forms
  //----------------------------------------------------------------------------

  pForm := FindForm(Application.MainForm, TProcForm);
  if Assigned(pForm) then
    begin
      // It exist aleady, close it

      pForm.Close
    end;

  pForm := FindForm(Application.MainForm, TValueForm);
  if Assigned(pForm) then
    begin
      // It exist aleady, close it

      pForm.Close
    end;

  //----------------------------------------------------------------------------
  // Log All Factories
  //----------------------------------------------------------------------------

  Log(StringOfChar('-',80));
  Log('MainForm Closing');
  PmaLog.LogMem(' ');

  if Assigned(MemoryFactory) then
    MemoryFactory.LogIt;

  if Assigned(NetworkFactory) then
    NetworkFactory.LogIt;

  if Assigned(CpuFactory) then
    CpuFactory.LogIt;

  if Assigned(CpuTempFactory) then
    CpuTempFactory.LogIt;

  if Assigned(FileSystem) then
    FileSystem.LogDrives;

  //----------------------------------------------------------------------------
  // ShutDown Background Queue
  //----------------------------------------------------------------------------

  BkgQueue.TerminatAllProcesses;
  PmaLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // ShutDown Application
  //----------------------------------------------------------------------------

  App.ShutDown;

  //----------------------------------------------------------------------------
  // ShutDown Components
  //----------------------------------------------------------------------------

  self.ShutDown;

  AppMenu.ShutDown;
  GenHintManager.ShutDown;
  FileSystem.ShutDown;

  Processes.LogIt;
  TPmaProcessList.ShutDown;
  TPmaProcessListDesc.ShutDown;
  PmaLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // ShutDown all Windows
  //----------------------------------------------------------------------------

  FWinList.Free;
  PmaLog.LogMem(' ');

  FValues.ShutDown;
  FValues.Free;
  PmaLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // Log Message Factory
  //----------------------------------------------------------------------------

  MsgFactory.LogIt;
  PmaLog.LogMem(' ');

  //----------------------------------------------------------------------------
  // ShutDown Classes
  //----------------------------------------------------------------------------

  ClassFactory.SaveandClose;
  PmaLog.LogMem(' ');

end;
//------------------------------------------------------------------------------
// Startup Subscribtions and Such
//------------------------------------------------------------------------------
Procedure TMainMonitor.ShutDown;
begin
  if Assigned(FSubscriber) then
    begin
      MsgFactory.DeSubscribe(FSubscriber);
    end;
end;
//------------------------------------------------------------------------------
//  Destroy Main Form
//------------------------------------------------------------------------------
procedure TMainMonitor.FormDestroy(Sender: TObject);
begin


  Log('');
  Log('MainForm Closed');
end;

//------------------------------------------------------------------------------
//
//                                  RUNNING PROCEDURES
//
//------------------------------------------------------------------------------
// Startup Subscribtions and Such
//------------------------------------------------------------------------------
Procedure TMainMonitor.MsgProcess(var Msg : TMsg);
begin
  case Msg.message of

    MSG_FS_DRIVE_ARRIVED : RefreshWindows;
    MSG_FS_DRIVE_REMOVED : RefreshWindows;
    MSG_PROC_ALARM       : OnProcessAlarm(nil);
  end;
end;
//------------------------------------------------------------------------------
// Event: Some Device has changed
//------------------------------------------------------------------------------
Procedure TMainMonitor.WMDeviceChange(Var Msg: TMessage);
begin
  // Post this Message back to Application

  PostMsg(MSG_FS_DEVICE_CHANGED,Msg.WParam, Msg.LParam);
End;
//------------------------------------------------------------------------------
//  Refresh all Windows (Drives Arrived or Removed
//------------------------------------------------------------------------------
procedure TMainMonitor.RefreshWindows;
var
  Ind   : integer;
  pWin  : TMonBaseWin;
  Iter  : integer;
  pFole : TGenFole;
begin
  // Un Mark all Current Windows

  if (FWinList.Count > 0) then
    for Ind := 0 to FWinList.Count - 1 do
      TMonBaseWin(FWinList[Ind]).pMark := false;

  // Walk all FileSystem Drives

  if Assigned(FileSystem) and Assigned(FileSystem.Computer) then
    begin
      Iter := 0;
      while FileSystem.Computer.IterFoles(Iter, pFole) do
        if (pFole is TGenDrive) then
          begin
            // Find this Drive Window

            pWin := nil;
            if (FWinList.Count > 0) then
              for Ind := FWinList.Count - 1 downto 0 do
                if (not TMonBaseWin(FWinList[Ind]).pMark) and
                   (FWinList[Ind] is TMonDriveWin) and
                   (TMonDriveWin(FWinList[Ind]).pDrive = pFole) then
                  begin
                    pWin := FWinList[Ind] as TMonDriveWin;
                    pWin.pMark := true;
                    BREAK;
                  end;

            // If no Window was Found, Create it

            if (not Assigned(pWin)) then
              begin
                Log('Drive Window Added ' + pFole.pFileName);
                pWin := TMonDriveWin.Create(
                    self.Canvas, Images, pFole as TGenDrive);
                pWin.pMark := true;

                // Add them in Drive Sort order

                for Ind := 0 to FWinList.Count - 1 do
                 if (FWinList[Ind] is TMonDriveWin) and
                    (TMonDriveWin(FWinList[Ind]).pOrder > pWin.pOrder) then
                  begin
                    FWinList.Insert(Ind, pWin);
                    pWin := nil;
                    BREAK;
                  end;

                if Assigned(pWin) then
                  FWinList.Add(pWin);
              end;
          end;
    end;

  // Remove all still Unmarked Windows

  if (FWinList.Count > 0) then
    for Ind := FWinList.Count - 1 downto 0 do
      if (not TMonBaseWin(FWinList[Ind]).pMark) and
         (FWinList[Ind] is TMonDriveWin) then
        begin
          Log('Drive Window Removed ' +
            TMonBaseWin(FWinList[Ind]).pTopText);
          FWinList.Delete(Ind);
        end;

  ResizeWindows;
end;
//------------------------------------------------------------------------------
//
//                                   RESIZING
//
//------------------------------------------------------------------------------
//  Resize Windows
//------------------------------------------------------------------------------
procedure TMainMonitor.ResizeWindows;
var
  Size : TSize;
  Ind  : integer;
  L    : integer;
begin
  // Get Size depending on Font

  Size := TMonBaseWin.CalcSize(FSize.pInt, self.Canvas);

  // Set Sizes on all Windows

  if (FWinList.Count > 0) then
    begin
      L := 0;

      for Ind := 0 to FWinList.Count - 1 do
        begin
          TMonBaseWin(FWinList[Ind]).pRect :=
            Rect (L, 0, L + Size.cx, Size.cy);

          Inc(L, Size.cx + BRD);
        end;
    end;

  self.Height := Size.cy + self.Height - self.ClientHeight;
  self.Width  := FWinList.Count * (Size.cx + BRD) - BRD +
                  self.Width  - self.ClientWidth;

  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Get Window from Position
//------------------------------------------------------------------------------
function TMainMonitor.WinByPos(const Value : TPoint):TMonBaseWin;
var
  Ind  : integer;
begin
  result := nil;

  if (FWinList.Count > 0) then
    for Ind := 0 to FWinList.Count - 1 do
      if PtInRect(TMonBaseWin(FWinList[Ind]).pRect, Value) then
        begin
          result := FWinList[Ind] as TMonBaseWin;
          BREAK;
        end;
end;
//------------------------------------------------------------------------------
//  The Application Font has Changed
//------------------------------------------------------------------------------
procedure TMainMonitor.AppPropFontSet;
begin
  ResizeWindows;
end;
//------------------------------------------------------------------------------
//
//                                      MISC
//
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TMainMonitor.Log(Line: String);
begin
  PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//
//                                     MENUS
//
//------------------------------------------------------------------------------
//  Pop App Menu
//------------------------------------------------------------------------------
procedure TMainMonitor.AppMenuPopup(Sender: TObject);
var
  pPref : TGenMenuItem;
  pMenu : TGenMenuItem;
begin
  AppMenu.Font := self.Font;
  AppMenu.Items.Clear;

  if Assigned(FCurWin) and (FCurWin is TMonCpuWin) then
    begin
      // Open / CLose Value Form

      pPref := TGenMenuItem.Create(AppMenu);
      if (GetValueForm <> nil) then
        pPref.Caption := 'Close Values '
      else
        pPref.Caption := 'Open Values ';

      pPref.OnClick := OnOpenValueForm;
      AppMenu.Items.Add(pPref);

      // Open / CLose Process Form

      pPref := TGenMenuItem.Create(AppMenu);
      if (FindForm(Application.MainForm, TProcForm) <> nil) then
        pPref.Caption := 'Close Processes '
      else
        pPref.Caption := 'Open Processes... ';

      pPref.OnClick := OnOpenProcessForm;
      AppMenu.Items.Add(pPref);

      // Add preferences Menu

      pPref := TGenMenuItem.Create(AppMenu);
      pPref.Caption := 'Preferences ';
      AppMenu.Items.Add(pPref);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := 'Locked ';
        pMenu.OnClick := OnLocked;
        pMenu.Checked := FLocked.pBool;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := '-';
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Free Order ';
        pMenu.OnClick   := OnZOrder;
        pMenu.RadioItem := true;
        pMenu.Tag       := Z_FREE;
        pMenu.Checked   := FZOrder.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'On Top ';
        pMenu.OnClick   := OnZOrder;
        pMenu.RadioItem := true;
        pMenu.Tag       := Z_TOP;
        pMenu.Checked   := FZOrder.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'On Bottom ';
        pMenu.OnClick   := OnZOrder;
        pMenu.RadioItem := true;
        pMenu.Tag       := Z_BOTTOM;
        pMenu.Checked   := FZOrder.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := '-';
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Small Size ';
        pMenu.OnClick   := OnRelSize;
        pMenu.RadioItem := true;
        pMenu.Tag       := SIZE_SMALL;
        pMenu.Checked   := FSize.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Normal Size ';
        pMenu.OnClick   := OnRelSize;
        pMenu.RadioItem := true;
        pMenu.Tag       := SIZE_NORMAL;
        pMenu.Checked   := FSize.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Big Size ';
        pMenu.OnClick   := OnRelSize;
        pMenu.RadioItem := true;
        pMenu.Tag       := SIZE_BIG;
        pMenu.Checked   := FSize.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := '-';
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Pause ';
        pMenu.OnClick   := OnSpeed;
        pMenu.RadioItem := true;
        pMenu.Tag       := rsPause;
        pMenu.Checked   := FSpeed.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Slow ';
        pMenu.OnClick   := OnSpeed;
        pMenu.RadioItem := true;
        pMenu.Tag       := rsSlow;
        pMenu.Checked   := FSpeed.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Normal ';
        pMenu.OnClick   := OnSpeed;
        pMenu.RadioItem := true;
        pMenu.Tag       := rsNormal;
        pMenu.Checked   := FSpeed.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption   := 'Fast ';
        pMenu.OnClick   := OnSpeed;
        pMenu.RadioItem := true;
        pMenu.Tag       := rsFast;
        pMenu.Checked   := FSpeed.pInt = pMenu.Tag;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := '-';
        pPref.Add(pMenu);

        // Add Application Generic Preferences Menus

        App.AddPrefMenu(AppMenu, pPref, false, true);
    end;

  // Add Window Dependent Menus

  if Assigned(FCurWin) then
    FCurWin.AddMenu(AppMenu);

 if Assigned(FCurWin) and (FCurWin is TMonCpuWin) then
    begin
      pPref := TGenMenuItem.Create(AppMenu);
      pPref.Caption := 'Utilities ';
      AppMenu.Items.Add(pPref);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := 'Stress Cpu ';
        pMenu.OnClick := OnStressCpu;
        pPref.Add(pMenu);

        pMenu := TGenMenuItem.Create(AppMenu);
        pMenu.Caption := 'Defrag Boot ';
        pMenu.OnClick := OnDefreagBoot;
        pPref.Add(pMenu);
    end;

  pPref := TGenMenuItem.Create(AppMenu);
  pPref.Caption := '-';
  AppMenu.Items.Add(pPref);

  pPref := TGenMenuItem.Create(AppMenu);
  pPref.Caption := 'Exit ';
  pPref.OnClick := OnCloseMe;
  AppMenu.Items.Add(pPref);
end;
//------------------------------------------------------------------------------
//  On Locked
//------------------------------------------------------------------------------
procedure TMainMonitor.OnLocked(Sender : TObject);
begin
  FLocked.Toggle;
end;
//------------------------------------------------------------------------------
//  On Speed
//------------------------------------------------------------------------------
procedure TMainMonitor.OnSpeed(Sender : TObject);
begin
  if Assigned(Sender) and (Sender is TMenuITem) then
    begin
      FSpeed.pInt := TMenuItem(Sender).Tag;

      if Assigned(CpuFactory) then
        CpuFactory.pRunSpeed := FSpeed.pInt;

      if Assigned(MemoryFactory) then
        MemoryFactory.pRunSpeed := FSpeed.pInt;

      if Assigned(NetworkFactory) then
        NetworkFactory.pRunSpeed := FSpeed.pInt;

      if Assigned(Processes) then
        Processes.pRunSpeed := FSpeed.pInt;

      if Assigned(FileSystem) then
        FileSystem.pRunSpeed := FSpeed.pInt;
    end;
end;
//------------------------------------------------------------------------------
//  On Z Order
//------------------------------------------------------------------------------
procedure TMainMonitor.OnZOrder(Sender : TObject);
begin
  if Assigned(Sender) and(Sender is TMenuItem) then
    begin
      FZOrder.pInt := TMenuItem(Sender).Tag;
      TPmaFormUtils.SetWindowZOrder(self.Handle, FZOrder.pInt);
    end;
end;
//------------------------------------------------------------------------------
//  On Relative Size
//------------------------------------------------------------------------------
procedure TMainMonitor.OnRelSize(Sender : TObject);
begin
  if Assigned(Sender) and(Sender is TMenuItem) then
    begin
      FSize.pInt := TMenuItem(Sender).Tag;
      ResizeWindows;
    end;
end;
//------------------------------------------------------------------------------
//  On Stress CPU
//------------------------------------------------------------------------------
procedure TMainMonitor.OnStressCpu(Sender : TObject);
var
  sErr : string;
begin
  DoShellCmd('MyStressApp.exe','', false, sErr);
end;
//------------------------------------------------------------------------------
//  On Boot Defragging
//------------------------------------------------------------------------------
procedure TMainMonitor.OnDefreagBoot(Sender : TObject);
var
  sTmp : string;
  sErr : string;
begin
  // Get Defrag Application String

  sTmp := GetEnvStr('SystemRoot') + '\system32\defrag.exe';
  Log('Defrag Boot: ' + sTmp);
  DoShellCmd(sTmp,'C: -b',false, sErr);
end;
//------------------------------------------------------------------------------
//  On Closing
//------------------------------------------------------------------------------
procedure TMainMonitor.OnCloseMe(Sender : TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
// Return Value Form if it exists
//------------------------------------------------------------------------------
function TMainMonitor.GetValueForm : TValueForm;
var
  Ind : integer;
begin

  result := nil;
  if (self.ComponentCount > 0) then
    for Ind := 0 to self.ComponentCount - 1 do
      if (self.Components[Ind] is TValueForm) then
        begin
          result := self.Components[Ind] as TValueForm;
          BREAK;
        end;
end;
//------------------------------------------------------------------------------
//  Toggle Value Form
//------------------------------------------------------------------------------
procedure TMainMonitor.OnOpenValueForm(Sender : TObject);
var
  ValueForm : TValueForm;
begin
  // Test if this Form is still around

  ValueForm := GetValueForm;

  if Assigned(ValueForm) then
    begin
      ValueForm.Close;
    end
  else
    begin
      FValues.Flush;
      ValueForm := TValueForm.Create(self, FStartTick, FStartTime);
      ValueForm.Show;
    end;
end;
//------------------------------------------------------------------------------
//  Toggle Value Form
//------------------------------------------------------------------------------
procedure TMainMonitor.OnOpenProcessForm(Sender : TObject);
var
  pForm : TForm;
  dlg   : TProcForm;
begin
  pForm := FindForm(Application.MainForm, TProcForm);
  if Assigned(pForm) then
    begin
      // It exist aleady, close it

      pForm.Close
    end
  else
    begin
      // Create it, Initiate it, Show it, and forget about it
  
      dlg := TProcForm.Create(self);
      dlg.Init;
      dlg.Show;
    end;
end;
//------------------------------------------------------------------------------
//  On Process Alarm
//------------------------------------------------------------------------------
procedure TMainMonitor.OnProcessAlarm(Sender : TObject);
var
  pForm : TForm;
  dlg   : TProcForm;
begin
  pForm := FindForm(Application.MainForm, TProcForm);
  if Assigned(pForm) then
    begin
      pForm.Show
    end
  else
    begin
      dlg := TProcForm.Create(self);
      dlg.Init;
      dlg.Show;
    end;
end;
//------------------------------------------------------------------------------
//  User Double CLicked
//------------------------------------------------------------------------------
procedure TMainMonitor.FormDblClick(Sender: TObject);
var
  pDrive : TGenDrive;
  sTmp   : string;
  sErr   : string;
begin
  if Assigned(FCurWin) then
    begin
      if (FCurWin is TMonCpuWin) then
        OnOpenValueForm(Sender)
      else if (FCurWin is TMonDriveWin) then
        begin
          pDrive := TMonDriveWin(FCurWin).pDrive;

          DoShellCmd('explorer.exe',
                     '/e,/root,"' + pDrive.pFileName + '"',
                     false, sErr);
        end
      else
        begin
          sTmp := Trim(GetEnvStr('SystemRoot')) + '\system32\perfmon.msc';
          Log('Env ' + sTmp);
          DoShellCmd(sTmp, '/s', false, sErr);
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//                                  PAINTING MONITOR
//
//------------------------------------------------------------------------------
//  Draw All Windows
//------------------------------------------------------------------------------
procedure TMainMonitor.FormPaint(Sender: TObject);
var
  Ind : integer;
begin
  self.Color := $00D0C5AE;
  self.TransparentColor      := true;
  self.TransparentColorValue := self.Color;

  self.Canvas.Font := self.Font;

  if (FWinList.Count > 0) then
    for Ind := 0 to FWinList.Count - 1 do
      TMonBaseWin(FWinList[Ind]).Paint;
end;
//------------------------------------------------------------------------------
//
//                                  MOVING MONITOR
//
//------------------------------------------------------------------------------
//  Mouse Down Event
//------------------------------------------------------------------------------
procedure TMainMonitor.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  pWin : TMonBaseWin;
begin
  if (ssLeft in Shift) then
    begin
      pWin := self.WinByPos(Point(X,Y));
      if (not FLocked.pBool) and
          Assigned(pWin) and
         (pWin is TMonCpuWin) then
        begin
          FMouseDown  := true;
          FMousePos   := Point(X,Y);
          FMouseMoved := false;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Move Event
//------------------------------------------------------------------------------
procedure TMainMonitor.FormMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
var
  dx, dy : integer;
begin
  if FMouseDown then
    begin
      dx := X - FMousePos.X;
      dy := Y - FMousePos.Y;

      if (Abs(dx) <> 0) or (Abs(dy) <> 0) then
        begin
          self.Left := self.Left + dx;
          self.Top  := self.Top  + dy;
          FCurWin := nil;

          FMouseMoved := true;
        end;
    end;

  SetHintPos(Point(X,Y));
end;
//------------------------------------------------------------------------------
//  Mouse Up Event
//------------------------------------------------------------------------------
procedure TMainMonitor.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FMouseMoved then
    App.MainRectMoved;

  FMouseDown := false;
end;
//------------------------------------------------------------------------------
// Set new Hint Position
//------------------------------------------------------------------------------
procedure TMainMonitor.SetHintPos(const Pos : TPoint);
var
  pWin : TMonBaseWin;
begin
  if Application.ShowHint then
    begin
      pWin := self.WinByPos(Pos);
      if (pWin <> FCurWin) then
        begin
          FCurWin := pWin;

          if Assigned(FCurWin) then
            begin
              self.Hint := FCurWin.pHint;

              if ((self.Top + FCurWin.pRect.Top) >
                 (Screen.DesktopRect.Bottom -
                  Screen.DesktopRect.Top) div 2) then
                begin
                  HintManager.SetHintPos(
                    Point(self.Left + FCurWin.pRect.Left,
                          self.Top  + FCurWin.pRect.Top - 6),
                          hptBottomLeft);
                end
              else
                begin
                  HintManager.SetHintPos(
                    Point(self.Left + FCurWin.pRect.Left,
                          self.Top  + FCurWin.pRect.Bottom + 2),
                    hptTopLeft);
                end;

              Application.HintHidePause := 30000;
              Application.ActivateHint(Mouse.CursorPos);
            end
          else
            self.Hint := '';
        end;
    end;
end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMainMonitor);
end.
