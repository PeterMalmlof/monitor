unit TMonBaseWinUnit;

interface

uses
  Windows,    // Stuff
  SysUtils,   // Stuff
  StdCtrls,   // TMemo
  StrUtils,
  Graphics,
  ExtCtrls,
  Controls,
  Classes,    // TComponent
  Forms,      // TApplication
  Menus,
  Messages,

  TInfoFormUnit,
  TWmMsgFactoryUnit;  // Message Factory

const
  MSG_MON_PAINT = WM_USER + 1800;

const
  ArrowNone  = 0;
  ArrowUp    = 1;
  ArrowDown  = 2;
  ArrowLeft  = 3;
  ArrowRight = 4;

  ImageNone    = -1;
  ImageCdFull  = 0;
  ImageCdEmpty = 1;
  ImageRecycle = 2;

  CRLF = #13#10;
type
  TMonBaseWin = class(TObject)
  private
    FRect      : TRect;    // Window Rectangle
    FMark      : boolean;
    FCanvas    : TCanvas;  // Canvas to Draw on
    FBackColor : TColor;   // Background Color
    FForeColor : TColor;   // Foreground Color
    FHighColor : TColor;   // Highlight Color
    FTopText   : string;   // Top Text
    FMdlText   : string;   // Middle Text
    FBtmText   : string;   // Bottom Text
    FImageId : integer;

    FBars      : integer;          // Number of Bars
    FBarArr    : array of integer; // Bar Position in %
    FBarArrows : array of integer; // Bar Arrow

    FImages : TImageList;
    FTimer  : TTimer;
  protected
    FId         : integer;          // Window Id
    FSubscriber : TWmMsgSubscriber; // Me as a Subscriber

    procedure SetRect(const Value : TRect);
    procedure SetBars(const Value : integer);

    procedure SetTopText(const Value : string);
    procedure SetMdlText(const Value : string);
    procedure SetBtmText(const Value : string);
    procedure SetImageId(const Value : integer);

    procedure MsgProcess(var Msg : TMsg); virtual;

    procedure OnMyTimer(Sender : TObject);

    function GetHint: string; virtual;

    function GetOrder : integer; virtual;

    function  GetInfoForm: TInfoForm;

    procedure Invalidate;

    procedure Log(const Line : string);
  public
    constructor Create(
      const Canvas : TCanvas;
      const Images : TImageList); virtual;

    destructor  Destroy; override;

    procedure StartUp;  virtual;
    procedure ShutDown; virtual;

    // Calculate the Size of the Window based on Font

    class function  CalcSize(
        const Size   : integer;  // Relative Size
        const Canvas : TCanvas)  // Canvas to Draw on
                     : TSize;    // Resulting Size

    // Paint this Window

    procedure Paint;

    procedure SetBarPos   (const Ind, Value : integer);
    procedure SetBarArrow (const Ind, Value : integer);

    procedure AddMenu(const PopupMenu : TPopupMenu); virtual;

    property pId      : integer  read FId;
    property pMark    : boolean  read FMark    write FMark;
    property pRect    : TRect    read FRect    write SetRect;
    property pTopText : string   read FTopText write SetTopText;
    property pMdlText : string   read FMdlText write SetMdlText;
    property pBtmText : string   read FBtmText write SetBtmText;
    property pImageId : integer  read FImageId write SetImageId;
    property pBars    : integer  read FBars    write SetBars;
    property pHint    : string   read GetHint;
    property pOrder   : integer  read GetOrder;
end;

const
  SIZE_SMALL  = 1;
  SIZE_NORMAL = 2;
  SIZE_BIG    = 3;

implementation

uses
  TPmaLogUnit,
  TGenGraphicsUnit,
  TGenAppPropUnit,
  TPmaProcessUtils,
  TPmaFormUtils,
  TPmaClassesUnit;

var
  WindowSeq   : integer = 0;
  WindowCount : integer = 0;

//------------------------------------------------------------------------------
// Create Window
//------------------------------------------------------------------------------
constructor TMonBaseWin.Create(
      const Canvas : TCanvas;
      const Images : TImageList);
begin
  inherited Create;

  FImages := Images;
  FImageId := -1;

  FId := WindowSeq;
  Inc(WindowSeq);
  Inc(WindowCount);

  FCanvas := Canvas;

  FBackColor := App.pBackColor;
  FForeColor := App.pForeColor;
  FHighColor := App.pHighColor;

  FBars := 0;
  FTimer := nil;

  StartUp;
end;
//------------------------------------------------------------------------------
// Log
//------------------------------------------------------------------------------
procedure TMonBaseWin.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
// StartUp
//------------------------------------------------------------------------------
procedure TMonBaseWin.StartUp;
begin
  if Assigned(MsgFactory) then
    begin
      FSubscriber := MsgFactory.Subscribe(
        self.ClassName + IntToStr(FId), MsgProcess);

      // Add Default Messages

      FSubscriber.AddMessage(MSG_UI_BACKCOLOR);
      FSubscriber.AddMessage(MSG_UI_FORECOLOR);
      FSubscriber.AddMessage(MSG_UI_HIGHCOLOR);
      FSubscriber.AddMessage(MSG_MON_PAINT);
    end;

  if (not Assigned(FTimer)) then
    begin
      FTimer := TTimer.Create(nil);
      FTimer.Interval := 1000;
      FTimer.OnTimer  := OnMyTimer;
      FTimer.Enabled  := false;
    end;

  FBackColor := App.pBackColor;
  FForeColor := App.pForeColor;
  FHighColor := App.pHighColor;
end;
//------------------------------------------------------------------------------
// ShutDown
//------------------------------------------------------------------------------
procedure TMonBaseWin.ShutDown;
begin
  if Assigned(FTimer) then
    begin
      FTimer.Enabled := false;
      FTimer.Free;
      FTimer := nil;
    end;

  if Assigned(FSubscriber) then
    begin
      MsgFactory.DeSubscribe(FSubscriber);
    end;
end;
//------------------------------------------------------------------------------
// Destroy Window
//------------------------------------------------------------------------------
destructor TMonBaseWin.Destroy;
begin
  ShutDown;

  FBars := 0;
  SetLength(FBarArr,    FBars);
  SetLength(FBarArrows, FBars);

  Dec(WindowCount);

  inherited;
end;
//------------------------------------------------------------------------------
// Default Message Process
//------------------------------------------------------------------------------
procedure TMonBaseWin.MsgProcess(var Msg : TMsg);
begin
  // Handle Default Messages

  case Msg.message of

    MSG_UI_BACKCOLOR :
      begin
        self.FBackColor := TCOLOR(Msg.lParam);
        Invalidate;
      end;

    MSG_UI_FORECOLOR :
      begin
        self.FForeColor := TCOLOR(Msg.lParam);
        Invalidate;
      end;

    MSG_UI_HIGHCOLOR :
      begin
        self.FHighColor := TCOLOR(Msg.lParam);
        Invalidate;
      end;

    MSG_MON_PAINT : if (Msg.wParam = self.FId) then  Paint;
  end;
end;
//------------------------------------------------------------------------------
// Timer
//------------------------------------------------------------------------------
procedure TMonBaseWin.Invalidate;
begin
  PostMsg(MSG_MON_PAINT, self.FId, 0);
end;
//------------------------------------------------------------------------------
// Timer
//------------------------------------------------------------------------------
procedure TMonBaseWin.OnMyTimer(Sender : TObject);
var
  I : integer;
begin
  FTimer.Enabled := false;

  if (FBars > 0) then
    for I := 0 to FBars - 1 do
      FBarArrows[I] := ArrowNone;
  Invalidate;
end;
//------------------------------------------------------------------------------
// Get Current Ifor Form or Create it
//------------------------------------------------------------------------------
function TMonBaseWin.GetInfoForm: TInfoForm;
begin
  result := FindForm(Application.MainForm, TInfoForm) as TInfoForm;
  if Assigned(result) then
    begin
      result.Clear;
      result.show;
    end
  else
    begin
      result := TInfoForm.Create(Application.MainForm);
      result.Show;
    end;
end;
//------------------------------------------------------------------------------
// Set WIndow Position and Size, and repaint
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetRect(const Value : TRect);
begin
  FRect := Value;
end;
//------------------------------------------------------------------------------
// Set Text and Repaint
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetTopText(const Value : string);
begin
  if (FTopText <> Value) then
    begin
      FTopText := Value;
      Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Set Text and Repaint
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetMdlText(const Value : string);
begin
  if (FMdlText <> Value) then
    begin
      FMdlText := Value;
      Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Set Text and Repaint
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetBtmText(const Value : string);
begin
  if (FBtmText <> Value) then
    begin
      FBtmText := Value;
      Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Set Text and Repaint
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetImageId(const Value : integer);
begin
  if (FImageId <> Value) then
    begin
      FImageId := Value;
      Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Set Number of Bars
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetBars(const Value : integer);
var
  Ind : integer;
begin
  FBars := Value;
  SetLength(FBarArr,    FBars);
  SetLength(FBarArrows, FBars);

  if (FBars > 0) then
    For Ind := 0 to FBars - 1 do
      begin
        FBarArr[Ind]    := 0;
        FBarArrows[Ind] := ArrowNone;
      end;
end;
//------------------------------------------------------------------------------
// Set Number of Bars
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetBarPos(const Ind, Value : integer);
begin
  if (Ind >= 0) and (Ind < FBars) and (FBarArr[Ind] <> Value) then
    begin
      FBarArr[Ind] := Value;
      Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Set Number of Bars
//------------------------------------------------------------------------------
procedure TMonBaseWin.SetBarArrow(const Ind, Value : integer);
begin
  if (Ind >= 0) and (Ind < FBars) and (FBarArrows[Ind] <> Value) then
    begin
      if Assigned(FTimer) then
        FTimer.Enabled := true;
        
      FBarArrows[Ind] := Value;
      Invalidate;
    end;
end;
//------------------------------------------------------------------------------
// Get Hint
//------------------------------------------------------------------------------
function TMonBaseWin.GetHint: string;
begin
  result := 'Window Id: ' + IntToStr(FId);
end;
//------------------------------------------------------------------------------
// Calculate the Size of the Window based on Font
//------------------------------------------------------------------------------
class function TMonBaseWin.CalcSize(
        const Size   : integer;  // Relative Size
        const Canvas : TCanvas)  // Canvas to Draw on
                     : TSize;    // Resulting Size
var
  rSize : TSize;
begin
  result.cx := 80;
  result.cy := 120;

  if Assigned(Canvas) then
    begin
      // Get Size of a Nominal Text

      case Size of
        SIZE_SMALL  : rSize := Canvas.TextExtent('XXXXXXX');
        SIZE_BIG    : rSize := Canvas.TextExtent('XXXXXXXXXXX');
      else
        rSize := Canvas.TextExtent('XXXXXXXXX');
      end;

      result.cx := rSize.cx;
      result.cy := round(1.3 * result.cx);
    end;
end;
//------------------------------------------------------------------------------
// Paint Window
//------------------------------------------------------------------------------
procedure TMonBaseWin.Paint;
const
  BRD = 2;
var
  R : TRect;
  H,W,D : integer;
begin
  if Assigned(FCanvas) then
    begin
      // Draw Background Rectangle

      FCanvas.Pen.Style := psClear;

      FCanvas.Brush.Style := bsSolid;
      FCanvas.Brush.Color := FBackColor;

      FCanvas.RoundRect(FRect.Left, FRect.Top, FRect.Right, FRect.Bottom,5,5);

      //------------------------------------------------------------------------
      // Draw Bars
      //------------------------------------------------------------------------

      if (FBars > 0) then
        begin
          FCanvas.Brush.Color := FForeColor;

          W := (FRect.Right - FRect.Left) div FBars;
          for H := 0 to FBars - 1 do
            begin
              FCanvas.RoundRect(
                FRect.Left + H * W,
                FRect.Top +
                ((100 - FBarArr[H]) * (FRect.Bottom - FRect.Top)) div 100,
                FRect.Left + (H + 1) * W,
                FRect.Bottom,5,5);
            end;
        end;

      //------------------------------------------------------------------------
      // Draw Image
      //------------------------------------------------------------------------

      if (FImageId >= 0) then
        begin
          FImages.Draw(FCanvas,
            FRect.Left + (FRect.Right  - FRect.Left - FImages.Width)  div 2,
            FRect.Top  + 4*(FRect.Bottom - FRect.Top -  FImages.Height) div 5,
            FImageId);
        end;

      //------------------------------------------------------------------------
      // Draw Arrows
      //------------------------------------------------------------------------

      if (FBars > 0) then
        begin
          FCanvas.Pen.Style   := psSolid;
          FCanvas.Pen.Color   := FForeColor;
          FCanvas.Pen.Width   := 1;
          FCanvas.Brush.Style := bsSolid;
          FCanvas.Brush.Color := FHighColor;

          W := (FRect.Right - FRect.Left) div 2;
          for H := 0 to FBars - 1 do
            begin
              case FBarArrows[H] of
                ArrowUp    : D := cArrowDirUp;
                ArrowDown  : D := cArrowDirDown;
                ArrowLeft  : D := cArrowDirLeft;
                ArrowRight : D := cArrowDirRight;
              else
                D := -1;
              end;

              if (D <> -1) then
                DrawArrow(
                  FCanvas,
                  Point(FRect.Left + H * W + W div 2,
                        FRect.Top + (FRect.Bottom - FRect.Top) div 3),
                  round(0.25 * W), round(0.25 * W),
                  D);
            end;
        end;

      //------------------------------------------------------------------------
      // Draw all Texts
      //------------------------------------------------------------------------

      Windows.SetBkMode(FCanvas.Handle, Windows.TRANSPARENT);

      H := FCanvas.TextHeight('Qg');

      // Draw Top Text

      if (length(FTopText) > 0) then
        begin
          R := Rect(
            FRect.Left  + BRD,
            FRect.Top   + BRD,
            FRect.Right - BRD,
            FRect.Top + H + BRD);

          DrawTextEx(FCanvas.Handle, PAnsiChar(FTopText), -1, R,
            DT_CENTER or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS, nil);
        end;

      // Draw Middle Text

      if (length(FMdlText) > 0) then
        begin
          R := Rect(
            FRect.Left + BRD,
            FRect.Top + (FRect.Bottom - FRect.Top - H) div 2,
            FRect.Right - BRD,
            FRect.Top + H + (FRect.Bottom - FRect.Top - H) div 2);

          DrawTextEx(FCanvas.Handle, PAnsiChar(FMdlText), -1, R,
            DT_CENTER or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS, nil);
        end;

      // Draw Bottom Text

      if (length(FBtmText) > 0) then
        begin
          R := Rect(
            FRect.Left + BRD,
            FRect.Bottom - H - BRD,
            FRect.Right - BRD,
            FRect.Bottom - BRD);

          DrawTextEx(FCanvas.Handle, PAnsiChar(fBtmText), -1, R,
            DT_CENTER or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS, nil);
        end;
    end;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
function TMonBaseWin.GetOrder:integer;
begin
  result := FId;
end;
//------------------------------------------------------------------------------
// Add Menu
//------------------------------------------------------------------------------
procedure TMonBaseWin.AddMenu(const PopupMenu : TPopupMenu);
begin
  // Must be overridden
end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonBaseWin);
end.
