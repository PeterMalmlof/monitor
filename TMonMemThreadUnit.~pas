unit TMonMemThreadUnit;

interface

uses
  Windows,          // Lots...
  SysUtils,         // Lots...
  Classes,          // Threads
  Contnrs,          // TObjectList
  Forms,            // Post Message

  TPmaMemUnit,          // Memory Manager
  TBkgThreadQueueUnit;  // Base Thread


//------------------------------------------------------------------------------
// Test Media Thread
//------------------------------------------------------------------------------
type TMonMemThread = class(TBkgThread)
  private
    objMemManager : TPmaMemObj;
  protected

  public
    constructor Create(const Sub : integer); override;
    destructor  Destroy; override;

    procedure Execute; override;
end;

implementation

uses
  TPmaClassesUnit;

//------------------------------------------------------------------------------
//
//                              CREATE THREAD
//
//------------------------------------------------------------------------------
// Create Thread: This will be run in its own thread
//------------------------------------------------------------------------------
constructor TMonMemThread.Create(const Sub : integer);
begin
  inherited Create(Sub);

  // The Thread is write locked by the inherited constructor

  // We dont need a real reply

  objResult  := 0;
  objReply   := false;

  objMemManager := TPmaMemObj.Create;

  // It will be write locked until AfterConstruction is called
end;
//------------------------------------------------------------------------------
// Destroy Thread:
//------------------------------------------------------------------------------
destructor TMonMemThread.Destroy;
begin
  objMemManager.free;

  inherited;
end;
//------------------------------------------------------------------------------
//
//                                 GET & SET
//
//------------------------------------------------------------------------------
// Execute Object
//------------------------------------------------------------------------------
procedure TMonMemThread.Execute;
var
  NextTick   : cardinal;
begin
  Log(self.ClassName + ' Started');

  // Get Next Interval

  objCS.BeginRead;
  NextTick := objMemManager.pNextTick;
  objCS.EndRead;

  //----------------------------------------------------------------------------
  // Measure System Memory in a Loop

  while (not self.Terminated) do
    begin
      // Refresh Load if its Time

      if (Windows.GetTickCount > NextTick) then
        begin
          objCS.BeginWrite;
          objMemManager.Refresh;
          NextTick := objMemManager.pNextTick;
          objCS.EndWrite;
        end;

      // Sleep in little steps to make Terminate faster

      Sleep(100);
    end;

  //----------------------------------------------------------------------------
  // Now Write Lock it until its Destroyed

  objCS.BeginWrite;

  objFinished := true;

  Log(self.ClassName + ' Terminated');

  // When finnished it will be Destroyed

  objCS.EndWrite;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonMemThread);
end.
