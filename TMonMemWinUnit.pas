unit TMonMemWinUnit;

interface

uses
  Windows, SysUtils, StdCtrls, StrUtils, Graphics, Controls,
  Classes, Forms, Menus,

  TMonMemThreadUnit,
  TMonBaseWinUnit; // Base Class
type
  TMonMemWin = class(TMonBaseWin)
  private
    FPhysSize : int64;
    FPhysFree : int64;
    FPhysLoad : int64;

    FPageSize : int64;
    FPageFree : int64;
    FPageLoad : int64;

  protected

    procedure MsgProcess(var Msg : TMsg); override;

    function GetHint:string; override;

    procedure OnInformation (Sender : TObject);
  public
    constructor Create(
      const Canvas : TCanvas;
      const Images : TImageList);
                     override;

    destructor  Destroy; override;

    procedure StartUp;  override;

    procedure AddMenu(const PopupMenu : TPopupMenu); override;

end;

implementation

uses
  TGenStrUnit,
  TPmaMemUnit,
  TGenPopupMenuUnit,
  TWmMsgFactoryUnit,
  TPmaProcessUtils,
  TInfoFormUnit,
  TPmaClassesUnit;

const
  ONE_KB = 1020;

//------------------------------------------------------------------------------
// Create Window
//------------------------------------------------------------------------------
constructor TMonMemWin.Create(
      const Canvas : TCanvas;
      const Images : TImageList);
begin
  inherited Create(Canvas, Images);

  pTopText := 'Memory';
  pMdlText := 'Load (%)';
  pBtmText := 'Size (GB)';

  // We use two Bars: Physical and Page Memory

  pBars := 2;
  
  // Just Create the Thread and leave it (it will be taken care of)

  TMonMemThread.Create(self.FId);

  FPhysSize := 0;
  FPhysFree := 0;
  FPhysLoad := 0;

  FPageSize := 0;
  FPageFree := 0;
  FPageLoad := 0;
end;
//------------------------------------------------------------------------------
// StartUp
//------------------------------------------------------------------------------
procedure TMonMemWin.StartUp;
begin
  inherited;

  if Assigned(FSubscriber) then
    begin
      // Add My Messages

      FSubscriber.AddMessage(MSG_MEM_PHYS_SIZE);
      FSubscriber.AddMessage(MSG_MEM_PHYS_FREE);
      FSubscriber.AddMessage(MSG_MEM_PHYS_LOAD);
      FSubscriber.AddMessage(MSG_MEM_PHYS_TRIG);

      FSubscriber.AddMessage(MSG_MEM_PAGE_SIZE);
      FSubscriber.AddMessage(MSG_MEM_PAGE_FREE);
      FSubscriber.AddMessage(MSG_MEM_PAGE_LOAD);
      FSubscriber.AddMessage(MSG_MEM_PAGE_TRIG);
    end;
end;
//------------------------------------------------------------------------------
// Default Message Process
//------------------------------------------------------------------------------
procedure TMonMemWin.MsgProcess(var Msg : TMsg);
begin
  inherited;

  // Handle Default Messages

  case Msg.message of

    //--------------------------------------------------------------------------
    // Physical Memory
    //--------------------------------------------------------------------------

    MSG_MEM_PHYS_SIZE : 
      begin
        FPhysSize := INT64(ONE_KB) * INT64(Msg.lParam);
        self.pBtmText := SizeToStr(FPhysSize);
      end;

    MSG_MEM_PHYS_FREE :
      begin
        FPhysFree := INT64(ONE_KB) * INT64(Msg.lParam);
      end;

    MSG_MEM_PHYS_LOAD :
      begin
        FPhysLoad     := Msg.lParam;
        self.pMdlText := IntToStr(FPhysLoad) + '%';

        self.SetBarPos(0, FPhysLoad);
      end;

    MSG_MEM_PHYS_TRIG :
      begin
        self.SetBarArrow(0, Msg.lParam);
      end;

    //--------------------------------------------------------------------------
    // Page Memory
    //--------------------------------------------------------------------------

    MSG_MEM_Page_SIZE :
      begin
        FPageSize := INT64(ONE_KB) * INT64(Msg.lParam);
      end;

    MSG_MEM_Page_FREE :
      begin
        FPageFree := INT64(ONE_KB) * INT64(Msg.lParam);
      end;

    MSG_MEM_PAGE_LOAD :
      begin
        FPageLoad := Msg.lParam;
        self.SetBarPos(1, FPageLoad);
      end;

    MSG_MEM_PAGE_TRIG :
      begin
        if Msg.lParam > 0 then
          self.SetBarArrow(1,ArrowUp)
        else if Msg.lParam < 0 then
          self.SetBarArrow(1,ArrowDown);
      end;
  end;
end;
//------------------------------------------------------------------------------
// Destroy Window
//------------------------------------------------------------------------------
destructor TMonMemWin.Destroy;
begin
  TPmaMemObj.ShutDown;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
function TMonMemWin.GetHint:string;
begin
  result := 'Memory' + CRLF +
            'Physical Size: ' + SizeToStr(FPhysSize) + CRLF +
            'Physical Free: ' + SizeToStr(FPhysFRee) + CRLF +
            'Physical Load: ' + IntToStr (FPhysLoad) + '%' + CRLF +
            'Pagefile Size: ' + SizeToStr(FPageSize) + CRLF +
            'Pagefile Free: ' + SizeToStr(FPageFRee) + CRLF +
            'Pagefile Load: ' + IntToStr (FPageLoad) + '%';
end;
//------------------------------------------------------------------------------
// Add Menu
//------------------------------------------------------------------------------
procedure TMonMemWin.AddMenu(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenuItem;
begin
  pMenu := TGenMenuItem.Create(PopupMenu);
  pMenu.Caption := 'Information ';
  pMenu.OnClick := OnInformation;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonMemWin.OnInformation (Sender : TObject);
var
  dlg : TInfoForm;
begin
  dlg := self.GetInfoForm;
  dlg.Caption := 'Memory Information ';

  dlg.AddWmiClass('Win32_PhysicalMemory', '', 'Physical Memory', 'BankLabel');
  dlg.AddWmiClass('Win32_CacheMemory',    '', 'Cash Memory',     'Purpose');
  dlg.AddWmiClass('Win32_PageFile',       '', 'Page File',       'Caption');
  dlg.AddWmiClass('Win32_PageFileUsage',  '', 'Page File Usage', 'Caption');
  dlg.AddWmiClass('Win32_SMBIOSMemory',   '', 'SMBIOS Memory',   'DeviceId');

end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonMemWin);
end.
