unit TMonDriveThreadUnit;

interface

uses
  Windows,  // Lots...
  SysUtils, // Lots...
  Classes,  // Threads

  TGenFileSystemUnit,   // File System
  TBkgThreadQueueUnit;  // Base Thread

//------------------------------------------------------------------------------
// Test Media Thread
//------------------------------------------------------------------------------
type TMonDriveThread = class(TBkgThread)
  private
    FDrive : TGenDrive;
  public
    constructor Create(
      const Sub   : integer;
      const Drive : TGenDrive); reintroduce;

    procedure   Execute; override;
end;

implementation

uses
  TGenStrUnit,      // String Handling
  TPmaWmiUnit,      // WMI Query
  TPmaClassesUnit;  // Class Management

const
  Interval          =  1000; // Monitor Interval (1 Second)
  IntervalTempStart = 10000; // 10 Seconds at Start
  IntervalTempNext  = 10000; // 10 Seconds at Start

//------------------------------------------------------------------------------
//
//                              CREATE THREAD
//
//------------------------------------------------------------------------------
// Create Thread: This will be run in its own thread
//------------------------------------------------------------------------------
constructor TMonDriveThread.Create(
      const Sub   : integer;
      const Drive : TGenDrive);
begin
  inherited Create(Sub);

  // The Thread is write locked by the inherited constructor

  // We dont need a real reply

  objResult  := 0;
  objReply   := false;

  FDrive := Drive;

  // It will be write locked until AfterConstruction is called
end;
//------------------------------------------------------------------------------
//
//                                 GET & SET
//
//------------------------------------------------------------------------------
// Execute Object
//------------------------------------------------------------------------------
procedure TMonDriveThread.Execute;
var
  NextLoadTick : cardinal;
  NextTempTick : cardinal;
begin
  Log(self.ClassName + ' Started');

  objCS.BeginWrite;

  //----------------------------------------------------------------------------
  // NOTE: NEED THIS IF NOT MAIN THREAD
  //----------------------------------------------------------------------------

  //TWmiObject.OleInitialize;  // Need to Initialize OLE in this Thread

  //----------------------------------------------------------------------------
  // Initate NetWork Manager

  Log('Drive Path: ' + FDrive.pPathName);
  Log('Drive Type: ' + FDrive.pTypeStr);
  Log('Drive Size: ' + SizeToStr(FDrive.pSize));
  Log('Drive Free: ' + SizeToStr(FDrive.pFree));

  // Get Next Interval for Load and Temp

  objCS.BeginRead;
  NextLoadTick := FDrive.pLoadNextTick;
  NextTempTick := FDrive.pTempNextTick;
  objCS.EndRead;

  //----------------------------------------------------------------------------
  // Refresh Network Manager in a Loop

  while (not self.Terminated) do
    begin
      // Refresh Load if its Time

      if (Windows.GetTickCount > NextLoadTick) then
        begin
          objCS.BeginWrite;
          FDrive.Refresh;
          NextLoadTick := FDrive.pLoadNextTick;
          objCS.EndWrite;
        end;

      // Refresh Temp if its Time

      if (Windows.GetTickCount > NextTempTick) then
        begin
          objCS.BeginWrite;
          FDrive.RefreshTemp;
          NextTempTick := FDrive.pTempNextTick;
          objCS.EndWrite;
        end;

      // Sleep in little steps to make Terminate faster

      Sleep(100);
    end;

  //----------------------------------------------------------------------------
  // Now Write Lock it until its Destroyed

  objCS.BeginWrite;

  objFinished := true;

  Log(self.ClassName + ' Terminated');

  // When finnished it will be Destroyed

  objCS.EndWrite;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonDriveThread);
end.
