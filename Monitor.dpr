program Monitor;

uses
  Windows,
  Forms,
  SysUtils,
  TMainMonitorUnit in 'TMainMonitorUnit.pas' {MainMonitor},
  TMonBaseWinUnit in 'TMonBaseWinUnit.pas',
  TMonCpuWinUnit in 'TMonCpuWinUnit.pas',
  TMonMemWinUnit in 'TMonMemWinUnit.pas',
  TMonNetWinUnit in 'TMonNetWinUnit.pas',
  TMonMemThreadUnit in 'TMonMemThreadUnit.pas',
  TPmaMemUnit in '..\PmaComp\TPmaMemUnit.pas',
  TPmaNetworkUnit in '..\PmaComp\TPmaNetworkUnit.pas',
  TPmaWmiUnit in '..\PmaComp\TPmaWmiUnit.pas',
  TMonNetThreadUnit in 'TMonNetThreadUnit.pas',
  TMonDriveWinUnit in 'TMonDriveWinUnit.pas',
  TMonDriveThreadUnit in 'TMonDriveThreadUnit.pas',
  TGenDriveUtilsUnit in '..\PmaComp\TGenDriveUtilsUnit.pas',
  TGenDriveTempUnit in '..\PmaComp\TGenDriveTempUnit.pas',
  TGenDriveRecycleBinUnit in '..\PmaComp\TGenDriveRecycleBinUnit.pas',
  TPmaCpuUtilsUnit in '..\PmaComp\TPmaCpuUtilsUnit.pas',
  TPmaCpuUnit in '..\PmaComp\TPmaCpuUnit.pas',
  TMonCpuThreadUnit in 'TMonCpuThreadUnit.pas',
  TPmaCpuTempUnit in '..\PmaComp\TPmaCpuTempUnit.pas',
  TPmaProcessUtils in '..\PmaComp\TPmaProcessUtils.pas',
  TMonFileUnit in 'TMonFileUnit.pas',
  TValueFormUnit in 'TValueFormUnit.pas' {ValueForm},
  TPmaDateTimeUnit in '..\PmaComp\TPmaDateTimeUnit.pas',
  TPmaProcessUnit in '..\PmaComp\TPmaProcessUnit.pas',
  TPmaProcessListUnit in '..\PmaComp\TPmaProcessListUnit.pas',
  TPmaTimerUnit in '..\PmaComp\TPmaTimerUnit.pas',
  TPmaProcessListDescUnit in '..\PmaComp\TPmaProcessListDescUnit.pas',
  TPmaTextFileUnit in '..\PmaComp\TPmaTextFileUnit.pas',
  TProcFormUnit in 'TProcFormUnit.pas' {ProcForm},
  TInfoFormUnit in 'TInfoFormUnit.pas' {InfoForm},
  TMonProcessListThreadUnit in 'TMonProcessListThreadUnit.pas',
  TPmaFormUtils in '..\PmaComp\TPmaFormUtils.pas';

{$R *.res}
var
  mHandle: THandle;
begin
  mHandle := CreateMutex(nil, True, 'MONITOR_MUTEX');
  if (GetLastError = ERROR_ALREADY_EXISTS) then
  begin
    BEEP;
    EXIT;
  end;

  Application.Initialize;
  Application.Title := 'Monitor';
  Application.CreateForm(TMainMonitor, MainMonitor);
  Application.Run;

  if (mHandle <> 0) then CloseHandle(mHandle);
end.
