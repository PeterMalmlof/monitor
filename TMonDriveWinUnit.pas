unit TMonDriveWinUnit;

interface

uses
  Windows,    // Stuff
  SysUtils,   // Stuff
  StdCtrls,   // TMemo
  StrUtils,
  Graphics,
  Controls,
  Classes,    // TComponent
  Forms,      // TApplication
  Menus,
  ComCtrls, ExtCtrls,

  TGenFileSystemUnit,   // Drives
  TMonDriveThreadUnit,  // Thread
  TBkgThreadQueueUnit,  // Base Thread
  TMonBaseWinUnit;      // Base Class
type
  TMonDriveWin = class(TMonBaseWin)
  private
    FDrive  : TGenDrive;        // Reference to Drive Object
    FThread : TMonDriveThread;  // Reference to Thread

    FHint  : string;

    FTimer : TTimer;
  protected
    function  IsDriveObject: boolean;

    procedure MsgProcess(var Msg : TMsg); override;

    procedure GetHintInt;

    function  GetHint:string; override;

    function  GetOrder: integer; override;

    // Menu Commands

    procedure OnTrayOpen        (Sender : TObject);
    procedure OnTrayClose       (Sender : TObject);
    procedure OnEjectUsb        (Sender : TObject);
    procedure OnEmptyRecycleBin (Sender : TObject);
    procedure OnInformation     (Sender : TObject);
    procedure OnPerf     (Sender : TObject);

  public
    constructor Create(
      const Canvas : TCanvas;
      const Images : TImageList;
      const pDrive : TGenDrive); reintroduce;

    destructor  Destroy; override;

    procedure   StartUp; override;

    procedure AddMenu(const PopupMenu : TPopupMenu); override;

    property pDrive  : TGenDrive read FDrive;
end;

implementation

uses
  TGenPopupMenuUnit,
  TGenDriveUtilsUnit,
  TGenStrUnit,
  TPmaLogUnit,
  TWmMsgFactoryUnit,
  TMonNetThreadUnit,
  TMonFileUnit,
  TInfoFormUnit,
  TPmaWmiUnit,
  TPmaProcessUtils,
  TPmaClassesUnit;

resourcestring
  resDrive        = 'Drive';
  resDriveLetter  = 'Driveletter';
  resLabel        = 'Label';
  resSize         = 'Size';
  resFree         = 'Free';
  resDriveType    = 'Drive Type';
  resDriveId      = 'Drive Id';
  resNotAvailable = 'Not Available';

//------------------------------------------------------------------------------
// Is Drive Available anymore
//------------------------------------------------------------------------------
function TMonDriveWin.IsDriveObject:boolean;
begin
  result := Assigned(FileSystem) and
            Assigned(FileSystem.Computer) and
            FileSystem.Computer.IsDrive(FDrive);
end;
//------------------------------------------------------------------------------
// Create Window
//------------------------------------------------------------------------------
constructor TMonDriveWin.Create(
      const Canvas : TCanvas;
      const Images : TImageList;
      const pDrive : TGenDrive);
begin
  inherited Create(Canvas, Images);

  FDrive := pDrive;

  if FDrive.pType = dtUsb then
    pTopText := FDrive.pFileName + ' (Usb)'
  else  if FDrive.pType = dtCD then
    pTopText := FDrive.pFileName + ' (Dvd)'
  else
    pTopText := FDrive.pFileName;

  pMdlText := SizeToStr(FDrive.pFree);
  pBtmText := SizeToStr(FDrive.pSize);

  // We use one Bar: Free

  pBars := 1;

  if (FDrive.pSize > 0) then
    SetBarPos(0, round(100 * (FDrive.pSize-FDrive.pFree)/FDrive.pSize))
  else
    SetBarPos(0,0);

  self.GetHintInt;

  // Create the Thread and remember it

  FThread := TMonDriveThread.Create(self.FId, FDrive);

  FTimer := TTimer.Create(nil);
  FTimer.Interval := 2000;
  FTimer.OnTimer  := OnPerf;
  FTimer.Enabled  := false;
end;
//------------------------------------------------------------------------------
// StartUp
//------------------------------------------------------------------------------
procedure TMonDriveWin.StartUp;
begin
  inherited;

  if Assigned(FSubscriber) then
    begin
      // Add My Messages

      FSubscriber.AddMessage(MSG_FS_DRIVE_TRIG);
      FSubscriber.AddMessage(MSG_FS_DRIVE_FREE_UP);
      FSubscriber.AddMessage(MSG_FS_DRIVE_FREE_DOWN);

      FSubscriber.AddMessage(MSG_FS_DRIVE_TEMP);
    end;
end;
//------------------------------------------------------------------------------
// Default Message Process
//------------------------------------------------------------------------------
procedure TMonDriveWin.MsgProcess(var Msg : TMsg);
begin
  inherited;

  // Handle Default Messages

  if IsDriveObject then
  case Msg.message of

    //--------------------------------------------------------------------------
    // Drive Data Changed
    //--------------------------------------------------------------------------

    MSG_FS_DRIVE_TRIG :
      if (TObject(Msg.lParam) = FDrive) then
      begin
        if (FDrive.pTempCelsius > 0) then
          pTopText := FDrive.pFileName +
            '(' + IntToStr(FDrive.pTempCelsius) + Chr(176) + ')';

        if (FDrive.pFree > 0) then
          pMdlText := SizeToStr(FDrive.pFree)
        else
          pMdlText := '';

        pBtmText := SizeToStr(FDrive.pSize);

        if (FDrive.pSize > 0) then
          SetBarPos(0, round(100 * (FDrive.pSize-FDrive.pFree)/FDrive.pSize))
        else
          SetBarPos(0,0);

        if (FDrive.pType = dtCD) then
          begin
            If (FDrive.pSize > 0) then
              pImageId := ImageCdFull
            else
              pImageId := ImageCdEmpty;
              
          end
        else if (FDrive.pRecycleCount > 0) then
          pImageId := ImageRecycle
        else
          pImageId := ImageNone;

        self.GetHintInt;
      end;

    MSG_FS_DRIVE_FREE_UP :
      if (TObject(Msg.lParam) = FDrive) then
      begin
        pMdlText := SizeToStr(FDrive.pFree);

        if (FDrive.pSize > 0) then
          SetBarPos(0, round(100 * (FDrive.pSize-FDrive.pFree)/FDrive.pSize))
        else
          SetBarPos(0,0);

        SetBarArrow(0, ArrowDown);

        self.GetHintInt;
      end;

    MSG_FS_DRIVE_FREE_DOWN :
      if (TObject(Msg.lParam) = FDrive) then
      begin
        pMdlText := SizeToStr(FDrive.pFree);

        if (FDrive.pSize > 0) then
          SetBarPos(0, round(100 * (FDrive.pSize-FDrive.pFree)/FDrive.pSize))
        else
          SetBarPos(0,0);

        SetBarArrow(0, ArrowUp);

        self.GetHintInt;
      end;

    MSG_FS_DRIVE_TEMP :
      begin
        if (TMonValueType.GetQualifier(Msg.wParam) = FDrive.pDriveId) then
          begin
            if (Msg.lParam > 0) then
              self.pTopText := FDrive.pFileName +
                '(' + IntToStr(FDrive.pTempCelsius) + Chr(176) + ')';
          end;
      end;
  end;
end;
//------------------------------------------------------------------------------
// Destroy Window
//------------------------------------------------------------------------------
destructor TMonDriveWin.Destroy;
begin
  // Terminate the Thread if it still there

  if Assigned(BkgQueue) then
    begin
    if (BkgQueue.IsThread(FThread)) then
      FThread.Terminate;
    end;

  FThread := nil;
  FTimer.Free;
  inherited;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
procedure TMonDriveWin.OnPerf(Sender : TObject);
begin
  if Assigned(FDrive) then
    FDrive.RefreshPerf;


end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
procedure TMonDriveWin.GetHintInt;
begin
  FHint := 'Unknown Drive';
  if IsDriveObject then
    begin
      FHint :=
      'Drive Name   ' + FDrive.pFileName + CRLF +
      'Drive Path   ' + FDrive.pPathName + CRLF +
      'Drive Type   ' + FDrive.pTypeStr  + CRLF +
      'MediaType    ' + FDrive.pMediaTypeStr;

      if (FDrive.pDriveId >= 0) then
        FHint := FHint + CRLF +
      'Drive Id     ' + IntToStr(FDrive.pDriveId);

      if (FDrive.pSize > 0) then
        begin
          FHint := FHint + CRLF +
          'Label        ' + FDrive.pLabel           + CRLF +
          'Total Size   ' + SizeToStr(FDrive.pSize) + CRLF +
          'Free Space   ' + SizeToStr(FDrive.pFree) + CRLF +
          'FileSystem   ' + FDrive.pFileSystem;
        end;

      if (FDrive.pTempCelsius > 0) then
        FHint := FHint + CRLF +
      'Temperature  ' + IntToStr(FDrive.pTempCelsius) + Chr(176);

      if (FDrive.pRecycleCount > 0) then
        begin
          FHint := FHint + CRLF +
          'Recycle Bin  ' + IntToStr(FDrive.pRecycleCount) + ' (#)' + CRLF +
          'Recycle Size ' + SizeToStr(FDrive.pRecycleSize);

        end;
    end;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
function TMonDriveWin.GetHint:string;
begin
  result := FHint;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
function TMonDriveWin.GetOrder:integer;
var
  Letter : string;
begin
  result := 10;
  if IsDriveObject then
    begin
      Letter := FDrive.pFileName;
      if (length(Letter) > 0) then
        result := 10 + ord(Letter[1]);
    end;
end;
//------------------------------------------------------------------------------
// Add Menu
//------------------------------------------------------------------------------
procedure TMonDriveWin.AddMenu(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenuItem;
  bSep : boolean;
begin
  bSep := false;

  if IsDriveObject then
    begin
      if (FDrive.pType = dtCD) then
        begin
          pMenu := TGenMenuItem.Create(PopupMenu);
          pMenu.Caption := '-';
          PopupMenu.Items.Add(pMenu);
          bSep := true;

          pMenu := TGenMenuItem.Create(PopupMenu);
          pMenu.Caption := 'Open Tray ';
          pMenu.OnClick := OnTrayOpen;
          PopupMenu.Items.Add(pMenu);

          pMenu := TGenMenuItem.Create(PopupMenu);
          pMenu.Caption := 'Close Tray ';
          pMenu.OnClick := OnTrayClose;
          PopupMenu.Items.Add(pMenu);
        end
      else if FDrive.IsEjectable then
        begin
          pMenu := TGenMenuItem.Create(PopupMenu);
          pMenu.Caption := '-';
          PopupMenu.Items.Add(pMenu);
          bSep := true;

          pMenu := TGenMenuItem.Create(PopupMenu);
          pMenu.Caption := 'Eject Usb Drive ';
          pMenu.OnClick := OnEjectUsb;
          PopupMenu.Items.Add(pMenu);
        end;

      if (FDrive.pRecycleCount > 0) then
        begin
          if (not bSep) then
            begin
              pMenu := TGenMenuItem.Create(PopupMenu);
              pMenu.Caption := '-';
              PopupMenu.Items.Add(pMenu);
            end;

          pMenu := TGenMenuItem.Create(PopupMenu);
          pMenu.Caption := 'Empty Recycle Bin ';
          pMenu.OnClick := OnEmptyRecycleBin;
          PopupMenu.Items.Add(pMenu);
        end;

      pMenu := TGenMenuItem.Create(PopupMenu);
      pMenu.Caption := 'Information ';
      pMenu.OnClick := OnInformation;
      PopupMenu.Items.Add(pMenu);

    end;
end;
//------------------------------------------------------------------------------
// Open DVD Tray
//------------------------------------------------------------------------------
procedure TMonDriveWin.OnTrayOpen  (Sender : TObject);
begin
  if IsDriveObject then
    FDrive.OpenTray;
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonDriveWin.OnTrayClose (Sender : TObject);
begin
  if IsDriveObject then
    FDrive.CloseTray;
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonDriveWin.OnEjectUsb (Sender : TObject);
begin
  if IsDriveObject then
    FDrive.Eject;
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonDriveWin.OnEmptyRecycleBin (Sender : TObject);
begin
  if IsDriveObject then
    FDrive.EmptyRecycleBin;
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonDriveWin.OnInformation (Sender : TObject);
const
  sGen   = 'Generic';
  sClass = 'Win32_LogicalDisk';
  sIdB   = '(DeviceID=''';
  sIdE   = ':'')';
  sLd    = 'Logical Disk';
  sDev   = 'DeviceId';
  sBin = 'Recycle Bin';
  sNA  = 'Not Avalable';
  sDd  = 'Disk Data';
  sPd  = 'Physical Disk';
  sDp  = 'Disk Partition';
var
  dlg     : TInfoForm;
  pNode   : TTreeNode;
  pBin    : TTreeNode;
  Letter  : string;
begin
  dlg := self.GetInfoForm;
  dlg.Caption := 'Drive Information ' + FDrive.pFileName;

  Letter := FDrive.pFileName[1];

  //----------------------------------------------------------------------------
  // Generic Information

  pNode := dlg.AddInfo(nil, sGen, resDriveLetter,  FDrive.pFileName);

  dlg.AddInfo(pNode, sGen, resLabel, FDrive.pLabel);
  dlg.AddInfo(pNode, sGen, resSize,  SizeToStr(FDrive.pSize,'b',false) +
                           ' (' + ToStr(FDrive.pSize) + ')');
  dlg.AddInfo(pNode, sGen, resFree,  SizeToStr(FDrive.pFree,'b',false) +
                           ' (' + ToStr(FDrive.pFree) + ')');

  if (FDrive.pTempCelsius > 0) then
    dlg.AddInfo(nil, 'Temperature',
      'Temp', IntToStr(FDrive.pTempCelsius) + chr(176));

  // Drive Type

  dlg.AddInfo(pNode, sGen, resDriveType, FDrive.pTypeStr);

  // Drive Identity

  if (FDrive.pDriveId >=0) then
    dlg.AddInfo(pNode, sGen, resDriveId, IntToStr(FDrive.pDriveId))
  else
    dlg.AddInfo(pNode, sGen, resDriveId, resNotAvailable);

  if (FDrive.pRecycleCount > 0) then
    begin
      pBin := dlg.AddInfo(nil, sBin,
          'Size', SizeToStr(FDrive.pRecycleSize,'b',false) +
                ' (' + ToStr(FDrive.pRecycleSize) + ')');
      dlg.AddInfo(pBin, sBin, 'Count', IntToStr(FDrive.pRecycleCount));
    end;

  //----------------------------------------------------------------------------
  // Logical Disk

  dlg.AddWmiClass(sClass, sIdB + Letter + sIdE, sLd, sDev);

  //----------------------------------------------------------------------------
  // Physical Disk

  if (FDrive.pDriveId >= 0) then
    dlg.AddWmiClass('Win32_DiskDrive',
        '(Index=' + IntToStr(FDrive.pDriveId) + ')',
        'Win32_DiskDrive','DeviceId');

  //----------------------------------------------------------------------------
  // Disk Partition

  if (FDrive.pDriveId >= 0) then
    dlg.AddWmiClass('Win32_DiskPartition',
        '(DiskIndex=' + IntToStr(FDrive.pDriveId) + ')',
        'Win32_DiskPartition','Name');

  //----------------------------------------------------------------------------
  // Performance Monitor

  if (FDrive.pDriveId >= 0) then
    dlg.AddWmiClass('Win32_PerfRawData_PerfDisk_LogicalDisk',
        '(Name="' + Letter + ':")',
        'Win32_PerfRawData_PerfDisk_LogicalDisk','Name');
end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonDriveWin);
end.
