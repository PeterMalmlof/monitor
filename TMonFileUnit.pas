unit TMonFileUnit;

interface

uses
  Windows, Classes, SysUtils, ExtCtrls, Messages, Forms, Contnrs, Graphics,

  TPmaTimerUnit,      // Timer
  TWmMsgFactoryUnit;  // Message Factory

//------------------------------------------------------------------------------
//  Value Record
//------------------------------------------------------------------------------
type TMonRec = record
  Tick  : cardinal; // Value Time Tick
  Id    : integer;  // Value Identifier
  Qual  : integer;  // Value Qualifier
  Value : integer;  // Value
end;
type TMonDynArr = array of TMonRec;

type TMonValueRec = record
  ValueTime : integer;  // Value Time
  Value     : integer;  // Value
end;
type TMonValueRecArr = array of TMonValueRec;

//------------------------------------------------------------------------------
//  Value Type Object
//------------------------------------------------------------------------------
type TMonValueType = class (TObject)
  private
    FValueName  : string;          // Name derived from Type
    FValueType  : integer;         // Type of Value  (MsgId
    FValueQual  : integer;         // Type Qualifier (Core index etc.)
    FVisible    : boolean;         // True if Visible
    FColor      : TColor;          // Color of Value
    FValueArr   : TMonValueRecArr; // Values
    FValueCount : integer;         // Number of Values in Array
  protected

    function GetCount : integer;
    function GetLabel : string;
  public
    constructor Create(const ValueType, Qual : integer);
    destructor  Destroy; override;

    procedure AddValue(const vTime, Value : integer);
    procedure Clear;

    function IterValues(
      var Iter      : integer;    // Iterator
      out ValueTime : integer;    // Value Time
      out Value     : integer)    // Value
                    : boolean;    // True of not EOV

    function IterTimeValues(
      var   Iter      : integer;  // Iterator
      const StartTime : integer;  // Start Time
      const EndTime   : integer;  // End Time
      out   ValueTime : integer;  // Value Time
      out   Value     : integer;  // Value
      out   LastTime  : integer;  // Last Time
      out   LastValue : integer)  // Last Value
                      : boolean;  // True of not EOV or Time >= End

    // Translate wParam to Qualifier and Time

    class function  GetQualifier(const Value : integer): integer;
    class function  GetTime     (const Value : integer): integer;

    property pName    : string  read FValueName;
    property pLabel   : string  read GetLabel;
    property pType    : integer read FValueType;
    property pQual    : integer read FValueQual;
    property pCount   : integer read GetCount;
    property pVisible : boolean read FVisible     write FVisible;
    property pColor   : TColor  read FColor       write FColor;
end;

//------------------------------------------------------------------------------
//  Value Type List Object
//------------------------------------------------------------------------------
type TMonValueTypeList = class (TObject)
  private
    FList : TObjectList;

  protected

    function GetCount : integer;
    
  public
    constructor Create;
    destructor  Destroy; override;

    procedure AddValueType(const ValueType : TMonValueType);

    function FindValueType(const Id, Qual : integer): TMonValueType;

    function IterValueTypes(
      var Iter      : integer;
      out ValueType : TMonValueType)
                    : boolean;

    procedure LogIt;

    property pCount : integer read GetCount;
end;
//------------------------------------------------------------------------------
//  Monitor File Object
//------------------------------------------------------------------------------
type TMonFile = class (TObject)
  private
    FCS : TMultiReadExclusiveWriteSynchronizer;

    FSubscriber : TWmMsgSubscriber;

    FValues : TStringList;
    FFilePath : string;

    FCount : int64;

  protected

    procedure MsgProcess(var Msg : TMsg);

    procedure AddValue(const ValueType, Qual, ValueTime, Value : integer);

    class function TransLine(const Line : string): TMonRec;
  public
    constructor Create;
    constructor CreateForRead;

    destructor  Destroy ; override;

    procedure StartUp;  // Start subscribing on Log Values
    procedure ShutDown; // ShutDown
    procedure Flush;    // Flush Values to Disk

    // Get all Values and Add them to Value Types in List

    procedure GetValues(const TypeList : TMonValueTypeList);
end;
implementation

uses
  TGenStrUnit,        // String Functions
  TGenIniFileUnit,    // IniFile Functions
  TPmaLogUnit,

  TPmaCpuUnit,          // Cpu Monitoring
  TPmaCpuTempUnit,      // Cpu Temp Monitoring
  TPmaMemUnit,          // Memory Monitoring
  TPmaNetworkUnit,      // Network Monitring
  TGenFileSystemUnit,   // Drive Monitoring
  TPmaProcessUnit,      // Processes
  TPmaProcessListUnit,  // Process List
  TPmaClassesUnit;      // Classes

const
  LogFile    = 'Values.txt';

  prfPref    = 'ValueTypes';
  prfColor   = ' Color';
  prfVisible = ' Visible';

  EOV = ';';

//------------------------------------------------------------------------------
// Log
//------------------------------------------------------------------------------
procedure Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//
//                                   VALUE TYPE
//
//------------------------------------------------------------------------------
//  Create Value Type Object
//------------------------------------------------------------------------------
constructor TMonValueType.Create(const ValueType, Qual : integer);
var
  Ini : TGenIniFile;
  sTmp : string;
  pProc : TPmaProcess;
begin
  inherited Create;

  FValueType := ValueType;
  FValueQual := Qual;
  FVisible   := true;

  // Set Name of Value Type derived from Type and Qualifier

  case ValueType of
    MSG_CPU_LOAD      : FValueName := 'Cpu Load';
    MSG_CPU_CORE_LOAD : FValueName := 'Core ' + IntToStr(Qual) + ' Load';
    MSG_CPU_TEMP      : FValueName := 'Cpu Temp';
    MSG_CPU_CORE_TEMP : FValueName := 'Core ' + IntToStr(Qual) + ' Temp';
    MSG_CPU_FREQ_LOAD : FValueName := 'Cpu Freq Load';

    MSG_MEM_PHYS_LOAD : FValueName := 'Phys Mem';
    MSG_MEM_PAGE_LOAD : FValueName := 'Page Mem';

    MSG_NET_UPLOAD    : FValueName := 'UpLoad';
    MSG_NET_DOWNLOAD  : FValueName := 'DownLoad';

    MSG_FS_DRIVE_LOAD :
      begin
        sTmp := FileSystem.DriveIdToName(Qual);
        FValueName := 'Drive ' + sTmp;
      end;

    MSG_FS_DRIVE_TEMP :
      begin
        sTmp := FileSystem.DriveIdToName(Qual);
        FValueName := 'Drive Temp ' + sTmp;
      end;

    MSG_PROC_LOAD :
      begin
        // Get Process (Alive) from its PID in Qual

        pProc := Processes.FindProcAliveByPID(Qual);
        if Assigned(pProc) then
          FValueName := pProc.pName
        else
          FValueName := 'Unknown';
      end;

  else
    FValueName := 'Unknown';
  end;

  // Set Color derived from Type and Qualifier

  case ValueType of
    MSG_CPU_LOAD      : FColor := RGB(255,0,0);
    MSG_CPU_CORE_LOAD : FColor := RGB(255,0,0);
    MSG_CPU_TEMP      : FColor := RGB(255,0,255);
    MSG_CPU_CORE_TEMP : FColor := RGB(255,0,255);
    MSG_CPU_FREQ_LOAD : FColor := RGB(0,255,255);

    MSG_MEM_PHYS_LOAD : FColor := RGB(0,0,255);
    MSG_MEM_PAGE_LOAD : FColor := RGB(0,255,0);

    MSG_NET_UPLOAD    : FColor := RGB(0,0,255);
    MSG_NET_DOWNLOAD  : FColor := RGB(0,255,0);

    MSG_FS_DRIVE_LOAD : FColor := RGB(0,255,0);
    MSG_FS_DRIVE_TEMP : FColor := RGB(0,255,0);

    MSG_PROC_LOAD : FColor := RGB(0,255,0);
  else
    FColor := clWhite;
  end;

  // Read IniFile Settings

  Ini := TGenIniFile.Create;
  FColor   := Ini.ReadColor(prfPref, FValueName + prfColor,   FColor);
  FVisible := Ini.ReadBool (prfPref, FValueName + prfVisible, FVisible);
  Ini.Free;

  // Log('Create ValueType ' + FValueName + ' Color ' + ToStr(FColor));

  FValueCount := 0;
  SetLength(FValueArr, FValueCount);
end;
//------------------------------------------------------------------------------
//  Destroy Value Type Object
//------------------------------------------------------------------------------
destructor TMonValueType.Destroy;
var
  Ini : TGenIniFile;
begin

  // Log('Destroy ValueType ' + FValueName + ' Color ' + ToStr(FColor));

  // Write IniFile Settings

  Ini := TGenIniFile.Create;
  Ini.WriteColor (prfPref, FValueName + prfColor,   FColor);
  Ini.WriteBool  (prfPref, FValueName + prfVisible, FVisible);
  Ini.Free;

  SetLength(FValueArr, 0);

  inherited;
end;
//------------------------------------------------------------------------------
// Translate wParam to Qualifier
//------------------------------------------------------------------------------
class function TMonValueType.GetQualifier(const Value : integer): integer;
begin
  // The Qualifier in bits 0-7 (8 bits = 0..31)

  result := (Value and $0000001F);
end;
//------------------------------------------------------------------------------
// Translate wParam to Time (seconds)
//------------------------------------------------------------------------------
class function TMonValueType.GetTime (const Value : integer): integer;
begin
  // The Time is in the bites 8-31 (24 bits = 0 to 134 217 727) in seconds
  // this mean 1 136 962 minutes, 37 282 hours, 1553 days (more than 47 days)

  result := (Value and $FFFFFFE1) div 32;
end;
//------------------------------------------------------------------------------
//  Add a New Value to Value Type
//------------------------------------------------------------------------------
function TMonValueType.GetCount : integer;
begin
  result := FValueCount;
end;
//------------------------------------------------------------------------------
//  Add a New Value to Value Type
//------------------------------------------------------------------------------
function TMonValueType.GetLabel : string;
begin
  result := FValueName;
end;
//------------------------------------------------------------------------------
//  Add a New Value to Value Type
//------------------------------------------------------------------------------
procedure TMonValueType.AddValue(const vTime, Value : integer);
begin
  // Make sure there is room

  if (FValueCount >= length(FValueArr)) then
    SetLength(FValueArr, FValueCount + 20);

  FValueArr[FValueCount].ValueTime := vTime;
  FValueArr[FValueCount].Value     := Value;
  Inc(FValueCount);
end;
//------------------------------------------------------------------------------
//  Add a New Value to Value Type
//------------------------------------------------------------------------------
procedure TMonValueType.Clear;
begin
  FValueCount := 0;
  SetLength(FValueArr, FValueCount);
end;
//------------------------------------------------------------------------------
//  Iterate all Values
//------------------------------------------------------------------------------
function TMonValueType.IterValues(
      var Iter      : integer;    // Iterator
      out ValueTime : integer;    // Value Time
      out Value     : integer)    // Value
                    : boolean;    // True of not EOV
begin
  if (Iter >= 0) and (Iter < FValueCount) then
    begin
      result    := true;
      ValueTime := FValueArr[Iter].ValueTime;
      Value     := FValueArr[Iter].Value;
      Inc(Iter);
    end
  else
    begin
      result    := false;
      ValueTime := 0;
      Value     := 0;
    end;
end;
//------------------------------------------------------------------------------
//  Iterate Values between Start and End Time
//------------------------------------------------------------------------------
function TMonValueType.IterTimeValues(
      var   Iter      : integer;  // Iterator
      const StartTime : integer;  // Start Time
      const EndTime   : integer;  // End Time
      out   ValueTime : integer;  // Value Time
      out   Value     : integer;  // Value
      out   LastTime  : integer;  // Last Time
      out   LastValue : integer)  // Last Value
                      : boolean;  // True of not EOV or Time >= End
var
  Ind : integer;
begin
  result    := false;
  ValueTime := 0;
  Value     := 0;

  // Always at least return the First Time as the Last

  if (FValueCount > 0) then
    begin
      LastTime  := FValueArr[0].ValueTime;
      LastValue := FValueArr[0].Value;
    end;

  // Now walk all Records

  if (Iter >= 0) and (Iter < FValueCount) then
    begin
      // 1) Walk from Iter to the first time >= Start Time
      // 2) Remember the Last Value just before StartTime
      // 3) Return if Next Value is >= Start Time
      // 3) Stop if Time >= End Time

      // Walk from Iter until End of Values (EOV)

      for Ind := Iter to FValueCount - 1 do

        // Is this Value between (or at) Start and End

        if (FValueArr[Ind].ValueTime >= StartTime) and
           (FValueArr[Ind].ValueTime <= EndTime) then
          begin
            // Take this Value and

            result    := true;
            ValueTime := FValueArr[Ind].ValueTime;
            Value     := FValueArr[Ind].Value;

            // Set Iter to next Value

            Iter      := Ind + 1;
            BREAK;
          end

        // Have we Passed End of Time

        else if (FValueArr[Ind].ValueTime > EndTime) then
          begin
            // Return with false

            BREAK;
          end
        else
          begin
            // Remember this as the Value just before Start

            LastTime  := FValueArr[Ind].ValueTime;
            LastValue := FValueArr[Ind].Value;
          end;
    end;
end;
//------------------------------------------------------------------------------
//
//                                VALUE TYPE LIST
//
//------------------------------------------------------------------------------
//  Create TMonValueTypeList
//------------------------------------------------------------------------------
constructor TMonValueTypeList.Create;
begin
  inherited;

  FList := TObjectList.Create(true);
end;
//------------------------------------------------------------------------------
//  Create TMonValueTypeList
//------------------------------------------------------------------------------
destructor TMonValueTypeList.Destroy;
begin
  FList.Free;

  inherited;
end;
//------------------------------------------------------------------------------
//  Add a New Value to Value Type
//------------------------------------------------------------------------------
function TMonValueTypeList.GetCount : integer;
begin
  result := FList.Count;
end;
//------------------------------------------------------------------------------
//  Add a New Value Type Object
//------------------------------------------------------------------------------
procedure TMonValueTypeList.AddValueType(const ValueType : TMonValueType);
var
  Ind  : integer;
begin
  if Assigned(ValueType) and (ValueType is TMonValueType) then
    begin
      if (FList.IndexOf(ValueType) < 0) then
        begin
          // Walk all current Types and Insert it Sorted

          if (FList.Count > 0) then
            for Ind := 0 to FList.Count - 1 do
              if (AnsiCompareText(
                    ValueType.pName,
                    TMonValueType(FList[Ind]).pName) < 0) then
                begin
                  FList.Insert(Ind, ValueType);
                  EXIT;
                end;

          // Still Here, Add it Last
          
          FList.Add(ValueType);
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Find an Existing Value Type By Id and Qualifier
//------------------------------------------------------------------------------
function TMonValueTypeList.FindValueType
            (const Id, Qual : integer): TMonValueType;
var
  Ind : integer;
begin
  result := nil;

  if (FList.Count > 0) then
    for Ind := 0 to FList.Count - 1 do
      if (TMonValueType(FList[Ind]).FValueType = Id) and
         (TMonValueType(FList[Ind]).FValueQual = Qual) then
        begin
          result := FList[Ind] as TMonValueType;
          BREAK;
        end;
end;
//------------------------------------------------------------------------------
//  Iterate all Value Types
//------------------------------------------------------------------------------
function TMonValueTypeList.IterValueTypes(
      var Iter      : integer;
      out ValueType : TMonValueType)
                    : boolean;
begin
  result := false;
  if (Iter >= 0) and (Iter < FList.Count) then
    begin
      result := true;
      ValueType := FList[Iter] as TMonValueType;
      Inc(Iter);
    end;
end;
//------------------------------------------------------------------------------
//  Find an Existing Value Type By Id and Qualifier
//------------------------------------------------------------------------------
procedure TMonValueTypeList.LogIt;
var
  Ind : integer;
begin
  Log('');
  Log('Value Types:');

  if (FList.Count > 0) then
    for Ind := 0 to FList.Count - 1 do
      Log('Name '    + TMonValueType(FList[Ind]).pLabel +
          ' Type '   + IntToStr(TMonValueType(FList[Ind]).pType) +
          ' Qual '   + IntToStr(TMonValueType(FList[Ind]).pQual) +
          ' Values ' + IntToStr(TMonValueType(FList[Ind]).pCount));
end;
//------------------------------------------------------------------------------
//
//                                   VALUE FILE
//
//------------------------------------------------------------------------------
//  Create TGenMemObj
//------------------------------------------------------------------------------
constructor TMonFile.Create;
begin
  inherited;

  FCS := TMultiReadExclusiveWriteSynchronizer.Create;
  FCS.BeginWrite;

  // Create the Stringlist that hlds all Values received until Flushed to Disk

  FValues := TStringList.Create;
  FCount  := 0;

  // Create the File

  FFilePath :=
    SysUtils.IncludeTrailingPathDelimiter(
      SysUtils.ExtractFileDir(Application.ExeName)) + LogFile;

  TGenFileSystem.CreateTextFile(FFilePath);

  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
//  Create TGenMemObj
//------------------------------------------------------------------------------
constructor TMonFile.CreateForRead;
begin
  inherited;

  FCS := TMultiReadExclusiveWriteSynchronizer.Create;
  FCS.BeginWrite;

  // Create the Stringlist that hlds all Values received until Flushed to Disk

  FValues := TStringList.Create;
  FCount  := 0;

  // Create the File

  FFilePath :=
    SysUtils.IncludeTrailingPathDelimiter(
      SysUtils.ExtractFileDir(Application.ExeName)) + LogFile;

  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
//  Destroy TGenMemObj
//------------------------------------------------------------------------------
destructor TMonFile.Destroy;
begin
  FCS.BeginWrite;

  FValues.Free;

  FCS.Free;

  inherited;
end;
//------------------------------------------------------------------------------
// StartUp
//------------------------------------------------------------------------------
procedure TMonFile.StartUp;
begin
  FCS.BeginWrite;

  Log('  ' + self.ClassName + '.Startup');

  if Assigned(MsgFactory) then
    begin
      FSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProcess);

      // Process Monitoring

      FSubscriber.AddMessage(MSG_CPU_LOAD);
      FSubscriber.AddMessage(MSG_CPU_CORE_LOAD);
      FSubscriber.AddMessage(MSG_CPU_TEMP);
      FSubscriber.AddMessage(MSG_CPU_CORE_TEMP);
      FSubscriber.AddMessage(MSG_CPU_FREQ_LOAD);

      // Memory Monitoring

      FSubscriber.AddMessage(MSG_MEM_PHYS_LOAD);
      FSubscriber.AddMessage(MSG_MEM_PAGE_LOAD);

      // Network Monitoring

      FSubscriber.AddMessage(MSG_NET_UPLOAD);
      FSubscriber.AddMessage(MSG_NET_DOWNLOAD);

      // Drive Monitoring

      FSubscriber.AddMessage(MSG_FS_DRIVE_TEMP);

      // Process Monitoring

      FSubscriber.AddMessage(MSG_PROC_LOAD);
    end;

  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
// Default Message Process
//------------------------------------------------------------------------------
procedure TMonFile.MsgProcess(var Msg : TMsg);
begin
  // Handle Default Messages

  case Msg.message of

    MSG_CPU_LOAD,
    MSG_CPU_CORE_LOAD,
    MSG_CPU_TEMP,
    MSG_CPU_CORE_TEMP,
    MSG_CPU_FREQ,

    MSG_MEM_PHYS_LOAD,
    MSG_MEM_PAGE_LOAD,

    MSG_NET_UPLOAD,
    MSG_NET_DOWNLOAD,

    MSG_FS_DRIVE_TEMP :
      AddValue(Msg.message,
        TMonValueType.GetQualifier(Msg.wParam),
        TMonValueType.GetTime(Msg.wParam), Msg.lParam);

    MSG_PROC_LOAD :
      AddValue(Msg.message, Msg.wParam,
        Windows.GetTickCount div 1000, Msg.lParam);
  end;
end;
//------------------------------------------------------------------------------
// ShutDown all Things
//------------------------------------------------------------------------------
procedure TMonFile.ShutDown;
begin
  Flush;

  FCS.BeginWrite;

  Log('');
  Log(self.ClassName + '.ShutDown');
  Log('  Values ' + IntToStr(FCount));

  if Assigned(FSubscriber) then
    MsgFactory.DeSubscribe(FSubscriber);

  FSubscriber := nil;

  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
// Flush all Values to Disk
//------------------------------------------------------------------------------
procedure TMonFile.Flush;
begin
  FCS.BeginWrite;

  Log(self.ClassName + '.Flush');

  // Append all Lines in FValues to LogFile

  TGenFileSystem.AppendTextFile(FFilePath, FValues);

  // Then Clear the Value List

  FValues.Clear;

  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
// Add a New Value
//------------------------------------------------------------------------------
procedure TMonFile.AddValue(const ValueType, Qual, ValueTime, Value : integer);
begin
  FCS.BeginWrite;

  FValues.Add(
    IntToStr(ValueTime) + EOV +
    IntToStr(ValueType) + EOV +
    IntToStr(Qual)      + EOV +
    IntToStr(Value)     + EOV);

  if (FValues.Count > 500) then
    Flush;

  Inc(FCount);

  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
// Translate a Text to a Monitor Record
//------------------------------------------------------------------------------
class function TMonFile.TransLine(const Line : string): TMonRec;
var
  Ind : integer;
  Str : string;
  Lev : integer;
begin
  result.Tick  := 0;
  result.Id    := 0;
  result.Qual  := 0;
  result.Value := 0;

  Str := '';
  Lev := 0;
  For Ind := 1 to length(Line) do
    begin
      if (Line[Ind] = EOV) then
        begin
          if (length(Str) > 0) then
            case Lev of
              0 : result.Tick  := StrToInt(Str);
              1 : result.Id    := StrToInt(Str);
              2 : result.Qual  := StrToInt(Str);
              3 : result.Value := StrToInt(Str);
            end;
          Str := '';
          Inc(Lev);
        end
      else if (Line[Ind] >= '0') and (Line[Ind] <= '9') then
        Str := Str + Line[Ind];
    end;
end;
//------------------------------------------------------------------------------
// Get all Values between two Tick Times
//------------------------------------------------------------------------------
procedure TMonFile.GetValues(const TypeList : TMonValueTypeList);
var
  Ind     : integer;
  Strings : TStringList;
  pType   : TMonValueType;
  CurRec  : TMonRec;
begin
  FCS.BeginWrite;

  // Open the LogFile and read all Values to Memory

  Strings := TStringList.Create;

  TGenFileSystem.ReadTextFile(FFilePath, Strings);

  // Walk all Strings and Handle the each Line as a Value

  if (Strings.Count > 0) then
    begin
      for Ind := 0 to Strings.Count - 1 do
        begin
          CurRec := self.TransLine(Strings[Ind]);

          // Find the Value Type or Create it

          pType := TypeList.FindValueType(CurRec.Id, CurRec.Qual);
          if (not Assigned(pType)) then
            begin
              pType := TMonValueType.Create(CurRec.Id, CurRec.Qual);
              TypeList.AddValueType(pType);
            end;

          pType.AddValue(CurRec.Tick, CurRec.Value);
        end;
    end;

  // Fre Strings

  Strings.Free;
  FCS.EndWrite;
end;
//------------------------------------------------------------------------------
//                                    INITIALIZE
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonFile);
end.
 