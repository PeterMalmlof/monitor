unit TMonNetWinUnit;

interface

uses
  Windows, SysUtils, StdCtrls, StrUtils, Graphics, Controls, Classes, Forms,
  Menus,

  TPmaNetworkUnit,      // Network Object
  TBkgThreadQueueUnit,  // Background Queue
  TMonBaseWinUnit;      // Base Class
type
  TMonNetWin = class(TMonBaseWin)
  private
    FHint       : string;
    FThread     : TBkgThread;
  protected

    procedure MsgProcess(var Msg : TMsg); override;

    procedure GetHintInt;

    function GetHint:string; override;

    procedure OnInformation (Sender : TObject);

  public
    constructor Create(
      const Canvas : TCanvas;
      const Images : TImageList);
                     override;

    destructor  Destroy; override;

    procedure   StartUp; override;
    procedure   ShutDown; override;

    procedure AddMenu(const PopupMenu : TPopupMenu); override;
end;

implementation

uses
  TGenStrUnit,
  TGenPopupMenuUnit,
  TWmMsgFactoryUnit,
  TMonNetThreadUnit,
  TPmaProcessUtils,
  TInfoFormUnit,
  TPmaClassesUnit;

//------------------------------------------------------------------------------
// Create Window
//------------------------------------------------------------------------------
constructor TMonNetWin.Create(
      const Canvas : TCanvas;
      const Images : TImageList);
begin
  inherited Create(Canvas, IMages);

  pTopText := 'Network';
  pMdlText := 'Up (MBps)';
  pBtmText := 'Down (MBps)';
  FHint    := 'Network';

  // We use two Bars: DownLoad and UpLoad

  pBars := 2;

  // Just Create the Thread and leave it (it will be taken care of)

  FThread := TMonNetThread.Create(self.FId);

end;
//------------------------------------------------------------------------------
// StartUp
//------------------------------------------------------------------------------
procedure TMonNetWin.StartUp;
begin
  inherited;

  if Assigned(FSubscriber) then
    begin
      // Add My Messages

      FSubscriber.AddMessage(MSG_NET_INITATED);

      FSubscriber.AddMessage(MSG_NET_UPBPS);
      FSubscriber.AddMessage(MSG_NET_UPLOAD);
      FSubscriber.AddMessage(MSG_NET_UPTRIG);

      FSubscriber.AddMessage(MSG_NET_DOWNBPS);
      FSubscriber.AddMessage(MSG_NET_DOWNLOAD);
      FSubscriber.AddMessage(MSG_NET_DOWNTRIG);
    end;
end;
//------------------------------------------------------------------------------
// ShutDown
//------------------------------------------------------------------------------
procedure TMonNetWin.ShutDown;
begin
  inherited;
end;
//------------------------------------------------------------------------------
// Default Message Process
//------------------------------------------------------------------------------
procedure TMonNetWin.MsgProcess(var Msg : TMsg);
begin
  inherited;

  // Handle Default Messages

  case Msg.message of

    //--------------------------------------------------------------------------
    // Network Manager is Initated and Fixed Data is Available
    //--------------------------------------------------------------------------

    MSG_NET_INITATED : if Assigned(NetworkFactory) then
      begin
        self.SetBarPos(0, 0);
        self.SetBarPos(1, 0);
        self.pMdlText := '0%';
        self.pBtmText := '0%';

        self.GetHintInt;
      end;

    //--------------------------------------------------------------------------
    // DownLoad Data Refreshed
    //--------------------------------------------------------------------------

    MSG_NET_DOWNLOAD :
      begin
        self.SetBarPos(0, Msg.lParam);
      end;

    MSG_NET_DOWNBPS :
      begin
        self.pMdlText := SizeToStr(INT64(8000) * Msg.lParam,'Bps', false);
        self.GetHintInt;
      end;

    MSG_NET_DOWNTRIG :
      begin
        self.SetBarArrow(0, ArrowDown);
      end;

    //--------------------------------------------------------------------------
    // UpLoad Data Refreshed
    //--------------------------------------------------------------------------

    MSG_NET_UPLOAD :
      begin
        self.SetBarPos(1, Msg.lParam);
      end;

    MSG_NET_UPBPS :
      begin
        self.pBtmText := SizeToStr(INT64(8000) * Msg.lParam,'Bps', false);
        self.GetHintInt;
      end;

    MSG_NET_UPTRIG :
      begin
        self.SetBarArrow(1, ArrowUp);
      end;
  end;
end;
//------------------------------------------------------------------------------
// Destroy Window
//------------------------------------------------------------------------------
destructor TMonNetWin.Destroy;
begin
  TPmaNetwork.ShutDown;

  inherited;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
procedure TMonNetWin.GetHintInt;
begin
  // The NetManager reference must exist

  if Assigned(NetworkFactory) then
  begin
  FHint :=
  'Network' + CRLF +
  'Type        ' + NetworkFactory.pType + CRLF +
  'Description ' + NetworkFactory.pDesc + CRLF +
  'Bandwidth   ' + SizeToStr(NetworkFactory.pBandwidth,'Bps', false) + CRLF +
  'IP Address  ' + NetworkFactory.pIp   + CRLF +
  'Mac Address ' + NetworkFactory.pMac  + CRLF +
  'DownLoad    ' + IntToStr(NetworkFactory.pReceivedUsage) + '%' + CRLF +
  'DownLoad    ' + SizeToStr(1000 * NetworkFactory.pReceivedkBpsMax,'BpS Max') + CRLF +
  'DownLoaded  ' + SizeToStr(NetworkFactory.pReceivedBytes) + ' Since Boot' + CRLF +
  'UpLoad      ' + IntToStr(NetworkFactory.pSentUsage) + '%' + CRLF +
  'Upload      ' + SizeToStr(1000 * NetworkFactory.pSentkBpsMax,'BpS Max')+ CRLF +
  'UpLoaded    ' + SizeToStr(NetworkFactory.pSentBytes) + ' Since Boot';
  end;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
function TMonNetWin.GetHint:string;
begin
  result := FHint;
end;
//------------------------------------------------------------------------------
// Add Menu
//------------------------------------------------------------------------------
procedure TMonNetWin.AddMenu(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenuItem;
begin
  pMenu := TGenMenuItem.Create(PopupMenu);
  pMenu.Caption := 'Information ';
  pMenu.OnClick := OnInformation;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonNetWin.OnInformation (Sender : TObject);
var
  dlg : TInfoForm;
begin
  dlg := self.GetInfoForm;
  dlg.Caption := 'Network Information';

  dlg.AddWmiClass('Win32_PerfRawData_Tcpip_NetworkInterface',
                   '','Win32_PerfRawData_Tcpip_NetworkInterface', 'Name');
                   
  dlg.AddWmiClass('Win32_PerfRawData_Tcpip_UDP',
                   '','Network Interface Udp', 'Name');

  dlg.AddWmiClass('Win32_NetworkConnection','', 'Network Connection', 'Name');
  dlg.AddWmiClass('Win32_NetworkClient',    '', 'Network Client',     'Name');
  dlg.AddWmiClass('Win32_NetworkAdapter',   '', 'Network Adapter',    'Name');

  dlg.AddWmiClass('Win32_PerfRawData_PerfNet_Browser','',
                   'Network Browser Performance', 'Name');

end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonNetWin);
end.
