unit TMonCpuWinUnit;

interface

uses
  Windows, SysUtils, StdCtrls, StrUtils, Graphics, Controls, Classes,
  Forms, Menus,

  TPmaCpuUnit,        // Cpu Object
  TPmaCpuTempUnit,    // Cpu Temp Object
  TMonCpuThreadUnit,  // Cpu Thread Object
  TMonBaseWinUnit;    // Base Class
type
  TMonCpuWin = class(TMonBaseWin)
  private

    FHint      : string;
    FHintDirty : boolean;   // Hint Text need to be refreshed
    FStartTick : Cardinal;
    FMonStart  : cardinal;  // Monitor Start Tick
    FUserInit  : cardinal;  // User Init
  protected

    procedure GetHintInt;

    function  GetHint:string; override;

    procedure MsgProcess(var Msg : TMsg); override;

    procedure OnCpuInformation  (Sender : TObject);
    procedure OnProcInformation (Sender : TObject);
  public
    constructor Create(
      const Canvas   : TCanvas;
      const Images   : TImageList;
      const MonStart : cardinal;
      const UserInit : cardinal);
                       reintroduce;

    destructor Destroy; override;

    procedure  StartUp; override;

    procedure AddMenu(const PopupMenu : TPopupMenu); override;

end;

implementation

uses
  TGenStrUnit,       // String Functions
  TGenPopupMenuUnit, // Popup Menu
  TMonFileUnit,      // Monitor Functions
  TWmMsgFactoryUnit, // Message Factory
  TPmaProcessUtils,
  TPmaProcessListUnit,
  TInfoFormUnit,
  TPmaDateTimeUnit,
  TPmaClassesUnit;   // Classes

//------------------------------------------------------------------------------
// Create Window
//------------------------------------------------------------------------------
constructor TMonCpuWin.Create(
      const Canvas   : TCanvas;
      const Images   : TImageList;
      const MonStart : cardinal;
      const UserInit : cardinal);
begin
  inherited Create(Canvas, IMages);

  FMonStart  := MonStart;
  FUserInit  := UserInit;
  pTopText   := 'Cpu';
  pMdlText   := '0%';
  pBtmText   := '0 MHz';
  FHintDirty := false;
  FStartTick := Windows.GetTickCount;

  // Create the Cpu Object and Cpu Temp Obejct

  TPmaCpu.StartUp;

  // We use as Many Bars as Cores

  self.pBars := CpuFactory.pCores;

  // Just Create the Thread and leave it (it will be taken care of)

  TMonCpuThread.Create(self.FId);
end;
//------------------------------------------------------------------------------
// Destroy Window
//------------------------------------------------------------------------------
destructor TMonCpuWin.Destroy;
begin
  // Free the Cpu Object and Temp Object

  TPmaCpu.ShutDown;
  TPmaCpuTemp.ShutDown;

  inherited;
end;
//------------------------------------------------------------------------------
// StartUp
//------------------------------------------------------------------------------
procedure TMonCpuWin.StartUp;
begin
  inherited;

  if Assigned(FSubscriber) then
    begin
      // Add My Messages

      FSubscriber.AddMessage(MSG_CPU_MAX_FREQ);
      FSubscriber.AddMessage(MSG_CPU_CORE_LOAD);
      FSubscriber.AddMessage(MSG_CPU_LOAD);
      FSubscriber.AddMessage(MSG_CPU_TEMP);
      FSubscriber.AddMessage(MSG_CPU_FREQ);
      FSubscriber.AddMessage(MSG_CPU_TEMP_INIT);
    end;
end;
//------------------------------------------------------------------------------
// Default Message Process
//------------------------------------------------------------------------------
procedure TMonCpuWin.MsgProcess(var Msg : TMsg);
begin
  inherited;

  // Handle Default Messages

  case Msg.message of

    MSG_CPU_MAX_FREQ :
      begin
        // Max Cpu Frequency

        pBtmText   := SizeToStr(Msg.lParam);
        FHintDirty := true;
      end;

    MSG_CPU_CORE_LOAD :
      begin
        // One of the Cpu Cores has changed Load

        SetBarPos(TMonValueType.GetQualifier(Msg.wParam), Msg.lParam);
        FHintDirty := true;
      end;

    MSG_CPU_LOAD :
      begin
        // Overall Cpu Load has changed

        pMdlText   := IntToStr(Msg.lParam) + '%';
        FHintDirty := true;
      end;

    MSG_CPU_TEMP :
      begin
        // The Cpu Temperature has changed

        pTopText   := 'Cpu ' + IntToStr(Msg.lParam) + chr(176);
        FHintDirty := true;
      end;

    MSG_CPU_FREQ :
      begin
        // Cpu Frequence and Multiplier has changed Value

        pBtmText   := IntToStr(Msg.lParam);
        FHintDirty := true;
      end;

    MSG_CPU_TEMP_INIT :
      begin
        TPmaCpuTemp.Startup;
      end;

  end;
end; 
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
function TMonCpuWin.GetHint:string;
begin
  if FHintDirty then GetHintInt;
  FHintDirty := false;
  
  result := FHint;
end;
//------------------------------------------------------------------------------
// Get Hint Text
//------------------------------------------------------------------------------
procedure TMonCpuWin.GetHintInt;
var
  Ind : integer;
  T1,T2 : TPmaDateTime;
begin

  if Assigned(CpuFactory) then
  begin

  T1 := TPmaDateTime.Create;
  T2 := TPmaDateTime.Create;
  T1.WindowsBootTime;
  T2.Now;
  T2.Subtract(T1);
  FHint :=
  'Cpu:' + CRLF +

  'Booted      ' + T1.pDateTime + CRLF +
  'Time        ' + ToStr(FMonStart / 1000, 1) + ' (S from Boot)';

  if (FUserInit > 200) then
  FHint := FHint + CRLF +
  'User Init   ' + ToStr(FUserInit / 1000, 1) + ' (S from Boot)';

  if Assigned(Processes) and (Processes.pUserInit > 0) then
  FHint := FHint + CRLF +
  'Start Time  ' + ToStr(Processes.pUserInit / 1000,1) + ' S' + CRLF +
  'Cores       ' + IntToStr(CpuFactory.pCores);

  if Assigned(CpuTempFactory) then
  FHint := FHint + CRLF +
  'Name        ' + CpuTempFactory.pName;

  for Ind := 0 to CpuFactory.pCores - 1 do
    FHint := FHint + CRLF +
    'Core ' + IntToStr(Ind) + ' Load ' + IntToStr(CpuFactory.GetCoreLoad(Ind)) +
    ' Max: ' + IntToStr(CpuFactory.GetCoreLoadMax(Ind));

  if Assigned(CpuTempFactory) then
  begin
  for Ind := 0 to CpuFactory.pCores - 1 do
    FHint := FHint + CRLF +
    'Core ' + IntToStr(Ind) + ' Temp ' + IntToStr(CpuTempFactory.pTemp(Ind)) +
    ' Max: ' + IntToStr(CpuTempFactory.pTempMax(Ind));

  FHint := FHint + CRLF +
  'Frequency   ' + IntToStr(CpuTempFactory.pSpeed) +
  ' Max ' + IntToStr(CpuFactory.pMaxFreq) + ' (MHz)' + CRLF +
  'Multiplier  ' + IntToStr(round(round(CpuTempFactory.pMult/5)/2)) +
  ' Max ' + IntToStr(round(round(CpuTempFactory.pMultMax/5)/2)) + CRLF +
  'FSB         ' + IntToStr(CpuTempFactory.pFsb) + ' (MHz)';
  end;

  T1.Free;
  T2.Free;
  end;
end;
//------------------------------------------------------------------------------
// Add Menu
//------------------------------------------------------------------------------
procedure TMonCpuWin.AddMenu(const PopupMenu : TPopupMenu);
var
  pMenu : TGenMenuItem;
begin
  pMenu := TGenMenuItem.Create(PopupMenu);
  pMenu.Caption := 'Cpu Information ';
  pMenu.OnClick := OnCpuInformation;
  PopupMenu.Items.Add(pMenu);

  pMenu := TGenMenuItem.Create(PopupMenu);
  pMenu.Caption := 'Process Information ';
  pMenu.OnClick := OnProcInformation;
  PopupMenu.Items.Add(pMenu);
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonCpuWin.OnCpuInformation (Sender : TObject);
var
  dlg : TInfoForm;
begin
  dlg := self.GetInfoForm;
  dlg.Caption := 'Cpu and OS Information';

  dlg.AddWmiClass('Win32_Processor',       '', 'Processor', '');
  dlg.AddWmiClass('Win32_CacheMemory',    '', 'Cash Memory',     'Purpose');
  dlg.AddWmiClass('Win32_OperatingSystem', '', 'OS',        'Caption');
end;
//------------------------------------------------------------------------------
// Close Dvd Tray
//------------------------------------------------------------------------------
procedure TMonCpuWin.OnProcInformation (Sender : TObject);
var
  dlg : TInfoForm;
begin
  dlg := self.GetInfoForm;
  dlg.Caption := 'Process Information';

  dlg.AddWmiClass('Win32_Process',         '', 'Processes', 'Caption');
end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonCpuWin);
end.
