object ValueForm: TValueForm
  Left = 700
  Top = 888
  Width = 217
  Height = 122
  Hint = 'HInt Window'
  Caption = 'ValueForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = Menu
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  OnDestroy = FormDestroy
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnMouseWheel = FormMouseWheel
  OnPaint = FormPaint
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Menu: TGenPopupMenu
    AutoHotKeys = maManual
    OwnerDraw = True
    OnPopup = MenuPopup
    BackColor = clBtnFace
    ForeColor = clGray
    HighColor = clCream
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 12
    Font.Name = 'Ariel'
    Font.Style = []
    WindowHandle = 262414
    Left = 8
  end
end
