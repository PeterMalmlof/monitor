unit TValueFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus,    Math,     ExtCtrls,

  TMonFileUnit,        // Monitor Data Management
  TPmaDateTimeUnit,    // Date Time Object
  TGenAppPropUnit,     // Application Properties
  TGenPopupMenuUnit,   // Popup Menu
  TWmMsgFactoryUnit;   // Message Factory
type
  TValueForm = class(TForm)
    Menu: TGenPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MenuPopup(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDblClick(Sender: TObject);
  private
    // Form Properties

    FFormRect      : TGenAppPropRect;   // Form Position and Size
    FBackColor     : TGenAppPropColor;  // Background Color
    FLabelFont     : TGenAppPropFont;   // Label Font and Color
    FWinStartColor : TGenAppPropColor;  // Win Start Color
    FTimeLineColor : TGenAppPropColor;  // Time Line Color
    FMainFont      : TGenAppPropFont;   // Main Font
    FResolution    : TGenAppPropDouble; // Resolution

    // Monitor Start Time is set when creating and both FileTime and Ticks
    // are needed for references

    FMonStartTime : TPmaDateTime; // DateTime when Monitor Started
    FMonStartTick : cardinal;     // Tick when Monitor Started (in Seconds)

    // Window Start Time is the Time of the Left Side and will change
    // when scrolling the Window. Both Ticks and FileTime is needed
    // for reference:
    //  File Time is Needed for writing the Time Labels accuratly
    //  Tick Time is needed since the Log Records is in Ticks (seconds only)

    FWinStartTime  : TPmaDateTime; // DateTime of Window's Left Position
    FWinStartTick  : integer;      // Time Tick of Window's Left Position
    FWinEndTick    : integer;      // Time Tick of Window's Right Position
    FWinStartPixel : integer;      // Pixel of the Start Line
    TempTime       : TPmaDateTime; // Used for Calculations only

    FTimer     : TTimer; // Timer for moving Time Line

    FLabelSize : TSize;   // Size of the Label 'XX'
    FMonRect   : TRect;   // Size and Position of the Monitor Draw Window
    FStarted   : boolean; // Monitor Window has Started

    // Measurement for drawing Timing

    FTimeLineDistPixels  : integer; // Dist between Time Lines in Pixels
    FTimeLineDistSeconds : integer; // Dist between Time Lines in Seconds

    FTimeLIneFirstPixels  : integer;  // First TimeLIne in Pixels
    FTimeLineFirstSeconds : integer;  // First TimeLine in Seconds

    FTimeLineX      : integer; // Last Time Line X position

    FMouseDown      : boolean; // Mouse Left Button is Down
    FMouseLast      : integer; // Last X Position of Mouse
    FMouseDblClick  : boolean; // Mouse Double Clicked

    FSubscriber     : TWmMsgSubscriber; // Message Subscriber

    FValueTypeList  : TMonValueTypeList; // All Value Types
    FCurValueType   : TMonValueType;     // Selected ValueType
    FTimeLineSelect : boolean;           // Time Line is Selected
    FWinStartSelect : boolean;           // Win Start is Selected
    FLabelSelect    : boolean;

  protected
    procedure Log(const Line : string);

    procedure MsgProcess(var Msg : TMsg);

    // Return Resolution in Number of Seconds per Pixel

    function  SecondsPerPixel : double;
    function  MinutesPerPixel : double;

    // Translate Seconds into Pixels

    function SecondToPixel(const Second : integer): integer;
    function PixelToSecond(const Pixel  : integer): integer;

    // Translate Value (%) into Pixels

    function ValueToPixel(const Value : integer): integer;
    function PixelToValue(const Pixel : integer): integer;

    // Return a Nice Number of Minutes between Time Labels
    
    function  DefaultLabelMinutes : double;

    procedure PickOrPaint(const bPaint : boolean);

    procedure CalcLabelTiming;

    procedure OnMyTimer      (Sender : TObject);
    procedure OnMyClose      (Sender : TObject);
    procedure OnFont         (Sender : TObject);
    procedure OnBackColor    (Sender : TObject);
    procedure OnLabelColor   (Sender : TObject);
    procedure OnLabelFont    (Sender : TObject);
    procedure OnTimeColor    (Sender : TObject);
    procedure OnStartColor   (Sender : TObject);
    procedure OnResolution   (Sender : TObject);
    procedure OnValueType    (Sender : TObject);
    procedure OnValueColor   (Sender : TObject);
    procedure OnValueVisible (Sender : TObject);
    procedure OnProcess      (Sender : TObject);

    procedure OnColor    (Col : TColor);

    procedure OnFontChg(
      Name   : string;
      Height : integer;
      Bold   : boolean;
      Italic : boolean);

    procedure AddValue(const ValueType, Qual, ValueTime, Value : integer);
  public
    constructor Create(
        AOwner    : TComponent;   // Owner of Form (Main Monitor Window)
        StartTick : cardinal;     // Monitor Start Time in Ticks
        StartTime : TFileTime);   // Monitor Start File Time
                    reintroduce;
  end;

var
  ValueForm: TValueForm;

implementation

uses
  TPmaLogUnit,
  TGenStrUnit,
  TGenGraphicsUnit,
  TGenColorPickUnit,
  TGenHintManagerUnit,

  TPmaCpuUnit,
  TPmaCpuTempUnit,
  TPmaMemUnit,
  TPmaNetworkUnit,
  TGenFileSystemUnit,
  TPmaProcessUnit,
  TPmaProcessListUnit,

  TPmaClassesUnit;

{$R *.dfm}

const
  prfPref           = 'ValueForm';
  prfRect           = 'Rect';
  prfBackColor      = 'BackColor';
  prfForeColor      = 'ForeColor';
  prfLabelFont      = 'LabelFont';
  prfTimeLineColor  = 'TimeLineColor';
  prfWinStartColor  = 'WinStartColor';
  prfFont           = 'Font';
  prfResolution     = 'Resolution';

  TBRD = 2; // Borders around Label Text

//------------------------------------------------------------------------------
//  Create Monitor Windows
//------------------------------------------------------------------------------
constructor TValueForm.Create(
        AOwner    : TComponent;   // Owner of Form (Main Monitor Window)
        StartTick : cardinal;     // Monitor Start Time in Ticks
        StartTime : TFileTime);   // Monitor Start File Time
var
  S : int64;
begin
  inherited Create(AOwner);

  FStarted        := false;
  FMouseDblClick  := false;
  FCurValueType   := nil;
  FTimeLineSelect := false;
  FWinStartSelect := false;
  FLabelSelect    := false;

  //----------------------------------------------------------------------------
  // Calculate all Start Values
  //----------------------------------------------------------------------------

  // Create the Temporary Time object

  TempTime := TPmaDateTime.Create;

  // Log Ticks when the Monitor Started

  Log('');
  Log('Monitor Window Started:');
  if Assigned(PmaLog) then PmaLog.LogMem('Mem Init');

  // Translate Tick Time into real time

  FMonStartTime := TPmaDateTime.Create;
  FMonStartTime.pFileTime := StartTime;

  Log('Monitor StartTime : ' + FMonStartTime.pDateTime);

  // We need to get theWindows Boot time in real Ticks

  TempTime.WindowsBootTime;
  TempTime.Subtract(FMonStartTime);

  S := -TempTime.pSeconds;
  Log('Time from Boot    : ' + SecondsToStr(S));
  FMonStartTick := S;
  Log('Monitor StartTick : ' + IntToStr(FMonStartTick) + ' (Seconds)');
  Log('Time from Boot    : ' + SecondsToStr(FMonStartTick));

  // From start the Left position of windows is the Monitors Start Time
  // and its Time in Ticks is the Monitor's Start Tick

  FWinStartTime := TPmaDateTime.Create;
  FWinStartTime.pFileTime := FMonStartTime.pFileTime;
  FWinStartTick := FMonStartTick;

  Log('MonWin  StartTime : ' + FWinStartTime.pDateTime);
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TValueForm.FormCreate(Sender: TObject);
var
  MF  : TMonFile;
begin
  // Add FormRect to Application Properies

  FFormRect := App.CreatePropRect(prfPref, prfRect, Rect(100,100,400,300));

  FFormRect.SetBounds(self);

  FBackColor := App.CreatePropColor(prfPref, prfBackColor, clBtnFace);
  self.Color := FBackColor.pColor;

  FLabelFont := App.CreatePropFont(prfPref, prfLabelFont, self.Font);

  FTimeLineColor := App.CreatePropColor(prfPref, prfTimeLineCOlor, clYellow);

  FWinStartColor := App.CreatePropColor(
                    prfPref, prfWinStartColor, RGB(255,127,0));

  FMainFont := App.CreatePropFont(prfPref, prfFont, self.Font);

  FResolution := App.CreatePropDouble(prfPref, prfResolution, 20);

  Menu.StartUp;

  self.DoubleBuffered := true;

  Canvas.Font := FLabelFont.pFont;
  FLabelSize := Canvas.TextExtent('XX ');
  FMonRect   := Rect(FLabelSize.cx, 0,
                     self.ClientWidth,
                     self.ClientHeight - FLabelSize.cy - 2);

  // Calculate Current Label Timing

  CalcLabelTiming;

  Log('MonWin  StartTime : ' + FWinStartTime.pDateTime);
  Log('MonWin  StartTick : ' + IntToStr(FWinStartTick));

  Log('TimeLine Dist     : ' + IntToStr(FTimeLineDistSeconds) + ' (Seconds)');
  Log('TimeLine Dist     : ' + IntToStr(FTimeLineDistPixels)   + ' (Pixels)');

  Log('TimeLine First    : ' + IntToStr(FTimeLineFirstSeconds) + ' (Seconds)');
  Log('TimeLine First    : ' + IntToStr(FTimeLIneFirstPixels)  + ' (Pixels)');

  // Create Value Type List holding all Values

  FValueTypeList := TMonValueTypeList.Create;

  // Get All Old Values

  MF := TMonFile.CreateForRead;
  MF.GetValues(FValueTypeList);
  MF.Free;

  Log('Value Types Loaded: ' + IntToStr(FValueTypeList.pCount));

  // Subscribe on Messages

  if Assigned(MsgFactory) then
    begin
      FSubscriber := MsgFactory.Subscribe(self.ClassName, MsgProcess);

      // Process Monitoring

      FSubscriber.AddMessage(MSG_CPU_LOAD);
      FSubscriber.AddMessage(MSG_CPU_CORE_LOAD);
      FSubscriber.AddMessage(MSG_CPU_TEMP);
      FSubscriber.AddMessage(MSG_CPU_CORE_TEMP);
      FSubscriber.AddMessage(MSG_CPU_FREQ_LOAD);

      // Memory Monitoring

      FSubscriber.AddMessage(MSG_MEM_PHYS_LOAD);
      FSubscriber.AddMessage(MSG_MEM_PAGE_LOAD);

      // Network Monitoring

      FSubscriber.AddMessage(MSG_NET_UPLOAD);
      FSubscriber.AddMessage(MSG_NET_DOWNLOAD);

      // Drive Monitoring

      FSubscriber.AddMessage(MSG_FS_DRIVE_TEMP);

      // Process Monitoring

      FSubscriber.AddMessage(MSG_PROC_LOAD);
    end;

  FTimer := TTimer.Create(nil);
  FTimer.Interval := 1000;
  FTimer.Enabled  := true;
  FTimer.OnTimer  := OnMyTimer;

  FStarted := true;

  if Assigned(PmaLog) then PmaLog.LogMem('Mem Used');

end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TValueForm.MsgProcess(var Msg : TMsg);
begin
  case Msg.message of

    // Cpu Loggin
    
    MSG_CPU_LOAD,
    MSG_CPU_CORE_LOAD,
    MSG_CPU_TEMP,
    MSG_CPU_CORE_TEMP,
    MSG_CPU_FREQ_LOAD,
    MSG_MEM_PHYS_LOAD,
    MSG_MEM_PAGE_LOAD,
    MSG_NET_UPLOAD,
    MSG_NET_DOWNLOAD,
    MSG_FS_DRIVE_TEMP :
      AddValue(Msg.message,
        TMonValueType.GetQualifier(Msg.wParam),
        TMonValueType.GetTime(Msg.wParam), Msg.lParam);

    MSG_PROC_LOAD :
      AddValue(Msg.message, Msg.wParam,
        Windows.GetTickCount div 1000, Msg.lParam);

  end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TValueForm.FormDestroy(Sender: TObject);
begin
  Log('');
  Log(self.ClassName + '.FormDestroy');

  FTimer.Enabled := false;
  FTimer.Free;

  Menu.ShutDown;

  if Assigned(FSubscriber) then
    begin
      MsgFactory.DeSubscribe(FSubscriber);
    end;

  FValueTypeList.LogIt;
  FValueTypeList.Free;

  // Make Sure App Properties are written

  FFormRect.GetBounds(self);

  // Free Time objects

  FMonStartTime.Free;
  FWinStartTime.Free;
  TempTime.Free;

  // Release it from Parent Window

  if Assigned(self.Owner) then
    begin
      Log('Owner ' + Owner.ClassName);
      Owner.RemoveComponent(self);
    end;
end;
//------------------------------------------------------------------------------
//
//
//
//------------------------------------------------------------------------------
//  Add a New Value
//------------------------------------------------------------------------------
procedure TValueForm.AddValue(const ValueType, Qual, ValueTime, Value : integer);
var
  pValueType : TMonValueType;
begin
  // Log('Type '  + IntToStr(ValueType) +
  //     ' Qual ' + IntToStr(Qual) +
  //     ' Time ' + IntToStr(ValueTime) +
  //     ' Value ' + IntToStr(Value));

  // FInd the Value Type or Create it

  pValueType := FValueTypeList.FindValueType(ValueType, Qual);
  if (not Assigned(pValueType)) then
    begin
      pValueType := TMonValueType.Create(ValueType, Qual);
      FValueTypeList.AddValueType(pValueType);
    end;

  // Add Value to the Value Type

  pValueType.AddValue(ValueTime, Value);
end;
//------------------------------------------------------------------------------
//
//
//
//------------------------------------------------------------------------------
//  Log
//------------------------------------------------------------------------------
procedure TValueForm.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  Free the Form on Close
//------------------------------------------------------------------------------
procedure TValueForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//------------------------------------------------------------------------------
//  Pop Menu
//------------------------------------------------------------------------------
procedure TValueForm.MenuPopup(Sender: TObject);
var
  pPref, pMenu : TGenMenuItem;
  iType  : integer;
  pType  : TMonValueType;
  pProc : TPmaProcess;
begin
  Menu.Items.clear;

  // We need Current Position of Mouse if User changed Resolution

  FMouseLast := self.ScreenToClient(Mouse.CursorPos).X;

  if Assigned(FCurValueType) then
    begin
      pPref := TGenMenuItem.Create(Menu);
      pPref.Caption := 'Color ';
      pPref.OnClick := OnValueColor;
      Menu.Items.Add(pPref);

      pPref := TGenMenuItem.Create(Menu);
      pPref.Caption := 'Visible ';
      pPref.OnClick := OnValueVisible;
      pPref.Checked := FCurValueType.pVisible;
      Menu.Items.Add(pPref);


      pPref := TGenMenuItem.Create(Menu);
      pPref.Caption := '-';
      Menu.Items.Add(pPref);
    end;

  pPref := TGenMenuItem.Create(Menu);
  pPref.Caption := 'Values   ';
  Menu.Items.Add(pPref);

  iType := 0;
  while FValueTypeList.IterValueTypes(iType, pType) do
    begin
      pMenu := TGenMenuItem.Create(Menu);
      pMenu.Caption   := pType.pName;
      pMenu.OnClick   := OnValueType;
      pMenu.Tag       := iType - 1;
      pMenu.Checked   := pType.pVisible;
      pPref.Add(pMenu);
    end;

  if Assigned(Processes) then
    begin
      pPref := TGenMenuItem.Create(Menu);
      pPref.Caption := 'Processes   ';
      Menu.Items.Add(pPref);

      iType := 0;
      while Processes.GetNext(iType, pProc) do
        if pProc.pAlive then
          begin
            pMenu := TGenMenuItem.Create(Menu);
            pMenu.Caption   := pProc.pName;
            pMenu.OnClick   := OnProcess;
            pMenu.Checked   := pProc.pLogged;
            pMenu.Tag       := pProc.pPID;
            pPref.Add(pMenu);
          end;
    end;

  pPref := TGenMenuItem.Create(Menu);
  pPref.Caption := 'Resolution  ';
  Menu.Items.Add(pPref);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption   := '1 Minute/cm ';
    pMenu.OnClick   := onResolution;
    pMenu.Tag       := 1;
    pMenu.RadioItem := true;
    pMenu.Checked   := round(FResolution.pDouble) = pMenu.Tag;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption   := '5 Minute/cm ';
    pMenu.OnClick   := onResolution;
    pMenu.Tag       := 5;
    pMenu.RadioItem := true;
    pMenu.Checked   := round(FResolution.pDouble) = pMenu.Tag;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption   := '20 Minutes/cm ';
    pMenu.OnClick   := onResolution;
    pMenu.Tag       := 20;
    pMenu.RadioItem := true;
    pMenu.Checked   := round(FResolution.pDouble) = pMenu.Tag;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption   := '1 Hour/cm ';
    pMenu.OnClick   := onResolution;
    pMenu.Tag       := 60;
    pMenu.RadioItem := true;
    pMenu.Checked   := round(FResolution.pDouble) = pMenu.Tag;
    pPref.Add(pMenu);

  pPref := TGenMenuItem.Create(Menu);
  pPref.Caption := 'Preferences  ';
  Menu.Items.Add(pPref);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption := 'Main Font';
    pMenu.OnClick := OnFont;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption := 'Background Color';
    pMenu.OnClick := OnBackColor;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption := 'Label Color';
    pMenu.OnClick := OnLabelColor;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption := 'Label Font';
    pMenu.OnClick := OnLabelFont;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption := 'Time Line Color';
    pMenu.OnClick := OnTimeColor;
    pPref.Add(pMenu);

    pMenu := TGenMenuItem.Create(Menu);
    pMenu.Caption := 'Start Line Color';
    pMenu.OnClick := OnStartColor;
    pPref.Add(pMenu);

  pPref := TGenMenuItem.Create(Menu);
  pPref.Caption := '-';
  Menu.Items.Add(pPref);

  pPref := TGenMenuItem.Create(Menu);
  pPref.Caption := 'Close';
  pPref.OnClick := OnMyClose;
  Menu.Items.Add(pPref);

end;
//------------------------------------------------------------------------------
//  Toggle Process Logging
//------------------------------------------------------------------------------
procedure TValueForm.OnProcess (Sender : TObject);
var
  pProc : TPmaProcess;
begin
  // Find Process with right PID, then Toggle its Logging

  if Assigned(Processes) and Assigned(Sender) and (Sender is TMenuItem) then
    begin
      pProc := Processes.FindProcAliveByPID(TMenuItem(Sender).Tag);
      if Assigned(pProc) then
        begin
          pProc.pLogged := not pProc.pLogged;
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TValueForm.OnValueColor (Sender : TObject);
var
  nColor : TCOlor;
begin
  if Assigned(FCurValueType) then
    begin
      if TGenColorPick.PickColor(FCurValueType.pColor, nil, nColor) then
        begin
          FCurValueType.pColor := nColor;
          self.Invalidate;
        end;
    end;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TValueForm.OnValueVisible (Sender : TObject);
begin
  if Assigned(FCurValueType) then
    begin
      FCurValueType.pVisible := not FCurValueType.pVisible;
      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//  Adjust Font
//------------------------------------------------------------------------------
procedure TValueForm.OnValueType (Sender : TObject);
var
  iType  : integer;
  pType  : TMonValueType;
begin
  if Assigned(Sender) and (Sender is TMenuItem) then
    begin
      iType := TMenuItem(Sender).Tag;
      if FValueTypeList.IterValueTypes(iType, pType) then
        begin
          pType.pVisible := not pType.pVisible;
          self.Invalidate;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  Adjust Font
//------------------------------------------------------------------------------
procedure TValueForm.OnFont (Sender : TObject);
begin
  FMainFont.PickFont(OnFontChg);
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Label Font
//------------------------------------------------------------------------------
procedure TValueForm.OnLabelFont      (Sender : TObject);
begin
  FCurValueType   := nil;
  FTimeLineSelect := false;
  FWinStartSelect := false;
  FLabelSelect    := true;

  FLabelFont.PickFont(OnFontChg);
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Font
//------------------------------------------------------------------------------
procedure TValueForm.OnFontChg(
      Name   : string;
      Height : integer;
      Bold   : boolean;
      Italic : boolean);
begin
  if FLabelSelect then
    begin
      FLabelFont.pFontName   := Name;
      FLabelFont.pFontHeight := Height;
      FLabelFont.pFontBold   := Bold;
      FLabelFont.pFontItalic := Italic;
      // FLabelFont.pFont.Color := FLabelColor.pColor;

      Canvas.Font := FLabelFont.pFont;
      FLabelSize := Canvas.TextExtent('XX ');
      FMonRect   := Rect(FLabelSize.cx, 0,
                     self.ClientWidth,
                     self.ClientHeight - FLabelSize.cy - 2);

      CalcLabelTiming;
    end
  else
    begin
      FMainFont.pFontName   := Name;
      FMainFont.pFontHeight := Height;
      FMainFont.pFontBold   := Bold;
      FMainFont.pFontItalic := Italic;
    end;

  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Background Color
//------------------------------------------------------------------------------
procedure TValueForm.OnBackColor (Sender : TObject);
begin
  FCurValueType   := nil;
  FTimeLineSelect := false;
  FWinStartSelect := false;
  FLabelSelect    := false;
  FBackColor.PickColor(nil, OnColor);
  self.Color      := FBackColor.pColor;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Highlight Color
//------------------------------------------------------------------------------
procedure TValueForm.OnLabelColor (Sender : TObject);
begin
  FCurValueType   := nil;
  FTimeLineSelect := false;
  FWinStartSelect := false;
  FLabelSelect    := true;
  FLabelFont.PickColor(nil, OnColor);
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Highlight Color
//------------------------------------------------------------------------------
procedure TValueForm.OnTimeColor (Sender : TObject);
begin
  FCurValueType   := nil;
  FTimeLineSelect := true;
  FWinStartSelect := false;
  FLabelSelect    := false;
  FTimeLineColor.PickColor(nil, OnColor);
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Highlight Color
//------------------------------------------------------------------------------
procedure TValueForm.OnStartColor (Sender : TObject);
begin
  FCurValueType   := nil;
  FTimeLineSelect := false;
  FWinStartSelect := true;
  FLabelSelect    := false;
  FWinStartColor.PickColor(nil, OnColor);
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Adjust Highlight Color
//------------------------------------------------------------------------------
procedure TValueForm.OnMyClose (Sender : TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
//  Adjust Highlight Color
//------------------------------------------------------------------------------
procedure TValueForm.OnResolution (Sender : TObject);
var
  OldPixel  : integer;
  OldSecond : integer;
  NewSecond : integer;
begin
  if (Sender is TMenuItem) then
    begin
      // Remember Current Position of Cursor to Left in Sec
      // FMouseLast is Mouse X-Pos when Menu was popped

      OldPixel  := FMouseLast - FMonRect.Left;
      OldSecond := PixelToSecond(OldPixel);

      FResolution.pDouble := TMenuItem(Sender).Tag;
      CalcLabelTiming;

      // Translate Remembered Pixels to New Seconds

      NewSecond := - (PixelToSecond(OldPixel) - OldSecond);

      // Increment/Descrement Windows Start Position

      FWinStartTime.AddSeconds(NewSecond);
      FWinStartTick := FWinStartTick + NewSecond;

      FWinEndTick := FWinStartTick +
                 PixelToSecond(FMonRect.Right - FMonRect.Left);

      self.Invalidate;
    end;
end; 
//------------------------------------------------------------------------------
//  Adjust Highlight Color
//------------------------------------------------------------------------------
procedure TValueForm.OnColor(Col : TColor);
begin
  if Assigned(FCurValueType) then
    begin
      FCurValueType.pColor := Col;
    end
  else if FTimeLineSelect then
    begin
      FTimeLineColor.pColor := Col;
    end
  else if FWinStartSelect then
    begin
      FWinStartColor.pColor := Col;
    end
  else if FLabelSelect then
    begin
      FLabelFont.pFontColor := COl;
    end
  else
    begin
      FBackColor.pColor := Col;
      self.Color := FBackColor.pColor;
    end;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//
//                                  PAINTING
//
//------------------------------------------------------------------------------
//  1 Second Timer, Repaint if any new Measure
//------------------------------------------------------------------------------
procedure TValueForm.OnMyTimer    (Sender : TObject);
begin
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//  Return Resolution in Seconds per Pixel
//------------------------------------------------------------------------------
function TValueForm.SecondsPerPixel: double;
begin
  // FResolution is in Minutes per Centimeter

  result := (FResolution.pDouble * 2.54 * 60) / Screen.PixelsPerInch;
end;
//------------------------------------------------------------------------------
//  Return Resolution in Minutes per Pixel
//------------------------------------------------------------------------------
function TValueForm.MinutesPerPixel: double;
begin
  // FResolution is in Minutes per Centimeter

  result := (FResolution.pDouble * 2.54) / Screen.PixelsPerInch;
end;
//------------------------------------------------------------------------------
//  Translate Seconds into Pixel
//------------------------------------------------------------------------------
function TValueForm.SecondToPixel(const Second : integer): integer;
begin
  result := round(Second / SecondsPerPixel);
end;
//------------------------------------------------------------------------------
//  Translate Pixel into Second
//------------------------------------------------------------------------------
function TValueForm.PixelToSecond(const Pixel : integer): integer;
begin
  result := round(Pixel * SecondsPerPixel);
end;
//------------------------------------------------------------------------------
// Translate Value (%) into Pixels
//------------------------------------------------------------------------------
function TValueForm.ValueToPixel(const Value : integer): integer;
begin
  result := FMonRect.Bottom - 1 -
            round((Value * (FMonRect.Bottom - FMonRect.Top - 2)) / 100);
end;
//------------------------------------------------------------------------------
// Translate Value (%) into Pixels
//------------------------------------------------------------------------------
function TValueForm.PixelToValue(const Pixel : integer): integer;
begin
  result := round(100 * (FMonRect.Bottom - 1 - Pixel) /
                        (FMonRect.Bottom - FMonRect.Top - 2));
end;
//------------------------------------------------------------------------------
//  Return The Default Label Distance in Pixels
//------------------------------------------------------------------------------
function TValueForm.DefaultLabelMinutes: double;
var
  Mult : integer;
begin
  // Get Number of Minutes per 50 Pixel

  result := SecondsPerPixel * 50 / 60;

  // Translate the result to a value of 0..9.9 * Mult

  Mult := 1;
  while result > 10 do
    begin
      Mult   := Mult   * 10;
      result := result / 10;
    end;

  // Trunk result to 1, 2 or 5 and then Multiply by Mult

       if (result < 1) then result := round(Mult)
  else if (result < 2) then result := round(2 * Mult)
  else if (result < 5) then result := round(5 * Mult)
  else result := round(10 * Mult);

  // We now have a nice rounded Number of Minutes for 50 Pixels
end;
//------------------------------------------------------------------------------
//  Calculate Horizontal Distance and Minutes
//------------------------------------------------------------------------------
procedure TValueForm.CalcLabelTiming;
var
  R : double;
  S : int64;
begin
  // Get A Nice rounded Number of Minutes between Labels

  R := DefaultLabelMinutes;

  // We need this when drawing Time Label Text (but in Seconds)

  FTimeLineDistSeconds := round(R * 60);

  // Translate this back to number of Pixels between Default Labels
  // We need this when drawing Time Lines after the First one

  FTimeLineDistPixels := round(R / MinutesPerPixel);

  // Round off Hours and Minutes from StartTime of the Monitor
  // Window and leave only Seconds

  S := FWinStartTime.pSeconds;
  S := S - trunc(S / 60) * 60;

  FTimeLineFirstSeconds := LARGE_INTEGER(S).LowPart;

  // We need to know the distance to first Line adjusted on the
  // Monitor Start Second when Drawing First TimeLine

  FTimeLineFirstPixels := FTimeLineDistPixels -
                          SecondToPixel(FTimeLineFirstSeconds);
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TValueForm.FormPaint(Sender: TObject);
begin
  inherited;
  PickOrPaint(true);
end;
//------------------------------------------------------------------------------
//  Paint
//------------------------------------------------------------------------------
procedure TValueForm.PickOrPaint(const bPaint : boolean);
var
  CurPos    : TPoint;        // Cursor Position if Select Now
  Ind, X, Y : integer;
  oX, oY    : integer;

  TextRect     : TRect;
  sTmp         : string;
  CurSecond    : integer;
  CurTimeLineX : integer;

  iType  : integer;
  pType  : TMonValueType;

  iValue : integer;
  vTime  : integer;
  vValue : integer;
  tLast  : integer;
  vLast  : integer;
  bFirst : boolean;

  TextSize : TSize;
begin
  // Get Cursor Position

  if bPaint then
    CurPos := Point(0,0)
  else
    begin
      CurPos := self.ScreenToClient(Mouse.CursorPos);
      FCurValueType   := nil;
      FTimeLineSelect := false;
      FWinStartSelect := false;
      FLabelSelect    := false;
    end;
    
  //----------------------------------------------------------------------------
  // Do we need to scroll
  //----------------------------------------------------------------------------

  // Get Current TimeLine X Position

  TempTime.Now;

  TempTime.Subtract(FWinStartTime);
  CurSecond := TempTime.pSeconds;

  // Translate this into X Pixel

  CurTimeLineX := FMonRect.Left +
                  round(CurSecond * FTimeLineDistPixels /
                  (FTimeLineDistSeconds));

  // If the TimeLine was visible last repaint and its not visible now
  // then scroll to the left to make it visible again

  if  bPaint and (not FMouseDown) and
     (FTimeLineX >= FMonRect.Left) and
     (FTimeLineX <= FMonRect.Right) then
    begin
      if (CurTimeLineX > FMonRect.Right) then
        begin
          // Scroll WIndow at least 2 pixels

          CurSecond := Max(1, PixelToSecond(2));
          Log('Scroll Left ' + IntToStr(CurSecond) + ' Seconds');

          FWinStartTime.AddSeconds(CurSecond);
          Inc(FWinStartTick, CurSecond);
          FWinEndTick := FWinStartTick +
                   PixelToSecond(FMonRect.Right - FMonRect.Left);
                   
          // Calculate new Cursor Position

          TempTime.Now;

          TempTime.Subtract(FWinStartTime);
          CurSecond := TempTime.pSeconds;

          CurTimeLineX := FMonRect.Left +
                  round(CurSecond * FTimeLineDistPixels /
                  (FTimeLineDistSeconds));
        end;
    end;

  //----------------------------------------------------------------------------
  // Draw horizontal Lines and Labels with Highlight Color
  //----------------------------------------------------------------------------

  if bPaint then
    begin
      // Set Pen Style

      Canvas.Pen.Style := psSolid;

      if FLabelSelect then
        Canvas.Pen.Width := 2
      else
        Canvas.Pen.Width := 0;

      canvas.Pen.Color := FLabelFont.pFontColor;

      Canvas.Brush.Style := bsClear;

      // Draw the Monitor Window Rectangle

      Canvas.Rectangle(FMonRect);

      Canvas.Pen.Width := 0;

      // Set Label Font

      Canvas.Font := FLabelFont.pFont;
      SetBkMode(Canvas.Handle, TRANSPARENT);

      // Set X Position and Size of Horizontal Label

      TextRect.Left  := TBRD div 2;
      TextRect.Right := FMonRect.Right;

      // Draw Lines from 0 to 9 (not first and not last) Dotted

      Canvas.Pen.Style := psDot;

      for Ind := 1 to 9 do
        begin
          // Set Y of this Line

          Y := round(Ind * (FMonRect.Bottom - FMonRect.Top) / 10);

          // Draw Line from Label to End of Window

          Canvas.MoveTo(FMonRect.Left,  Y);
          Canvas.LineTo(FMonRect.Right, Y);

          // Adjust Label Top and Bottom

          TextRect.Top    := Y - FLabelSize.cy div 2;
          TextRect.Bottom := Y + FLabelSize.cy div 2;

          // Draw Label Left adjusted and as a Single Line

          DrawtextEx(Canvas.Handle,
                 PAnsiChar(IntToStr(10 * (10 - Ind))),
                 -1,
                 TextRect,
                 DT_LEFT or DT_SINGLELINE, nil);
        end;
    end
  else
    begin
      if (CurPos.X < FMonRect.Left) or
         (CurPos.Y > FMonRect.Bottom) then
        begin
          FLabelSelect   := true;
          EXIT;
        end;
    end;

  //----------------------------------------------------------------------------
  // Draw Vertical Lines and Labels with Highlight Color
  //----------------------------------------------------------------------------

  if bPaint then
    begin
      // Set Label Top and Bottom

      TextRect.Top    := FMonRect.Bottom + TBRD div 2;
      TextRect.Bottom := self.ClientHeight;

      // Get Time of Windows Left Corner from Monitor Window Start Time

      TempTime.pFileTime := FWinStartTime.pFileTime;

      // Set up Start X position for the Left Side of Monitor Window using
      // the FTimeLIneFirstPixels calculated before

      X := FMonRect.Left + FTimeLIneFirstPixels;

      // Draw from Start to End of Window's Right Side

      Repeat
        // Draw the Line

        Canvas.MoveTo(X, FMonRect.Top);
        Canvas.LineTo(X, FMonRect.Bottom);

        // Calculate Label Text and translate to text

        TempTime.AddSeconds(FTimeLineDistSeconds);
        sTmp := TempTime.pTimeHHMM;

        // Set Label Left and Right position

        TextRect.Left  := X - FLabelSize.cx * 2;
        TextRect.Right := X + FLabelSize.cx * 2;

        // Draw Label HH:MM centered

        DrawtextEx(Canvas.Handle, PAnsiChar(sTmp), -1, TextRect,
                DT_CENTER or DT_SINGLELINE, nil);

        // Set next Index and Line X position for next repeat

        Inc(X, FTimeLineDistPixels);

      until (X > self.ClientWidth);
    end;

  //----------------------------------------------------------------------------
  // Draw a Line when the Monitor Started to Measure
  //----------------------------------------------------------------------------

  // Cet Current Time from Monitor Start Time in seconds

  TempTime.pFileTime := FMonStartTime.pFileTime;

  TempTime.Subtract(FWinStartTime);
  CurSecond := TempTime.pSeconds;

  // Translate this into X Pixel

  FWinStartPixel := FMonRect.Left + round(CurSecond * FTimeLineDistPixels /
                                            (FTimeLineDistSeconds));

  // Draw Line if it Visible

  if (FWinStartPixel > FMonRect.Left) and
     (FWinStartPixel < FMonRect.Right) then
    begin
      if bPaint then
        begin
          // Set Pen to Orange Color and Solid Style

          Canvas.Pen.Color := FWinStartColor.pColor;
          Canvas.Pen.Style := psSolid;

          if FWinStartSelect then
            Canvas.Pen.Width := 4
          else
            Canvas.Pen.Width := 2;

          // Draw the Line

          Canvas.MoveTo(FWinStartPixel, FMonRect.Top + 2);
          Canvas.LineTo(FWinStartPixel, FMonRect.Bottom - 3);
        end
      else
        begin
          if PointOnLine(FWinStartPixel, FMonRect.Top + 2,
                         FWinStartPixel, FMonRect.Bottom - 3,
                              CurPos.X, CurPos.Y, 8) then
            begin
              FWinStartSelect := true;
              EXIT;
            end;
        end;
    end;
  //----------------------------------------------------------------------------
  // Draw The ValueTypes as Text in the upper Left Corner
  //----------------------------------------------------------------------------

  TextRect.Left := FMonRect.Left + 20;
  TextRect.Top  := FMonRect.Top  + 12;

  Canvas.Font := FMainFont.pFont;
  SetBkMode(Canvas.Handle, TRANSPARENT);

  iType := 0;
  while FValueTypeList.IterValueTypes(iType, pType) do
    begin
      Canvas.Font.Color := pType.pColor;

      if pType = FCurValueType then
        Canvas.Font.Style := Canvas.Font.Style + [fsBold]
      else
        Canvas.Font.Style := FMainFont.pFont.Style;

      if pType.pVisible then
        sTmp := chr(149)
      else
        sTmp := '';

      TextSize := Canvas.TextExtent('X ');

      TextRect.Left   := FMonRect.Left + 20;
      TextRect.Right  := TextRect.Left + TextSize.cx;
      TextRect.Bottom := TextRect.Top + TextSize.cy;

      if bPaint then
        begin
          if pType.pVisible then

          // Draw Visible

          DrawtextEx(Canvas.Handle, PAnsiChar(sTmp), -1, TextRect,
                DT_LEFT or DT_SINGLELINE, nil);
        end
      else if PtInRect(TextRect, CurPos) then
        begin
          FCurValueType   := pType;
          EXIT
        end;

      TextRect.Left := TextRect.Right;

      sTmp := pType.pLabel;

      TextSize := Canvas.TextExtent(sTmp);

      TextRect.Right := TextRect.Left + TextSize.cx;
      TextRect.Bottom := TextRect.Top + TextSize.cy;

      if bPaint then
        begin
          // Draw Label HH:MM centered

          DrawtextEx(Canvas.Handle, PAnsiChar(sTmp), -1, TextRect,
                DT_LEFT or DT_SINGLELINE, nil);
        end
      else if PtInRect(TextRect, CurPos) then
        begin
          FCurValueType   := pType;
          EXIT
        end;

      Inc(TextRect.Top, TextSize.cy + 4);
    end;

  //----------------------------------------------------------------------------
  // Draw The Values
  //----------------------------------------------------------------------------

  X := 0; Y := 0; oX := 0; oY := 0;
  iType := 0;
  while FValueTypeList.IterValueTypes(iType, pType) do
    if pType.pVisible then
      begin
        if bPaint then
          begin
            Canvas.Pen.Style := psSolid;
            Canvas.Pen.Color := pType.pColor;

            if pType = FCurValueType then
              Canvas.Pen.Width := 2
            else
              Canvas.Pen.Width := 1;
          end;

        bFirst := true;
        iValue := 0;
        while pType.IterTimeValues(iValue,
            FWinStartTick , FWinEndTick, vTime, vValue, tLast, vLast) do
          begin
            if bFirst then
              begin
                // This is the First real Value, Move to Left Side
                // using the Last Value

                if (FWinStartPixel > FMonRect.Left) then
                  X := FWinStartPixel
                else
                  X := FMonRect.Left;
                  
                Y := ValueToPixel(vLast);

                if bPaint then
                  Canvas.MoveTo(X, Y)
                else
                  begin
                    oX := X;
                    oY := Y;
                  end;

                // Set new X Value and draw a Line to that with Old Y Value

                X := FMonRect.Left + SecondToPixel(vTime - FWinStartTick);

                if bPaint then
                  Canvas.LineTo(X, Y)
                else
                  begin
                    if  PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType   := pType;
                        EXIT;
                      end;
                     oX := X;
                     oY := Y;
                  end;

                // Set The New Y Value and draw to that

                Y := ValueToPixel(vValue);

                if bPaint then
                  Canvas.LineTo(X, Y)
                else
                  begin
                    if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType := pType;
                        EXIT;
                      end;
                     oX := X;
                     oY := Y;
                  end;

                bFirst := false;
              end
            else
              begin
                // Set new X Value and draw a Line to that

                X := FMonRect.Left + SecondToPixel(vTime - FWinStartTick);

                if bPaint then
                  Canvas.LineTo(X, Y)
                else
                  begin
                    if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType   := pType;
                        EXIT;
                      end;
                    oX := X;
                    oY := Y;
                  end;

                // Set The New Y Value and draw to that

                Y := ValueToPixel(vValue);

                if bPaint then
                  Canvas.LineTo(X, Y)
                else
                  begin
                    if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType := pType;
                        EXIT;
                      end;
                    oX := X;
                    oY := Y;
                  end;

              end;

            (*Log('From ' + IntToStr(FWinStartTick) +
                '-'     + IntToStr(FWinEndTick) +
                ' ('    + IntToStr(vTime) + ')' +
                ' = '   + INtToStr(vValue) +
                ' ('    + IntToStr(vLast) + ')' +
                ' P '   + ToStr(Point(X,Y))); *)
          end;

        if bFirst then
          begin
            // No Value existed in the Window, draw a Line from Left
            // to Right with the Last Value, if TempLine is to the Right of
            // Windows Left edge

            if (CurTimeLineX > FMonRect.Left) then
              begin
                if (FWinStartPixel > FMonRect.Left) then
                  X := FWinStartPixel
                else
                  X := FMonRect.Left;

                Y := ValueToPixel(vLast);

                if bPaint then
                  Canvas.MoveTo(X, Y)
                else
                  begin
                    if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType   := pType;
                        EXIT;
                      end;
                    oX := X;
                    oY := Y;
                  end;

                if (CurTimeLineX < FMonRect.Right) and
                   (CurTimeLineX > FMonRect.Left) then
                  X := CurTimeLineX
                else
                  X := FMonRect.Right;

                if bPaint then
                  Canvas.LineTo(X, Y)
                else
                  begin
                    if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType := pType;
                        EXIT;
                      end;
                    oX := X;
                    oY := Y;
                  end;
              end;
          end
        else 
          begin
            // At least one Point was inside Window, If TempLine is Visible
            // Draw to that

            if (CurTimeLineX >= FMonRect.Left) and
               (CurTimeLineX <= FMonRect.Right) then
              begin
                if (X < CurTimeLineX) then
                  begin
                    X := CurTimeLineX;

                    if bPaint then
                      Canvas.LineTo(X, Y)
                    else
                      begin
                        if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                          begin
                            FCurValueType := pType;
                            EXIT;
                          end;
                      end;
                  end;
              end
            else
              begin
                X := FMonRect.Right;

                if bPaint then
                  Canvas.LineTo(FMonRect.Right, Y)
                else
                  begin
                    if PointOnLine(oX, oY, X, Y, CurPos.X, CurPos.Y, 4) then
                      begin
                        FCurValueType := pType;
                        EXIT;
                      end;
                  end;
              end;
          end;
      end;

  //----------------------------------------------------------------------------
  // Draw the Time Line
  //----------------------------------------------------------------------------

  // Draw Line if it Visible

  if (CurTimeLineX > FMonRect.Left) and
     (CurTimeLineX < FMonRect.Right) then
    begin
      if bPaint then
        begin
          // Set Pen to Yellow Color and Solid Style

          Canvas.Pen.Color := FTimeLIneColor.pColor;
          Canvas.Pen.Style := psSolid;

          if FTimeLineSelect then
            Canvas.Pen.Width := 3
          else
            Canvas.Pen.Width := 1;

          // Draw the Line

          Canvas.MoveTo(CurTimeLineX, FMonRect.Top + 2);
          Canvas.LineTo(CurTimeLineX, FMonRect.Bottom - 2);
        end
      else
        begin
          if PointOnLine(CurTimeLineX, FMonRect.Top + 2,
                         CurTimeLineX, FMonRect.Bottom - 2,
                              CurPos.X, CurPos.Y, 8) then
            begin
              FTimeLineSelect := true;
              EXIT;
            end;
        end;
    end;

  // Remember The Current Position of the TimeLine

  FTimeLineX := CurTimeLineX;
end;
//------------------------------------------------------------------------------
//
//                                   RESIZING
//
//------------------------------------------------------------------------------
//  User Resized Form
//------------------------------------------------------------------------------
procedure TValueForm.FormResize(Sender: TObject);
begin
  if FStarted then
    begin
      // Set Monitor Window's Rectangle

      FMonRect   := Rect(FLabelSize.cx + 2, 0,
                     self.ClientWidth,
                     self.ClientHeight - FLabelSize.cy - 2);

      // Set a New End Value in Seconds

      FWinEndTick := FWinStartTick +
          PixelToSecond(FMonRect.Right - FMonRect.Left);

      self.Invalidate;
    end;
end;
//------------------------------------------------------------------------------
//
//                                   MOUSE WHEEL
//
//------------------------------------------------------------------------------
//  Change Resolution
//------------------------------------------------------------------------------
procedure TValueForm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  OldPixel  : integer;
  OldSecond : integer;
  NewSecond : integer;
begin
  // Remember Current Position of Cursor to Left in Sec (MousePos in screen)

  OldPixel  := self.ScreenToClient(Mouse.CursorPos).X - FMonRect.Left;
  OldSecond := PixelToSecond(OldPixel);

  // Change Resolution
  // Max resolution is 0.2 Min/cm and Min resolution is 4 Hours / cm

  if WheelDelta > 0 then
    FResolution.pDouble := Min(FResolution.pDouble * 1.2, 4 * 60)
  else
    FResolution.pDouble := Max(FResolution.pDouble / 1.2, 0.1);

  Handled := true;

  CalcLabelTiming;

  // Translate Remembered Pixels to New Seconds

  NewSecond := - (PixelToSecond(OldPixel) - OldSecond);

  // Increment/Descrement Windows Start Position

  FWinStartTime.AddSeconds(NewSecond);
  FWinStartTick := FWinStartTick + NewSecond;

  FWinEndTick := FWinStartTick +
                 PixelToSecond(FMonRect.Right - FMonRect.Left);

  CalcLabelTiming;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//
//                                   MOUSE MOVE
//
//------------------------------------------------------------------------------
//  Mouse Down
//------------------------------------------------------------------------------
procedure TValueForm.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  // Perform a Pick

  self.PickOrPaint(false);
  if Assigned(FCurValueType) then
    begin
      // We got a Value Type, just repaint

      self.Invalidate
    end
  else if (not FMouseDblClick) and (ssLeft in Shift) then
    begin
      FCurValueType := nil;
      FMouseDown    := true;
      FMouseLast    := X;
      self.Invalidate;
    end;

  FMouseDblClick := false;
end;
//------------------------------------------------------------------------------
//  Mouse Move
//------------------------------------------------------------------------------
procedure TValueForm.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  dx : integer;
  Sec  : integer;
begin
  if FMouseDown then
    begin
      // Get Delta Movement from last Move

      dx := X - FMouseLast;
      if (abs(Dx) > 0) then
        begin
          // Translate Delta X from Pixels to Seconds and move if changed

          Sec := PixelToSecond(-dx);
          if (abs(sec) > 0) then
            begin
              FMouseLast := X;

              // Add Seconds to Current Monitor Windows Start Time

              FWinStartTime.AddSeconds(Sec);
              FWinStartTick := FWinStartTick + Sec;

              FWinEndTick   := FWinStartTick +
                   PixelToSecond(FMonRect.Right - FMonRect.Left);

              // Calculate and repaint

              CalcLabelTiming;
              self.invalidate;
            end;
        end;
    end
  else if Application.ShowHint then
    begin
      (*
      self.PickOrPaint(false);
      if Assigned(FCurValueType) then
        begin
          self.Hint := FCurValueType.pLabel;
        end
      else if FTimeLineSelect then
        begin
          self.Hint := 'Time Line';
        end
      else if FWinStartSelect then
        begin
          self.Hint := 'Start ' + FMonStartTime.pDateTime;
        end
      else if FLabelSelect then
        begin
          self.Hint := 'Labels';
        end
      else
        begin
          self.Hint := 'BackGround';
        end;

      HintManager.SetHintPos(Point(X,Y), hptNone);
      Application.HintHidePause := 30000;
      Application.ActivateHint(Mouse.CursorPos); *)
    end;
end;
//------------------------------------------------------------------------------
//  Mouse Up
//------------------------------------------------------------------------------
procedure TValueForm.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FMouseDown := false;
end;
//------------------------------------------------------------------------------
//  User Double Clicked
//------------------------------------------------------------------------------
procedure TValueForm.FormDblClick(Sender: TObject);
begin
  FMouseDblClick := true;

  self.PickOrPaint(false);
  if Assigned(FCurValueType) then
    begin
      FCurValueType.pVisible := not FCurValueType.pVisible;
      //if TGenColorPick.PickColor(FCurValueType.pColor, OnColor, nColor) then
      //  FCurValueType.pColor := nColor;
    end
  else if FTimeLineSelect then
    begin
      FTimeLineColor.PickColor(nil, OnColor);
    end
  else if FWinStartSelect then
    begin
      FWinStartColor.PickColor(nil, OnColor);
    end
  else if FLabelSelect then
    begin
      FLabelFont.PickColor(nil, OnColor);
    end
  else
    begin
      FBackColor.PickColor(nil, OnColor);
      self.Color := FBackColor.pColor;
    end;
  self.Invalidate;
end;
//------------------------------------------------------------------------------
//                                  INITIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TValueForm);
end.
