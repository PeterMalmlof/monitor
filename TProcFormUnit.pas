unit TProcFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Contnrs, DateUtils,

  TGenAppPropUnit,      // Application Properties
  TPmaProcessUnit,      // Process Object
  TPmaProcessListUnit;  // Process List

//------------------------------------------------------------------------------
//  Form
//------------------------------------------------------------------------------
type
  TProcForm = class(TForm)
    BottomPanel: TPanel;
    Pages: TPageControl;
    TabLog: TTabSheet;
    ProcList: TListView;
    TabAlive: TTabSheet;
    ProcAlive: TListView;
    TabHier: TTabSheet;
    ProcHier: TTreeView;
    ProcPanel: TPanel;
    ProcName: TLabel;
    ProcDesc: TMemo;
    LiveProc: TGroupBox;
    LabelPrio: TLabel;
    PropPrio: TLabel;
    LabelParent: TLabel;
    PropParent: TLabel;
    LabelCreated: TLabel;
    PropCreated: TLabel;
    LabelPath: TLabel;
    PropPath: TLabel;
    LabelThreads: TLabel;
    PropThreads: TLabel;
    LabelMem: TLabel;
    PropMem: TLabel;
    LabelUsage: TLabel;
    PropUsage: TLabel;
    NotLevel: TGroupBox;
    BtnNormal: TRadioButton;
    BtnWarn: TRadioButton;
    BtnAlarm: TRadioButton;
    Splitter1: TSplitter;
    LabelHelp: TMemo;
    LabelUsageAvg: TLabel;
    PropUsageAvg: TLabel;
    LabelCmdLine: TLabel;
    PropCmdLine: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ProcListChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure ProcDescChange(Sender: TObject);
    procedure BtnNormalClick(Sender: TObject);
    procedure BtnWarnClick(Sender: TObject);
    procedure BtnAlarmClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure TabLogResize(Sender: TObject);
    procedure TabAliveResize(Sender: TObject);
    procedure PagesChange(Sender: TObject);
    procedure ProcAliveChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure TabHierResize(Sender: TObject);
    procedure ProcPanelResize(Sender: TObject);
    procedure ProcHierChange(Sender: TObject; Node: TTreeNode);
    procedure ProcListCustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure Splitter1CanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure ProcHierCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure ProcAliveCustomDrawItem(Sender: TCustomListView;
      Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);

    procedure ProcDoubleClick(Sender: TObject);
    procedure ProcListCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure ProcAliveSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    objMainRect  : TGenAppPropRect; // Window Rect
    objViewWidth : TGenAppPropInt;  // Left Splitter
    objCurTab    : TGenAppPropInt;  // Current Tab

    // OnInit is set when listboxes are updated and used internally to
    // filter out change events from other controls

    objOnInit  : boolean;

    // When user selects a process in a listbox its made the current process

    objProcObj : TPmaProcess;  // Proc Object (if available)

    // The Singleton Processes contains:
    //
    // 1) All processes currently loaded
    // 2) Pointer to the Process List File object that holds all processes
    //    added or removed while the monitor worked
    // 3) Pointer to the Process Description object that holds all process
    //    descriptions in a buffer.

    // The Timer is used for updating the current usage of process monitored
    // Its only used when the Current Process Tab is selected

    objTimer : TTimer;

    //--------------------------------------------------------------------------
    // Main Form Methods

    procedure SetHelp(const Value : string);

    procedure SetCurProcessData;

    procedure SetNotificationLevel(const nNotLevel : integer);
    procedure SetButtonNotificationLevel;

    procedure OnTimerUpdate(Senter : TObject);

    //--------------------------------------------------------------------------
    // Process Log List Tab

    procedure LoadProcListLog;

    procedure SetLogSelected;

    //--------------------------------------------------------------------------
    // Current Processes Tab

    procedure LoadCurProcesses;

    procedure SetRunSelected;

    //--------------------------------------------------------------------------
    //Process Hierachy Tab

    procedure LoadProcHier;
    function  GetHierProcNode(const pProc : TPmaProcess):TTreeNode;
    procedure SetHierSelected;

    class procedure Log(const Line : string);
  public

    // Initiates the dialog after its been created and loads all processes

    procedure Init;

    property pHint : string write SetHelp;
  end;

var
  ProcForm: TProcForm;

implementation

{$R *.dfm}

uses
  TGenStrUnit,       // String Functions
  TGenGraphicsUnit,  // Graphic Functions
  TPmaLogUnit,       // Logging
  TPmaDateTimeUnit,  // Date Time Object
  TPmaProcessUtils,  // Process List
  TPmaFormUtils,     // Form List
  TPmaClassesUnit;   // Classes

resourcestring
  resNA          = 'NA';
  resAdded       = 'Added';
  resRemoved     = 'Removed';
  resUsageAvg    = ' (Average over its life)';
  resUsageAvg3   = ' (Average over a Minute)';
  resNotAlive    = 'Not Alive';
  resMonitored   = ' Monitored';
  resAlive       = ' Alive';
  resNoParent    = 'No Parent';
  resNoSelection = 'No Process Selected';

const
  Prf          = 'ProcList';
  PrfRect      = 'Rect';
  PrfViewWidth = 'ViewWidth';
  PrfCurTab    = 'CurTab';

  BRD     = 4;
  HELPHGT = 25;

  //----------------------------------------------------------------------------
  // Page Indexes

  PageLoggedInd    = 0;
  PageRunningInd   = 1;
  PageHierarchyInd = 2;

  //----------------------------------------------------------------------------
  // Log Tab Column Index

  LogColumnPidInd     = 0;
  LogColumnNameInd    = 1;
  LogColumnCreatedInd = 2;
  LogColumnEventInd   = 3;
  LogColumnNoteInd    = 4;

  // Log Tab Subitem Index

  SubItemName    = 0;
  SubItemCreated = 1;
  SubItemEvent   = 2;
  SubItemNote    = 3;

  //----------------------------------------------------------------------------
  // Running Process List Column Indexes

  RunColumnPidInd   = 0;
  RunColumnNameInd  = 1;
  RunColumnPrioInd  = 2;
  RunColumnLongInd  = 3;
  RunColumnShortInd = 4;
  RunColumnMemInd   = 5;

  RunSubItemNameInd  = 0;
  RunSubItemPrioInd  = 1;
  RunSubItemLongInd  = 2;
  RunSubItemShortInd = 3;
  RunSubItemMemInd   = 4;

//------------------------------------------------------------------------------
//  CLASS: Log
//------------------------------------------------------------------------------
class procedure TProcForm.Log(const Line : string);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(Line);
end;
//------------------------------------------------------------------------------
//  On Create
//------------------------------------------------------------------------------
procedure TProcForm.FormCreate(Sender: TObject);
begin
  objOnInit := true;

  Log('TProcForm.FormCreate');

  // Set default data on some controls

  ProcDesc.Text := '';
  LabelHelp.Color := self.Color;

  BtnWarn.Color  := TPmaProcess.NotLevelColor(pnWarn);
  BtnAlarm.Color := TPmaProcess.NotLevelColor(pnAlarm);

  SPlitter1.MinSize := 100;
  SPlitter1.ResizeStyle := rsUpdate;
  
  // Read all Application Properties

  objMainRect := App.CreatePropRect(Prf, PrfRect, InMidScreen);
  objMainRect.SetBounds(self);

  objViewWidth := App.CreatePropInt(Prf, PrfViewWidth, 300);
  Pages.Width := objViewWidth.pInt;

  objCurTab := App.CreatePropInt(Prf, PrfCurTab, 0);

  // Avoid flickering when resizing

  TPmaFormUtils.SetDblBuffered(self, true);

  // Set current process attributes

  objProcObj := nil;

  // Prepare timer, but dont start it

  objTimer := TTimer.Create(nil);
  objTimer.Interval := 1000;
  objTimer.OnTimer  := OnTimerUpdate;
  objTimer.Enabled  := false;

  // Initiation is Done

  objOnInit := false;
end;
//------------------------------------------------------------------------------
//  Startup is called by client when its ready to load all things
//------------------------------------------------------------------------------
procedure TProcForm.Init;
begin
  // We need to have Process List Log already loaded

  if (not (objCurTab.pInt = PageLoggedInd)) then
    LoadProcListLog;

  // Set Current Tab

  Pages.ActivePageIndex := objCurTab.pInt;

  // Set default data depending on current tab

  PagesChange(nil);

  SetCurProcessData;
end;
//------------------------------------------------------------------------------
//  On Destroy
//------------------------------------------------------------------------------
procedure TProcForm.FormDestroy(Sender: TObject);
begin
  objOnInit := true;

  Log('TProcForm.FormDestroy');

  // Stop and Free the Timer

  objTimer.Enabled := false;
  objTimer.Free;

  // Write all Properties to Ini file

  objMainRect.GetBounds(self);

  objViewWidth.pInt := Pages.Width;

  // Release it from Parent Window

  if Assigned(self.Owner) then
    begin
      Log('Owner ' + Owner.ClassName);
      Owner.RemoveComponent(self);
    end;
end;
//------------------------------------------------------------------------------
//
//                               GENERIC METHODS
//
//------------------------------------------------------------------------------
//  User Changed Tab
//------------------------------------------------------------------------------
procedure TProcForm.PagesChange(Sender: TObject);
begin
  // Disable timer. Turns on if applicable depending on Tab

  objTimer.Enabled := false;

  // Remember this Tab

  objCurTab.pInt := Pages.ActivePageIndex;

  case Pages.ActivePageIndex of

    PageLoggedInd :
      begin
        LoadProcListLog;
        SetHelp(
          'These processes has been loaded since Windows Boot. ' +
          'You can update description and Notification Level anyway. ' +
          'Blue processes are still running. ' +
          'Double Click to Google');
        SetLogSelected;
      end;
    PageRunningInd :
      begin
        LoadCurProcesses;
        SetHelp(
          'Currently running processes. ' +
          'You can update description and Notification Level. ' +
          'Double Click to Google');
        SetRunSelected;
      end;
    PageHierarchyInd :
      begin
        LoadProcHier;
        SetHelp(
          'Currently running processes and their parent/child hierarchy. ' +
          'You can update description and Notification Level. ' +
          'Double Click to Google');
        SetHierSelected;
      end;
  end;

  objTimer.Enabled := true;
end;
//------------------------------------------------------------------------------
//  Set Help Text
//------------------------------------------------------------------------------
procedure TProcForm.SetHelp(const Value : string);
begin
  LabelHelp.Clear;
  LabelHelp.Lines.Add(Value);
  LabelHelp.SelStart  := 0;
  LabelHelp.SelLength := 0;
end;
//------------------------------------------------------------------------------
//  Resize the Main Form
//------------------------------------------------------------------------------
procedure TProcForm.FormResize(Sender: TObject);
begin

  // BOttom Panel above OK button from left to right

  BottomPanel.Left   := BRD;
  BottomPanel.Width  := self.ClientWidth - BRD * 2;
  BottomPanel.Top    := BRD;
  BottomPanel.Height := self.ClientHeight - HELPHGT - BRD * 3;

  // The Help to the right of the button

  LabelHelp.Left   := BRD;
  LabelHelp.Width  := self.ClientWidth - BRD * 2;
  LabelHelp.Top    := self.ClientHeight - HELPHGT;
  LabelHelp.Height := HELPHGT;

  Splitter1.Width := BRD;
end;
//------------------------------------------------------------------------------
//  Set Min Size of the Form
//------------------------------------------------------------------------------
procedure TProcForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  if (NewHeight < 400) then NewHeight := 400;
  if (NewWidth  < (Pages.Width + 250)) then NewWidth := (Pages.Width + 250);
end;
//------------------------------------------------------------------------------
//  Set Min Size for the Splitter Bar
//------------------------------------------------------------------------------
procedure TProcForm.Splitter1CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
  if NewSize < 200 then NewSize := 200
  else if (NewSize > (BottomPanel.ClientWidth - 250)) then
    NewSize := BottomPanel.ClientWidth - 250;
end;
//------------------------------------------------------------------------------
//
//                              PROCESS EDIT SECTION
//
//------------------------------------------------------------------------------
//  Set Current Process Information
//------------------------------------------------------------------------------
procedure TProcForm.SetCurProcessData;
var
  bOnInit   : boolean;
  pParent   : TPmaProcess;
begin
  bOnInit := objOnInit;
  objOnInit := true;

  // 1) The Process Name is set in objCurProcName
  // 2) objProcObj may hold a pointer to a TGenProcess or TProcLogItem

  // Test if the Processes happens to be killed

  if (objProcObj <> nil) and (not Processes.IsProc(objProcObj)) then
    objProcObj := nil;
    
  // Set Process Name and Description

  if (objProcObj <> nil) then
    begin
      ProcName.Caption := objProcObj.pName;
      ProcDesc.Text    := objProcObj.pDesc;
      ProcDesc.Enabled := true;
    end
  else
    begin
      ProcName.Caption := resNoSelection;
      ProcDesc.Text    := resNA;
      ProcDesc.Enabled := false;
    end;

  // Set Process Notification Level

  SetButtonNotificationLevel;

  // Add properties that only exists if Process is still running

  if (objProcObj <> nil) then
    begin
      PropPrio.Caption    := IntToStr(objProcObj.pPriority);

      // Set Parent

      pParent := Processes.GetParent(objProcObj);
      if (pParent <> nil) then
        begin
          // Parent still running

          PropParent.Caption  := pParent.pNameId;
        end
      else if (objProcObj.pParentId = 0) then
        begin
          PropParent.Caption  := resNoParent
        end;

      // Set Creation Time. Add still running if alive, Destroy time if dead

      if objProcObj.pAlive then
        begin
          PropCreated.Caption :=
            TPmaDateTime.FileTimeToFullStr(objProcObj.pCreated) + resAlive +

            ' (' + IntToStr(objProcObj.pCreTick) + ')'
            //objProcObj.pCreateTime.pDateTimeMilli + resAlive;
        end
      else
        begin
          PropCreated.Caption :=
            TPmaDateTime.FileTimeToFullStr(objProcObj.pCreated) +
            ' (' + IntToStr(objProcObj.pCreTick) + ')' +

            ' ' + resRemoved +
            TPmaDateTime.FileTimeToFullStr(objProcObj.pDestroyed);

           //   objProcObj.pCreateTime.pDateTimeMilli + ' ' +
           //   resRemoved + ' ' + objProcObj.pDestroyTime.pDateTimeMilli;
        end;

      PropThreads.Caption := IntToStr(objProcObj.pThreads);
      PropMem.Caption     := SizeToStr(objProcObj.pMemSize);
      PropPath.Caption    := objProcObj.pPath;
      PropCmdLine.Caption := objProcObj.pCmdLine;

      // Show Usage Long from Start of Process

      PropUsage.Caption := trim(ToStr(objProcObj.pUsageLong,2) + '%') +
                  resUsageAvg;

      // Show Usage For 5 Minutes Average

      PropUsageAvg.Caption := trim(ToStr(objProcObj.pUsageAvg,2) + '%') +
                  resUsageAvg3
    end

  // Set properties to Not Applicable
  
  else
    begin
      PropPrio.Caption     := resNA;
      PropParent.Caption   := resNA;
      PropCreated.Caption  := resNA;
      PropPath.Caption     := resNA;
      PropThreads.Caption  := resNA;
      PropMem.Caption      := resNA;
      PropUsage.Caption    := resNA;
      PropUsageAvg.Caption := resNA;
    end;

  objOnInit := bOnInit;
end;
//------------------------------------------------------------------------------
//  User has Changed Description
//------------------------------------------------------------------------------
procedure TProcForm.ProcDescChange(Sender: TObject);
begin
  if (not objOnInit) and ProcDesc.Enabled then
    begin
      // Update Process Description for All Processes with Same Name

      if Assigned(objProcObj) and Assigned(Processes) then
        Processes.SetProcDesc(objProcObj.pName, ProcDesc.Text);
    end;
end; 
//------------------------------------------------------------------------------
//  Google
//------------------------------------------------------------------------------
procedure TProcForm.ProcDoubleClick(Sender: TObject);
var
  sErr : string;
begin
  if (objProcObj <> nil) then
    begin
      if (not DoShellCmd(
          '"http://www.google.se/search?hl=sv&source=hp&q=' + objProcObj.pName +
          '&meta=&aq=f&oq="', '',false, sErr)) then
        Log('ERROR: Google: ' + sErr);
    end;
end;
//------------------------------------------------------------------------------
//  Set Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.SetNotificationLevel(const nNotLevel : integer);
var
  Ind : integer;
begin
  // Update relevant Proc Object, and Process Description Object

  if (objProcObj <> nil) and Assigned(Processes) then
    begin
      Processes.SetProcNotLevel(objProcObj.pName, nNotLevel);

      SetButtonNotificationLevel;

      // Update appropriate ListView of TreeView

      case Pages.ActivePageIndex of
        PageLoggedInd    :
          begin
            // Refresh all Item's Notefication Level

            for Ind := 0 to ProcList.Items.Count - 1 do
              ProcList.Items[Ind].SubItems[SubItemNote] :=
                TPmaProcess(ProcList.Items[Ind].Data).pNotLevelStr[1];

            ProcList.Invalidate;
          end;

        PageRunningInd   : ProcAlive.Invalidate;

        PageHierarchyInd : ProcHier.Invalidate;
      end;
    end;
end;
//------------------------------------------------------------------------------
//  Set Button Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.SetButtonNotificationLevel;
var
  nNotLevel : integer;
begin

  if (objProcObj <> nil) then
    nNotLevel := objProcObj.pNotLevel
  else
    begin
      BtnNormal.Checked := false;
      BtnWarn.Checked   := false;
      BtnAlarm.Checked  := false;

      BtnNormal.Enabled := false;
      BtnWarn.Enabled   := false;
      BtnAlarm.Enabled  := false;
      EXIT;
    end;

  BtnNormal.Enabled := true;
  BtnWarn.Enabled   := true;
  BtnAlarm.Enabled  := true;

  case nNotLevel of
    pnNormal :  begin
                  BtnNormal.Checked := true;
                  BtnWarn.Checked   := false;
                  BtnAlarm.Checked  := false;
                end;
    pnWarn   : begin
                  BtnNormal.Checked := false;
                  BtnWarn.Checked   := true;
                  BtnAlarm.Checked  := false;
                end;
    pnAlarm  : begin
                  BtnNormal.Checked := false;
                  BtnWarn.Checked   := false;
                  BtnAlarm.Checked  := true;
                end;
    else
      BtnNormal.Checked := false;
      BtnWarn.Checked   := false;
      BtnAlarm.Checked  := false;
  end;
end;
//------------------------------------------------------------------------------
//  Set Normal Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.BtnNormalClick(Sender: TObject);
begin
  if (not objOnInit) then
    SetNotificationLevel(pnNormal);
end;
//------------------------------------------------------------------------------
//  Set Warning Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.BtnWarnClick(Sender: TObject);
begin
  if (not objOnInit) then
    SetNotificationLevel(pnWarn);
end;
//------------------------------------------------------------------------------
//  Set Alarm Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.BtnAlarmClick(Sender: TObject);
begin
  if (not objOnInit) then
    SetNotificationLevel(pnAlarm);
end;
//------------------------------------------------------------------------------
// On Update Process Timer
//------------------------------------------------------------------------------
procedure TProcForm.OnTimerUpdate(Senter : TObject);
var
  Iter  : integer;
  pProc : TPmaProcess;
  Ind   : integer;
begin
  Iter := 0;
  while Processes.GetNext(Iter, pProc) do
    begin
      // Update the Usage of this Process if its Selected

      if (objProcObj <> nil) and (objProcObj = pProc) then
        begin
          PropUsage.Caption :=
            trim(ToStr(objProcObj.pUsageLong,2) + '%') + resUsageAvg;
          PropUsageAvg.Caption :=
            trim(ToStr(objProcObj.pUsageAvg,2) + '%') + resUsageAvg3;
          PropMem.Caption   := SizeToStr(objProcObj.pMemSize);

          PropThreads.Caption := IntToStr(objProcObj.pThreads);

          PropMem.Caption := SizeToStr(objProcObj.pMemSize);
        end;

      // Update depending on which tab is visible

      case Pages.ActivePageIndex of

        PageLoggedInd : ; // Dont do anything

        PageRunningInd :
          begin
            // Update SubItem

            for Ind := 0 to ProcAlive.Items.Count - 1 do
              if (ProcAlive.Items[Ind].Data = pProc) then
                begin
                  ProcAlive.Items[Ind].SubItems[RunSubItemLongInd] :=
                    trim(ToStr(pProc.pUsageLong, 2));

                  ProcAlive.Items[Ind].SubItems[RunSubItemShortInd] :=
                    trim(ToStr(pProc.pUsageShort, 2));
                end;
          end;

        PageHierarchyInd : ; // Dont do anything

      end;
    end;
end;
//------------------------------------------------------------------------------
//  Resize Process Detailed Information Panel
//------------------------------------------------------------------------------
procedure TProcForm.ProcPanelResize(Sender: TObject);
begin
  // Process Name Header

  ProcName.Left := BRD;
  ProcName.Top  := BRD;

  // Process below the process Name

  ProcDesc.Left   := BRD;
  ProcDesc.Width  := ProcPanel.ClientWidth - BRD * 2;
  ProcDesc.Top    := ProcName.Top + ProcName.Height + BRD;
  ProcDesc.Height := HELPHGT * 5;

  //----------------------------------------------------------------------------
  // Notitication Level is below Description

  NotLevel.Left   := BRD;
  NotLevel.Top    := ProcDesc.Top + ProcDesc.Height + BRD * 2;
  NotLevel.Width  := ProcDesc.Width;
  NotLevel.Height := BtnNormal.Height * 2 + BRD * 2;

  BtnNormal.Left  := BRD*2;
  BtnNormal.Top   := BRD*4;

  BtnWarn.Left    := BtnNormal.Left + BtnNormal.Width + BRD;
  BtnWarn.Top     := BtnNormal.Top;

  BtnAlarm.Left   := BtnWarn.Left + BtnWarn.Width + BRD;
  BtnAlarm.Top    := BtnNormal.Top;

  //----------------------------------------------------------------------------
  // Live Process Data

  LiveProc.Left   := BRD;
  LiveProc.Top    := NotLevel.Top + NotLevel.Height + BRD * 2;
  LiveProc.Width  := ProcDesc.Width;
  LiveProc.Height := LabelPrio.Height * 9 + BRD * (9+5);

  // Add all Properties below

  LabelPrio.Left    := BRD*2;
  LabelPrio.Top     := BRD * 4;
  PropPrio.Left     := LabelThreads.Width + BRD * 6;
  PropPrio.Top      := LabelPrio.Top;

  LabelParent.Left  := LabelPrio.Left;
  LabelParent.Top   := LabelPrio.Top + LabelPrio.Height + BRD;
  PropParent.Left   := PropPrio.Left;
  PropParent.Top    := LabelParent.Top;

  LabelCreated.Left := LabelPrio.Left;
  LabelCreated.Top  := LabelParent.Top + LabelParent.Height + BRD;
  PropCreated.Left  := PropPrio.Left;
  PropCreated.Top   := LabelCreated.Top;

  LabelThreads.Left := LabelPrio.Left;
  LabelThreads.Top  := LabelCreated.Top + LabelCreated.Height + BRD;
  PropThreads.Left  := PropPrio.Left;
  PropThreads.Top   := LabelThreads.Top;

  LabelMem.Left     := LabelPrio.Left;
  LabelMem.Top      := LabelThreads.Top + LabelThreads.Height + BRD;
  PropMem.Left      := PropPrio.Left;
  PropMem.Top       := LabelMem.Top;

  LabelUsage.Left   := LabelPrio.Left;
  LabelUsage.Top    := LabelMem.Top + LabelMem.Height + BRD;
  PropUsage.Left    := PropPrio.Left;
  PropUsage.Top     := LabelUsage.Top;

  LabelUsageAvg.Left   := LabelPrio.Left;
  LabelUsageAvg.Top    := LabelUsage.Top + LabelUsage.Height + BRD;
  PropUsageAvg.Left    := PropPrio.Left;
  PropUsageAvg.Top     := LabelUsageAvg.Top;

  LabelPath.Left    := LabelPrio.Left;
  LabelPath.Top     := LabelUsageAvg.Top + LabelUsageAvg.Height + BRD;
  PropPath.Left     := PropPrio.Left;
  PropPath.Top      := LabelPath.Top;

  LabelCmdLine.Left    := LabelPrio.Left;
  LabelCmdLine.Top     := LabelPath.Top + LabelPath.Height + BRD;
  PropCmdLine.Left     := PropPrio.Left;
  PropCmdLine.Top      := LabelCmdLine.Top;
end;
//------------------------------------------------------------------------------
//
//                             PROCESS LOG LIST TAB
//
//------------------------------------------------------------------------------
//  Load Process List
//------------------------------------------------------------------------------
procedure TProcForm.LoadProcListLog;
var
  Iter  : integer;
  pProc : TPmaProcess;
  LI    : TListItem;
begin
  if (not Assigned(Processes)) then Exit;

  objOnInit := true;

  //objProcList.ProcInit;

  ProcList.Items.BeginUpdate;
  ProcList.Columns.BeginUpdate;
  ProcList.Clear;

  // Read all processes in the order thay exist in Proc List and add them
  // If you encounter an Destroyed process, add it when it was Created
  // then add it as it was destroyed also. We use the sort order of TDateTime

  Iter := 0;
  while Processes.GetNext(Iter, pProc) do
    begin
      // Allways add it as an Created Process

      //if (pProc.pCreateTick > 0) then
        begin
          // Create List Item

          LI := ProcList.Items.Add;
          LI.Data := pProc;

          LI.Caption := IntToStr(pProc.pPID);
          LI.SubItems.Add(pProc.pName);
          LI.SubItems.Add(TPmaDateTime.FileTimeToFullStr(pProc.pCreated));

          LI.SubItems.Add(resAdded);

          LI.SubItems.Add(pProc.pNotLevelStr[1]);
        end;

      // If we know about its destruction add it once more

      if (not TPmaDateTime.IsFileTimeZero(pProc.pDestroyed)) then
        begin
          // Create List Item

          LI := ProcList.Items.Add;
          LI.Data := pProc;

          LI.Caption := IntToStr(pProc.pPID);
          LI.SubItems.Add(pProc.pName);
          LI.SubItems.Add(TPmaDateTime.FileTimeToFullStr(pProc.pDestroyed));

          LI.SubItems.Add(resRemoved);

          LI.SubItems.Add(pProc.pNotLevelStr[1]);
        end;
     end;

  ProcList.AlphaSort;
  
  ProcList.Items.EndUpdate;
  ProcList.Columns.EndUpdate;
  objOnInit := false;

  TabLogResize(nil);
end;
//------------------------------------------------------------------------------
//  Sort Compare using Creation DateTime COlumns
//------------------------------------------------------------------------------
procedure TProcForm.ProcListCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  Compare := 0;
  if Assigned(Item1) and Assigned(Item2) then
    begin
      Compare := AnsiCompareText(
                    Item1.SubItems[SubItemCreated],
                    Item2.SubItems[SubItemCreated]);
    end;
end;
//------------------------------------------------------------------------------
//  User Selected a Process in the Process Log Tab
//------------------------------------------------------------------------------
procedure TProcForm.ProcListChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if (Change = ctState) and
     (Item.Selected) and
     Assigned(Item.Data) then
    begin
      objProcObj    := nil;
      if (TObject(Item.Data) is TPmaProcess) then
        objProcObj := Item.Data;

      SetCurProcessData;
    end;
end;
//------------------------------------------------------------------------------
//  Set Selected Process
//------------------------------------------------------------------------------
procedure TProcForm.SetLogSelected;
var
  Ind : integer;
begin
  ProcList.Selected := nil;
  if Assigned(objProcObj) then
    begin
      for Ind := 0 to ProcList.Items.Count - 1 do
        if (ProcList.Items[Ind].Data = objProcObj) then
          begin
            ProcList.Selected := ProcList.Items[Ind];
            ProcList.Selected.MakeVisible(false);
            EXIT;
          end;
    end;
end;
//------------------------------------------------------------------------------
//  Set Color from Notification Level and Running Process
//------------------------------------------------------------------------------
procedure TProcForm.ProcListCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if Assigned(Item.Data) then
    Sender.Canvas.Brush.Color := TPmaProcess(Item.Data).pColor
  else
    Sender.Canvas.Brush.Color := clWhite;
end;
//------------------------------------------------------------------------------
//  Resize Tab Process Log
//------------------------------------------------------------------------------
procedure TProcForm.TabLogResize(Sender: TObject);
var
  Wdt, Ind : integer;
begin
  ProcList.Left   := BRD;
  ProcList.Width  := TabLog.ClientWidth - BRD * 2;
  ProcList.Top    := BRD;
  ProcList.Height := TabLog.ClientHeight - BRD * 2;

  // Set Column Widths

  ProcList.Columns[LogColumnPidInd].Width     :=  45;
  ProcList.Columns[LogColumnNameInd].Width    := 110;
  ProcList.Columns[LogColumnCreatedInd].Width := 140;
  ProcList.Columns[LogColumnEventInd].Width   :=  65;

  // Set Column Width of the last to cover all

  Wdt := ProcList.ClientWidth;
  for Ind := 0 to ProcList.Columns.Count - 2 do
    Wdt := Wdt - ProcList.Columns[Ind].Width;

  ProcList.Columns[LogColumnNoteInd].Width := Wdt;
end;
//------------------------------------------------------------------------------
//
//                             CURRENT PROCESSES TAB
//
//------------------------------------------------------------------------------
//  Load all Currently Alive Processes
//------------------------------------------------------------------------------
procedure TProcForm.LoadCurProcesses;
var
  bOnInit : boolean;
  Iter    : integer;
  pProc   : TPmaProcess;
  LI      : TListItem;
begin
  bOnInit   := objOnInit;
  objOnInit := true;

  ProcAlive.Items.BeginUpdate;
  ProcAlive.Columns.BeginUpdate;
  ProcAlive.Clear;

  Iter := 0;
  while Processes.GetNext(Iter, pProc) do
    if pProc.pAlive then
      begin
        LI := ProcAlive.Items.Add;

        LI.Caption := IntToStr(pProc.pPID);
        LI.SubItems.Add(pProc.pName);
        LI.SubItems.Add(IntToStr(pProc.pPriority));
        LI.SubItems.Add(trim(ToStr(pProc.pUsageLong, 2)));
        LI.SubItems.Add(trim(ToStr(pProc.pUsageShort, 2)));
        LI.SubItems.Add(SizeToStr(pProc.pMemSize));

        LI.Checked := pProc.pLogged;
        LI.Data := pProc;
      end;

  ProcAlive.Items.EndUpdate;
  ProcAlive.Columns.EndUpdate;
  objOnInit := bOnInit;
end;
//------------------------------------------------------------------------------
// User changed Current Process Selection
//------------------------------------------------------------------------------
procedure TProcForm.ProcAliveSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if (not objOnInit) and Selected and
     (Item.Data <> nil) and (TObject(Item.Data) is TPmaProcess) then
    begin
      objProcObj := Item.Data;
      SetCurProcessData;
    end;
end;
//------------------------------------------------------------------------------
// User changed Current Process Selection
//------------------------------------------------------------------------------
procedure TProcForm.ProcAliveChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if (not objOnInit) and (Change = ctState) and
     (Item.Data <> nil) and
     (TObject(Item.Data) is TPmaProcess) then
    begin
      // Test if the Monitored Property has Changed  TODO

      if (TPmaProcess(Item.Data).pLogged <> Item.Checked) then
        begin
          Log('ProcAliveChange ' + TPmaProcess(Item.Data).pNameId +
                     ' Ch ' + BoolToStr(Item.Checked, true));

          // Let Process List do the job

          TPmaProcess(Item.Data).pLogged := Item.Checked;
          //Processes.SetProcMonitored(Item.Data, Item.Checked);
        end;
    end;
end;
//------------------------------------------------------------------------------
// Set Selected Process
//------------------------------------------------------------------------------
procedure TProcForm.SetRunSelected;
var
  Ind : integer;
begin
  ProcAlive.Selected := nil;
  if (objProcObj <> nil) then
    begin
      for Ind := 0 to ProcAlive.Items.Count - 1 do
        if (ProcAlive.Items[Ind].Data = objProcObj) then
          begin
            ProcAlive.Selected := ProcAlive.Items[Ind];
            ProcAlive.Selected.MakeVisible(false);
            BREAK;
          end;
    end;
end;
//------------------------------------------------------------------------------
// Draw Item Background Color depending on Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.ProcAliveCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if (Item.Data <> nil) and (TObject(Item.Data) is TPmaProcess) then
    Sender.Canvas.Brush.Color :=
      TPmaProcess.NotLevelColor(TPmaProcess(Item.Data).pNotLevel)
  else
    Sender.Canvas.Brush.Color := clWhite;
end;
//------------------------------------------------------------------------------
//  Rezise Alaive Tab
//------------------------------------------------------------------------------
procedure TProcForm.TabAliveResize(Sender: TObject);
var
  Wdt, Ind : integer;
begin
  ProcAlive.Left   := BRD;
  ProcAlive.Width  := TabLog.ClientWidth - BRD * 2;
  ProcAlive.Top    := BRD;
  ProcAlive.Height := TabLog.ClientHeight - BRD * 2;

  ProcAlive.Columns[RunColumnPidInd].Width   :=  60;
  ProcAlive.Columns[RunColumnNameInd].Width  := 110;
  ProcAlive.Columns[RunColumnPrioInd].Width  :=  40;
  ProcAlive.Columns[RunColumnLongInd].Width  :=  55;
  ProcAlive.Columns[RunColumnShortInd].Width :=  55;

  // Set Column Width of the last to cover all

  Wdt := ProcAlive.ClientWidth;
  for Ind := 0 to ProcAlive.Columns.Count - 2 do
    Wdt := Wdt - ProcAlive.Columns[Ind].Width;

  ProcAlive.Columns[RunColumnMemInd].Width := Wdt;
end;
//------------------------------------------------------------------------------
//
//                            PROCESS HIERARCHY TAB
//
//------------------------------------------------------------------------------
// Load Process Hierarchy
//------------------------------------------------------------------------------
procedure TProcForm.LoadProcHier;
var
  bOnInit : boolean;
  Iter    : integer;
  pProc   : TPmaProcess;
  pParent : TPmaProcess;
  TN1,TN2 : TTreeNode;
begin
  bOnInit   := objOnInit;
  objOnInit := true;

  ProcHier.Items.BeginUpdate;
  ProcHier.Items.Clear;

  // Walk Process list and present Process Hierarchy

  Iter := 0;
  while Processes.GetNext(Iter, pProc) do

    // If its already loaded, dont do it again

    if pProc.pAlive and (GetHierProcNode(pProc) = nil) then
      begin
        // Find out if the Parent is still running

        pParent := Processes.GetParent(pProc);
        if Assigned(pParent) then
          begin
            // Load it as a Child of that TReeNode

            TN1 := GetHierProcNode(pParent);

            // If the Parent wasnt found, add it

            if (TN1 = nil) then
              begin
                if pParent.pLogged then
                  TN1 := ProcHier.Items.Add(nil, pParent.pNameId + resMonitored)
                else
                  TN1 := ProcHier.Items.Add(nil, pParent.pNameId);
                TN1.Data := pParent;
              end;

            // Now add this as Child to that parent

            If (TN1 <> nil) then
              begin
                if pProc.pLogged then
                  TN2 := ProcHier.Items.AddChild(
                            TN1, pProc.pNameId + resMonitored)
                else
                  TN2 := ProcHier.Items.AddChild(TN1, pProc.pNameId);
                TN2.Data := pProc;
              end;
          end
        else
          begin
            // Parent dont exist, Load it as an Orphan

            if pProc.pLogged then
              TN1 := ProcHier.Items.Add(nil, pProc.pNameId + resMonitored)
            else
              TN1 := ProcHier.Items.Add(nil, pProc.pNameId);
            TN1.Data := pProc;
          end;
      end;

  ProcHier.FullExpand;
  ProcHier.Items.EndUpdate;
  objOnInit := bOnInit;
end;
//------------------------------------------------------------------------------
//  Return TreeNode from its Process
//------------------------------------------------------------------------------
function TProcForm.GetHierProcNode(const pProc : TPmaProcess):TTreeNode;
var
  Ind : integer;
begin
  result := nil;
  for Ind := 0 to ProcHier.Items.Count - 1 do
    if (ProcHier.Items[Ind].Data = pProc) then
      begin
        result := ProcHier.Items[Ind];
        BREAK;
      end;
end;
//------------------------------------------------------------------------------
//  Set Selected Process
//------------------------------------------------------------------------------
procedure TProcForm.SetHierSelected;
var
  Node : TTreeNode;
begin
  ProcHier.Selected := nil;
  Node := GetHierProcNode(objProcObj);
  if Assigned(Node) then
    begin
      ProcHier.Selected := Node;
      ProcHier.Selected.MakeVisible;
    end
end;
//------------------------------------------------------------------------------
//  User Selected a process
//------------------------------------------------------------------------------
procedure TProcForm.ProcHierChange(Sender: TObject; Node: TTreeNode);
begin
  if (not objOnInit) and (Node.Selected) and
     Assigned(Node.Data) and (TObject(Node.Data) is TPmaProcess) then
    begin
      objProcObj := Node.Data;
      SetCurProcessData;
    end;
end;
//------------------------------------------------------------------------------
// Resize Process Hierarchy Tab
//------------------------------------------------------------------------------
procedure TProcForm.TabHierResize(Sender: TObject);
begin
  // Fill Tab with Hierarchy TreeView

  ProcHier.Left   := BRD;
  ProcHier.Width  := TabHier.ClientWidth - BRD * 2;
  ProcHier.Top    := BRD;
  ProcHier.Height := TabHier.ClientHeight - BRD * 2;
end;
//------------------------------------------------------------------------------
//  Use Node Background Color depending on Notification Level
//------------------------------------------------------------------------------
procedure TProcForm.ProcHierCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if (not Node.Selected) and Assigned(Node.Data) and
     (TObject(Node.Data) is TPmaProcess) then
    begin
      if TPmaProcess(Node.Data).pAlive then
        Sender.Canvas.Brush.Color :=
          TPmaProcess.NotLevelColor(TPmaProcess(Node.Data).pNotLevel)
      else
        Sender.Canvas.Brush.Color := RGB(240,240,240);
    end;
end;

//------------------------------------------------------------------------------
//                                   INITIALIZE
//------------------------------------------------------------------------------
procedure TProcForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;


initialization
  TPmaClassFactory.RegClass(TProcForm);
end.
