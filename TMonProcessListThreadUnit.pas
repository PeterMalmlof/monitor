unit TMonProcessListThreadUnit;

interface

uses
  Windows,  // Lots...
  SysUtils, // Lots...
  Classes,  // Threads

  TBkgThreadQueueUnit;  // Base Thread

//------------------------------------------------------------------------------
// Test Media Thread
//------------------------------------------------------------------------------
type TMonProcessListThread = class(TBkgThread)
  private
  public
    constructor Create(
      const Sub     : integer);  override;

    procedure   Execute; override;
end;

implementation

uses
  TGenStrUnit,          // String Handling
  TPmaWmiUnit,          // WMI Query
  TPmaProcessListUnit,  // Process List Object
  TPmaClassesUnit;      // Class Management

const
  Interval = 1000; // Monitor Interval (1 Second)

//------------------------------------------------------------------------------
//
//                              CREATE THREAD
//
//------------------------------------------------------------------------------
// Create Thread: This will be run in its own thread
//------------------------------------------------------------------------------
constructor TMonProcessListThread.Create(
      const Sub     : integer);
begin
  inherited Create(Sub);

  // We dont need a real reply

  objResult  := 0;
  objReply   := false;

  // It will be write locked until AfterConstruction is called
end;
//------------------------------------------------------------------------------
//
//                                 GET & SET
//
//------------------------------------------------------------------------------
// Execute Object
//------------------------------------------------------------------------------
procedure TMonProcessListThread.Execute;
var
  NextTick : cardinal;
begin
  Log(self.ClassName + ' Started');

  //----------------------------------------------------------------------------
  // NOTE: NEED THIS IF NOT MAIN THREAD
  //----------------------------------------------------------------------------

  TWmiObject.OleInitialize;  // Need to Initialize OLE in this Thread

  // Start Process List Object (MUST BE STARTED HERE BECAUSE OF WMI)

  TPmaProcessList.Startup;

  // Get Next Interval

  objCS.BeginRead;
  NextTick := Processes.pNextTick;
  objCS.EndRead;

  //----------------------------------------------------------------------------
  // Refresh Network Manager in a Loop

  while (not self.Terminated) do
    begin
      // Refresh Load if its Time

      if (Windows.GetTickCount > NextTick) then
        begin
          objCS.BeginWrite;
          Processes.RefreshProcList;
          NextTick := Processes.pNextTick;
          objCS.EndWrite;
        end;

      // Sleep in little steps to make Terminate faster

      Sleep(100);
    end;

  //----------------------------------------------------------------------------
  // Now Write Lock it until its Destroyed

  objCS.BeginWrite;

  objFinished := true;

  Log(self.ClassName + ' Terminated');

  // When finnished it will be Destroyed

  objCS.EndWrite;
end;
//------------------------------------------------------------------------------
//                              ININTIALIZATION
//------------------------------------------------------------------------------
initialization
  TPmaClassFactory.RegClass(TMonProcessListThread);
end.
